package eviet.vlogic.evietandroidapp.socketio;
import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import eviet.vlogic.evietandroidapp.R;

public class MessageAdapterr extends RecyclerView.Adapter<MessageAdapterr.ViewHolder> {
    private List<Message> messageList;
    private Context context;

    public MessageAdapterr(List<Message> messageList) {
        this.messageList = messageList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        context=viewGroup.getContext();
        int layout = -1;
        switch (viewType) {
            case Message.TYPE_MESSAGE:
                layout = R.layout.item_message;
                break;

            case Message.TYPE_MESSAGE_REMOTE:
                layout = R.layout.item_message;
                break;

            case Message.TYPE_ACTION:
                layout = R.layout.item_action;
                break;

        }
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(layout, viewGroup, false);
        return new ViewHolder(v);

    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Message message = messageList.get(position);
        holder.setTime(message.getmTime());
        holder.setMessage(message.getmMessage());
        Log.e("type",message.getmType()+"//"+message.TYPE_MESSAGE);
        if (message.getmType() == message.TYPE_MESSAGE) {
            RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);

            params.addRule(RelativeLayout.ALIGN_PARENT_END);
            holder.rlMessage.setLayoutParams(params);
            holder.rlMessage.setBackground(context.getResources().getDrawable(R.drawable.bg_send_mess));
            holder.txtmessage.setTextColor(R.color.colorWhite);
            holder.txtTime.setTextColor(R.color.colorWhite);
        }




    }


    @Override
    public int getItemCount() {
        return messageList.size();

    }

    @Override
    public int getItemViewType(int position) {
        return messageList.get(position).getmType();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtmessage;
        private TextView txtTime;
        private RelativeLayout rlMessage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtmessage = itemView.findViewById(R.id.txtmessage);
            txtTime = itemView.findViewById(R.id.txtTime);
            rlMessage = itemView.findViewById(R.id.rlMessage);


        }

        public void setTime(String time) {
            if (txtTime == null) return;
            txtTime.setText(time);

        }

        public void setMessage(String message) {
            if (txtmessage == null) return;
            txtmessage.setText(message);
        }
    }
}
