package eviet.vlogic.evietandroidapp.socketio;

public class Message {
    public static final  int TYPE_MESSAGE=0;
    public static final  int TYPE_MESSAGE_REMOTE =1;
    public static final  int TYPE_ACTION=2;

    private int mType;
    private String mMessage;
    private String mTime;
    private String fromMess;
    private String toMess;
    private String createdOn;



    /*"id": "5d036c9f3f14e7037c7bda04",
		"fromMsg": "sender",
		"toMsg": "receiver",
		"msg": "Hii",
		"room": "5d036c5f3f14e7037c7bda02",
		"createdOn": "2019-06-14T09:45:02.000Z"*/

    public Message() {
    }

    public int getmType() {
        return mType;
    }

    public String getmMessage() {
        return mMessage;
    }

    public String getmTime() {
        return mTime;
    }


    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public void setmType(int mType) {
        this.mType = mType;
    }

    public void setmTime(String mTime) {
        this.mTime = mTime;
    }

    public String getFromMess() {
        return fromMess;
    }

    public void setFromMess(String fromMess) {
        this.fromMess = fromMess;
    }

    public String getToMess() {
        return toMess;
    }

    public void setToMess(String toMess) {
        this.toMess = toMess;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public static class Builder{
        private final int mType;
        private String mMessage;
        private String mTime;

        public Builder(int mType) {
            this.mType = mType;
        }

        public Builder time(String time){
            mTime=time;
            return this;
        }
        public Builder message(String message)
        {
            mMessage=message;
            return this;
        }
        public Message build()
        {
            Message message=new Message();
            message.mType=mType;
            message.mMessage=mMessage;
            message.mTime =mTime;
            return message;
        }
    }
}

