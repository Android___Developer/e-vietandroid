package eviet.vlogic.evietandroidapp.socketio;

import android.nfc.Tag;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.BottomActivity;
import eviet.vlogic.evietandroidapp.adapters.MessageAdapter;
import eviet.vlogic.evietandroidapp.application.ChatApplication;
import eviet.vlogic.evietandroidapp.fragment.DashboardFragment;
import eviet.vlogic.evietandroidapp.fragment.MessageFragment;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.retrofit.RetrofitService;
import eviet.vlogic.evietandroidapp.retrofit.ServiceResponce;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;
import eviet.vlogic.evietandroidapp.utility.Utility;


public class FragmentChatMain extends Fragment implements View.OnClickListener,ServiceResponce {

    private static final String TAG = "fragmentChatMain";
    private static final int REQUEST_LOGIN = 0;
    private static final int TYPING_TIMER_LENGTH = 3000;
    private RecyclerView messagesRecycler;
    private EditText edMessageInput;
    private ImageButton btSend;
    private List<Message> messageList = new ArrayList<>();
    private RecyclerView.Adapter adapter;
    private boolean mTyping = false;
    private Handler mTypingHandler = new Handler();
    private Socket mSocket;
    private String toUser;
    private String roomId = "";
    private int msgCount = 20;
    private CountDownTimer timer = null;
    private TextView txtTyping;
    private Animation animFadeIn = null;
    private Animation animFadeOut = null;
    private int currentPage = 1;
    private int totalItemCount;
    private int visibleItemCount;
    private int scrolledOutItemCount;
    private boolean isScrolling = false;
    private LinearLayoutManager manager;
    private float total;
    private ImageView imgBack,setting;
    private TextView toolTitle;


    public FragmentChatMain() {
        super();
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
        Log.e("onAttach", "onAttach");

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BottomActivity)getActivity()).navigation.setVisibility(View.GONE);
        ChatApplication app = (ChatApplication) getActivity().getApplication();
        mSocket = app.getmSocket();
        mSocket.on("set-room", onGettingRoomId);
        mSocket.on("chat-msg", onNewMessage);
        mSocket.on("typing", onTyping);
        Log.e("socketData", "" + onGettingRoomId + "//" + onNewMessage + "//" + onTyping);
        Log.e("socketConnection", "" + mSocket.connect() + "//" + mSocket.connected());
        if (!mSocket.connected()) {
            mSocket.connect();

        }


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_chat_main, container, false);
//      setHasOptionsMenu(true);
        initView(v);

      /*  if (getArguments() != null) {
            String username = PreferencesHelper.getUserName();

            String secondUser = getArguments().getString("username");
            Log.e("usename",username+"//"+secondUser);
            toUser = secondUser.substring(1, secondUser.length() - 1);
            Log.d(TAG, username + "//" + toUser);
            String currentRoom = username + "-" + toUser;
            String reverseRoom = toUser + "-" + username;
            JSONObject jsonObject = new JSONObject();
            try {

                jsonObject.put("name1",currentRoom);
                jsonObject.put("name2",reverseRoom);

            } catch (JSONException e) {
                e.printStackTrace();
            }


            mSocket.emit("set-room",(Object) jsonObject);
            Log.d(TAG, jsonObject.toString());
        }*/

        return v;
    }

    private void initView(View v) {

        imgBack = v.findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.ic_back);
        imgBack.setOnClickListener(this);
        toolTitle =  v.findViewById(R.id.toolTitle);
        toolTitle.setText("Questions");
        setting = v.findViewById(R.id.setting);
        setting.setImageResource(R.drawable.ic_add);
        txtTyping = v.findViewById(R.id.txtTyping);
        txtTyping.setVisibility(View.GONE);
        adapter = new MessageAdapterr(messageList);
    }

    @Override
    public void onResume() {
        super.onResume();
        mSocket.connect();

        if (getArguments() != null) {
            //  String username = PreferencesHelper.getUserName();
            String username = "guest18";
            String secondUser = getArguments().getString("username");
            toUser = secondUser.substring(1, secondUser.length() - 1);
            Log.d(TAG, username + "//" + toUser);
            String currentRoom = username + "-" + toUser;
            String reverseRoom = toUser + "-" + username;
            JSONObject jsonObject = new JSONObject();

            try {
                jsonObject.put("name1",currentRoom);               //{"name1":"receiver-samm","name2":"samm-receiver"}
                jsonObject.put("name2",reverseRoom);
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d(TAG, "JsonException");
            }


            mSocket.emit("set-room", jsonObject);
            // mSocket.emit("set-room" ,jsonObject);

            Log.d(TAG, jsonObject.toString());
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        mSocket.off("set-room", onGettingRoomId);
        mSocket.off("new message", onNewMessage);
        mSocket.off("typing", onTyping);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        messagesRecycler = view.findViewById(R.id.messagesRecycler);
        manager = new LinearLayoutManager(getActivity());
        messagesRecycler.setLayoutManager(manager);
        messagesRecycler.setAdapter(adapter);
        edMessageInput = view.findViewById(R.id.edMessageInput);
        edMessageInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!mSocket.connected()) return;


                Log.d(TAG, "emitTyping");
                mSocket.emit("typing");
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        btSend = view.findViewById(R.id.btSend);
        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "clicked", Toast.LENGTH_SHORT).show();
                attemptSend();
            }
        });


        messagesRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Log.d(TAG, "dx " + dx + "//" + "dy " + dy);
                totalItemCount = manager.getItemCount();
                visibleItemCount = manager.getChildCount();
                scrolledOutItemCount = manager.findFirstVisibleItemPosition();
                if (dy < 0) {
                    if (isScrolling && scrolledOutItemCount + visibleItemCount == totalItemCount) {
                        currentPage++;
                        if (!roomId.isEmpty()) {
                            getOldChats(roomId);
                        }

                    }
                }


            }
        });


    }

    private void attemptSend() {

        if (!mSocket.connected()) return;

        String message = edMessageInput.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            edMessageInput.requestFocus();
            return;
        }
        edMessageInput.setText("");
        String time = new SimpleDateFormat("hh:mm aa", Locale.ENGLISH).format(Calendar.getInstance().getTime());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("msg", message);
            jsonObject.put("msgTo", toUser);
            jsonObject.put("date", time);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, jsonObject.toString() + "   emit this");
        addMessage(time, message, Message.TYPE_MESSAGE);
        mSocket.emit("chat-msg", jsonObject);


    }

    private Emitter.Listener onGettingRoomId = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.d(TAG, "getting Id");
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "roomih" + args[0] + "//");
                        Log.d(TAG, args[0] + "//");
                        roomId = args[0].toString();
                        if (!roomId.isEmpty()) {
                            getOldChats(roomId);

                        }
                    }
                });
            }
        }
    };


    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            if (getActivity() != null)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        JSONObject jsonObject = (JSONObject) args[0];
                        Log.e("new Message", "//" + jsonObject.toString());
                        if (jsonObject.has("msgFrom") && jsonObject.optString("msgFrom").equalsIgnoreCase(toUser)) {

                            Log.d(TAG, jsonObject.toString() + " remote message");
                            String time;
                            String message;
                            try {

                                time = jsonObject.getString("date");
                                message = jsonObject.getString("msg");

                            } catch (JSONException e) {
                                Log.d(TAG, "new Message exception ");
                                return;
                            }
                            Log.d(TAG, time + "//" + message);

                            addMessage(time, message, Message.TYPE_MESSAGE_REMOTE);

                        }

                    }
                });
        }
    };

    private void addMessage(String time, String message, int type) {
        messageList.add(new Message.Builder(type).time(time).message(message).build());
        adapter.notifyItemInserted(messageList.size() - 1);
        scrollToBottom();

    }

    private void scrollToBottom() {
        messagesRecycler.scrollToPosition(adapter.getItemCount() - 1);

    }


    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.d(TAG, "onTyping");
            if (getActivity() != null)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, args[0] + "");
                        String typingMess = args[0].toString();
                        animFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
                        animFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);

                        txtTyping.setAnimation(animFadeIn);
                        txtTyping.setVisibility(View.VISIBLE);
                        if (!mTyping)
                            mTyping = true;


                        Log.d(TAG, "problem here");
                        if (mTyping) {
                            mTypingHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    txtTyping.setVisibility(View.GONE);
                                    txtTyping.setAnimation(animFadeOut);
                                    mTyping = false;

                                }
                            }, 1500);
                        }


                    }
                });

        }
    };

    private void getOldChats(String roomId) {

        new RetrofitService(getActivity(), ServiceUrls.URL_GET_OLD_CHAT + "?room_id=" + roomId + "&pageSize=20&pageNo=" + currentPage,
                1, 1, new JsonObject(), true, this).callService(false);

    }

    @Override
    public void onServiceResponce(String result, int requestCode) {
        Log.e("OLD_MESSAGES", result + "//");
        if (requestCode == 1) {
            Log.d(TAG, "oldChat" + result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("isSuccess") && jsonObject.optBoolean("isSuccess")) {
                    total = Float.valueOf(jsonObject.optString("total")) / Float.valueOf(jsonObject.optString("pageSize"));
                    JSONArray jsonArray = jsonObject.optJSONArray("items");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Message message = new Message();
                        JSONObject obj = jsonArray.optJSONObject(i);
                        message.setFromMess(obj.optString("fromMsg"));
                        message.setToMess(obj.optString("toMsg"));
                        message.setmMessage(obj.optString("msg"));
                        message.setmTime(obj.optString("createdOn"));

                        if (jsonObject.optString("fromMsg").equalsIgnoreCase(PreferencesHelper.getUserName())) {
                            message.setmType(Message.TYPE_MESSAGE);
                        } else {
                            message.setmType(Message.TYPE_MESSAGE_REMOTE);
                        }

                        messageList.add(message);
                    }
                    if (!messageList.isEmpty()) {
                        adapter.notifyDataSetChanged();

                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            /*{
    "isSuccess": true,
	"statusCode": 200,
	"pageNo": 1,
	"pageSize": 20,
	"total": 12,
	"items": [{
		"id": "5d036c9f3f14e7037c7bda04",
		"fromMsg": "sender",
		"toMsg": "receiver",
		"msg": "Hii",
		"room": "5d036c5f3f14e7037c7bda02",
		"createdOn": "2019-06-14T09:45:02.000Z"
	}, */

        }
    }


    @Override
    public void onClick(View view) {
       switch (view.getId()){
           case R.id.imgBack:
               Utility.changeFragment(getActivity(),new MessageFragment());
               break;
       }
    }
}

