package eviet.vlogic.evietandroidapp.chatModule;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.net.URISyntaxException;
import java.util.ArrayList;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.adapters.MessageAdapter;
import eviet.vlogic.evietandroidapp.model.MessageModel;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

public class MessageModule extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = MessageModule.class.getSimpleName();
    private ImageView img_sync;
    private TextView toolTitle;
    private RecyclerView messageList;
    private MessageAdapter adapter;
    private ArrayList<MessageModel> arrayList;
    private ImageView sendMessage;
    private EditText editTextMessage;
    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://chat.socket.io");
        } catch (URISyntaxException e) {}
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_module);
        initView();
        mSocket.on("new message", onNewMessage);
        mSocket.connect();

        img_sync.setOnClickListener(this);
        sendMessage.setOnClickListener(this);

        if(PreferencesHelper.getValue()!=null){
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                toolTitle.setText("CHAT");

            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                toolTitle.setText(R.string.chat_vit);
            }
        }

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(false);
        messageList.setLayoutManager(linearLayoutManager);

        arrayList = new ArrayList<>();
        adapter = new MessageAdapter(this, arrayList);
        messageList.setAdapter(adapter);


    }

    private void initView() {
        editTextMessage = findViewById(R.id.editTextMessage);
        sendMessage = findViewById(R.id.sendMessage);
        img_sync = findViewById(R.id.imgBack);
        toolTitle = findViewById(R.id.toolTitle);
        messageList = findViewById(R.id.messageList);


//        messageList.getLayoutManager().scrollToPosition(adapter.getItemCount()-1);
      /*  messageList.scrollToPosition(adapter.getItemCount()-1);
        mLinearLayoutManager.scrollToPosition(yourList.size() - 1);*/

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.sendMessage:

                if (editTextMessage.getText().toString().trim().length() != 0) {
                    MessageModel messageModule = new MessageModel();
                    messageModule.setReceiverName("Driver");
                    messageModule.setUserType("1");
                    messageModule.setMessage(editTextMessage.getText().toString());
                    mSocket.emit("new message", editTextMessage.getText().toString().trim());
                    editTextMessage.setText("");
                    arrayList.add(messageModule);
                    adapter.notifyDataSetChanged();

                } else {
                    Toast.makeText(getApplicationContext(), "No Message here", Toast.LENGTH_SHORT).show();

                }

                break;

            case R.id.imgBack:
                onBackPressed();
                break;

        }

    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            MessageModule.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    Log.e("data",data+"");
                    String username;
                    String message;
                    try {
                        username = data.getString("username");
                        Log.e("username",username+"");
                        message = data.getString("message");
                        Log.e("message",message+"");
                    } catch (JSONException e) {
                        return;
                    }

                    // add the message to view
//                    addMessage(username, message);
                }
            });
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();

        mSocket.disconnect();
        mSocket.off("new message", onNewMessage);
    }
}
