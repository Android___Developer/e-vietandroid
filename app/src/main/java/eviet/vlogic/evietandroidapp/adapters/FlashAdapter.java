package eviet.vlogic.evietandroidapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.model.FlashCardResponseModel;
import eviet.vlogic.evietandroidapp.model.FlashData;


public class FlashAdapter extends RecyclerView.Adapter<FlashAdapter.FlashHolder> {
    private Context context;
    private ArrayList<FlashData> flashArrayList;
    FlashData flashData;
    FlashCardResponseModel flashCardResponseModel;
    FlashCardClick click;

    public FlashAdapter(Context context, ArrayList<FlashData> flashArrayList) {
        this.context = context;
        this.flashArrayList = flashArrayList;
    }

    @NonNull
    @Override
    public FlashAdapter.FlashHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.flash_card_row, parent, false);
        return new FlashAdapter.FlashHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FlashHolder flashHolder, int position) {
        flashData = flashArrayList.get(position);
        flashHolder.flashTextView.setText(flashData.getName());

        if (flashData.isStatus()){

            flashHolder.flashImageView.setBackground(context.getDrawable(R.drawable.water_background_flashcard));
        }else {
            flashHolder.flashImageView.setBackground(context.getDrawable(R.drawable.white_background_flashcard));
        }

        Picasso.get()
                .load(flashData.getImage())
                .placeholder(R.color.colorGrey)
                .error(R.drawable.bread).fit()
                .into(flashHolder.flashImageView);



        Log.e("Flash_name", flashData.getName());
    }

    @Override
    public int getItemCount() {
        return flashArrayList.size();
    }

    class FlashHolder extends RecyclerView.ViewHolder {
        private TextView flashTextView, questionFlashCard;
        private ImageView flashImageView;

        public FlashHolder(View itemView) {
            super(itemView);
//            questionFlashCard = itemView.findViewById(R.id.questionFlashCard);
            flashTextView = itemView.findViewById(R.id.flashTextView);
            flashImageView = itemView.findViewById(R.id.flashImageView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    click.onFlashCardClick(getAdapterPosition());
                }
            });

        }
    }

    public void setOnFlashCardClickListener(FlashCardClick click) {
        this.click = click;
    }

    public interface FlashCardClick {
        public void onFlashCardClick(int pos);
    }
}
