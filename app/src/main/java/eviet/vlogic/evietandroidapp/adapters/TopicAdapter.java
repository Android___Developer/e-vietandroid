package eviet.vlogic.evietandroidapp.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.BubbleImageView;
import eviet.vlogic.evietandroidapp.fragment.FlashCardApiFragment;
import eviet.vlogic.evietandroidapp.fragment.FlashCardFragment;
import eviet.vlogic.evietandroidapp.fragment.NotificationsFragment;
import eviet.vlogic.evietandroidapp.fragment.TopicFragment;
import eviet.vlogic.evietandroidapp.model.MessageModel;
import eviet.vlogic.evietandroidapp.model.TopicResponseModel;
import eviet.vlogic.evietandroidapp.utility.Utility;

public class TopicAdapter extends RecyclerView.Adapter<TopicAdapter.TopicHolder> {
    private Context context;
    private ArrayList<TopicResponseModel> topicArrayList;

    private String Topic_id;

    public TopicAdapter(Context context, ArrayList<TopicResponseModel> arrayList) {
        this.context = context;
        this.topicArrayList = arrayList;
    }

    @NonNull
    @Override
    public TopicHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.topic_row, parent, false);
        return new TopicHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TopicHolder holder, int position) {
    //todo for set data
         TopicResponseModel   topicResponseModel = topicArrayList.get(position);
        holder.topicName.setText(topicResponseModel.getTopic_name());

        Picasso.get()
                .load(topicResponseModel.getTopic_image())
                .placeholder(R.color.colorGrey)
                .error(R.drawable.bread).fit().rotate(90)
                .into(holder.topicImage);

    Log.d("topic_name",topicResponseModel.getTopic_name());
        Log.d("getTopic_id",topicResponseModel.getTopic_id());
        Topic_id = topicResponseModel.getTopic_id();

    }

    @Override
    public int getItemCount() {
   return topicArrayList.size();
    }


    class TopicHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private LinearLayout topicLayoutView;
        private TextView topicName;
        private BubbleImageView topicImage;

        public TopicHolder(View itemView) {
            super(itemView);
            topicLayoutView = itemView.findViewById(R.id.topicLayoutView);
            topicName = itemView.findViewById(R.id.topicName);
            topicImage = itemView.findViewById(R.id.topicImage);
            topicLayoutView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.topicLayoutView:
                    Log.e("okok", getAdapterPosition() + "");

                    String topic_id = topicArrayList.get(getAdapterPosition()).getTopic_id();

                    Log.e("topic_id",topic_id);

                    //Put the value
                    FlashCardApiFragment flashCardApiFragment = new FlashCardApiFragment();
                    Bundle args = new Bundle();
                    args.putString("topic_id", topic_id);
                    flashCardApiFragment.setArguments(args);

                    Utility.changeFragment(context,flashCardApiFragment);
                    break;
            }

        }
    }
}


