package eviet.vlogic.evietandroidapp.adapters;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.model.MessageModel;
import eviet.vlogic.evietandroidapp.socketio.Message;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageHolder> {
    private Context context;
    private ArrayList<MessageModel> arrayList;
    private MessageModel messageModel;

    public MessageAdapter(Context context, ArrayList arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }


    @NonNull
    @Override
    public MessageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.chat_message_row, parent, false);
        return new MessageHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull MessageHolder holder, int position) {
//todo for set data
        messageModel=arrayList.get(position);
        holder.receiverLayout.setVisibility(View.GONE);
        holder.senderLayout.setVisibility(View.VISIBLE);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm aa");
        String datetime = dateformat.format(c.getTime());
        System.out.println(datetime);
        holder.sander_message.setText(messageModel.getMessage()+"\t\t"+datetime);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class MessageHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private RelativeLayout receiverLayout,senderLayout;
        private TextView sander_message,receiverName;
        public MessageHolder(View itemView) {
            super(itemView);
            receiverLayout = itemView.findViewById(R.id.receiverLayout);
            senderLayout   = itemView.findViewById(R.id.senderLayout);
            sander_message   = itemView.findViewById(R.id.sander_message);
            receiverName   = itemView.findViewById(R.id.receiverName);

            /*cardMain.setOnClickListener(this);*/

        }

        @Override
        public void onClick(View v) {
            /*switch (v.getId()) {
                case R.id.cardMain:
                    Intent intent = new Intent(context, HistoryDetailActivty.class);
                    context.startActivity(intent);
                    break;*/
            }

        }
    }

