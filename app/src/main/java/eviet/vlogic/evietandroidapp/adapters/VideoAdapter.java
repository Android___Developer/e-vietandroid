package eviet.vlogic.evietandroidapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.ViewVideoActivity;
import eviet.vlogic.evietandroidapp.model.VideoResponseModel;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.Holder> {
    private Context context;
    private ArrayList<VideoResponseModel> videoArrayList;


    public VideoAdapter(Context context, ArrayList<VideoResponseModel> videoArrayList) {
        this.context = context;
        this.videoArrayList = videoArrayList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.video_adapter, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        VideoResponseModel videoResponseModel = videoArrayList.get(position);
        Log.e("vdurl", videoResponseModel.getVideo() + "//");
       try {
           Uri uri = Uri.parse(videoArrayList.get(position).getVideo());
//           holder.tutorialsVideo.setVideoURI(uri);
          /* Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(videoArrayList.get(position).getVideo(), MediaStore.Video.Thumbnails.MINI_KIND);
           Log.e("Bitmap",bitmap+"");*/
           videoArrayList.get(position).getVideo();
//           holder.videoImage.setImageBitmap(bitmap);

           holder.videoName.setText(videoArrayList.get(position).getVideo_title());

           Picasso.get()
                   .load(videoArrayList.get(position).getVideo_image())
                   .placeholder(R.color.colorGrey)
                   .error(R.drawable.bread).fit()
                   .into(holder.videoImage);

       }catch (Exception ex){

       }
    }


    @Override
    public int getItemCount() {
        return videoArrayList.size();
    }

    class Holder extends RecyclerView.ViewHolder {
         TextView videoName;
        ImageView videoImage;

        public Holder(@NonNull View itemView) {
            super(itemView);
            videoName = itemView.findViewById(R.id.videoName);
            videoImage = itemView.findViewById(R.id.videoImage);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                  /*  VideoWatchFragment fragment=new VideoWatchFragment();
                    Bundle args = new Bundle();
                    args.putString("video",videoArrayList.get(getAdapterPosition()).getVideo() );
                    fragment.setArguments(args);
                    Utility.changeFragmentWithString(context, fragment, "Reward");*/


                    Intent intent = new Intent(context,ViewVideoActivity.class);
                    intent.putExtra("video", videoArrayList.get(getAdapterPosition()).getVideo());
                    intent.putExtra("video_id", videoArrayList.get(getAdapterPosition()).getVideo_id());
                    context.startActivity(intent);
                }

            });

        }

    }

}
