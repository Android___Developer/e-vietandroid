package eviet.vlogic.evietandroidapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;

import eviet.vlogic.evietandroidapp.R;

public class FlashCardAdapter extends RecyclerView.Adapter<FlashCardAdapter.FlashViewHolder> {
    private Context context;
    private ArrayList<String> arrayListFlash;

    public FlashCardAdapter(Context context, ArrayList arrayListFlash) {
        this.context = context;
        this.arrayListFlash = arrayListFlash;
    }

    @NonNull
    @Override
    public FlashViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.flash_card_row, parent, false);
        return new FlashViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FlashViewHolder holder, int position) {
//            holder.txtProductName.setText(productName);
//            holder.txtProductDesp.setText(productDesc);
//            holder.txtProductWeight.setText(productWeight);
//            holder.txtProductPO.setText(productPO);
    }

    @Override
    public int getItemCount() {
//        return arrayListFlash.size();
        return 5;
    }

    class FlashViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private LinearLayout flashLayout;
        private ImageView flashImageView;
        private TextView flashTextView;
        public FlashViewHolder(View itemView) {
            super(itemView);
            flashLayout = itemView.findViewById(R.id.flashLayout);
            flashImageView = itemView.findViewById(R.id.flashImageView);
            flashTextView = itemView.findViewById(R.id.flashTextView);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
              /*  case R.id.cardMain:
                    Intent intent = new Intent(context, HistoryDetailActivty.class);
                    context.startActivity(intent);
                    break;*/
            }

        }
    }
}
