package eviet.vlogic.evietandroidapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.model.WordMatchingOptionResponse;


public class WordMatchAdapter extends RecyclerView.Adapter<WordMatchAdapter.WordMatchHolder> {
    private Context context;
    private ArrayList<WordMatchingOptionResponse> marchResponseModelArrayList;
    private WordMatchingOptionResponse wordMatchingOptionResponse;

    public WordMatchAdapter(Context context, ArrayList<WordMatchingOptionResponse> marchResponseModelArrayList) {
        this.context = context;
        this.marchResponseModelArrayList = marchResponseModelArrayList;
    }

    @NonNull
    @Override
    public WordMatchHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.word_match_row, parent, false);
        return new WordMatchHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull WordMatchHolder wordMatchHolder, int position) {
        //todo for set data
        wordMatchingOptionResponse = marchResponseModelArrayList.get(position);
        wordMatchHolder.option.setText(wordMatchingOptionResponse.getOptions());

        switch (position){
            case 0:
                wordMatchHolder.option.setBackground(context.getDrawable(R.drawable.orange_background_corners));
                break;
            case 1:
                wordMatchHolder.option.setBackground(context.getDrawable(R.drawable.button_red_bg));
                break;
            case 2:
                wordMatchHolder.option.setBackground(context.getDrawable(R.drawable.button_green_bg));
                break;
            case 3:
                wordMatchHolder.option.setBackground(context.getDrawable(R.drawable.button_blue_bg));
                break;
        }

        Log.d("option", wordMatchingOptionResponse.getOptions());
    }

    @Override
    public int getItemCount() {
        return marchResponseModelArrayList.size();
    }

    class WordMatchHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView option;

        public WordMatchHolder(View itemView) {
            super(itemView);
            option = itemView.findViewById(R.id.option);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
//                case R.id.topicLayoutView:
//                    Log.e("okok", getAdapterPosition() + "");
////                    Utility.changeFragment(context, new FlashCardFragment());
//                    Utility.changeFragment(context, new FlashCardApiFragment());
//                    break;
            }

        }
    }
}

