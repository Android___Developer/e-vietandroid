package eviet.vlogic.evietandroidapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.model.MessageModel;

public class DemoAdapter extends RecyclerView.Adapter<DemoAdapter.DemoHolder> {
    private Context context;
    private ArrayList<MessageModel> arrayList;
    private MessageModel messageModel;

    public DemoAdapter(Context context, ArrayList arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public DemoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.topic_row, parent, false);
        return new DemoHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull DemoHolder holder, int position) {
//todo for set data

    }

    @Override
    public int getItemCount() {
//        return arrayList.size();
        return 10;
    }

    class DemoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private RelativeLayout receiverLayout,senderLayout;
        private TextView sander_message,receiverName;
        public DemoHolder(View itemView) {
            super(itemView);
            receiverLayout = itemView.findViewById(R.id.receiverLayout);
            senderLayout   = itemView.findViewById(R.id.senderLayout);
            sander_message   = itemView.findViewById(R.id.sander_message);
            receiverName   = itemView.findViewById(R.id.receiverName);

            /*cardMain.setOnClickListener(this);*/

        }

        @Override
        public void onClick(View v) {
            /*switch (v.getId()) {
                case R.id.cardMain:
                    Intent intent = new Intent(context, HistoryDetailActivty.class);
                    context.startActivity(intent);
                    break;*/
        }

    }
}


