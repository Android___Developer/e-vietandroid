package eviet.vlogic.evietandroidapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.model.ChatUserListModel;


public class ChatUserListAdapter extends RecyclerView.Adapter<ChatUserListAdapter.ChatViewHolder> {
    private List<ChatUserListModel> chatUserList;
    private Context context;
    private UserClickInterface click;

    public ChatUserListAdapter(List<ChatUserListModel> chatUserList) {
        this.chatUserList = chatUserList;
    }

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chat_user_list_adapter,viewGroup,false);
        context=viewGroup.getContext();
        return new ChatViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder holder, int position) {
        ChatUserListModel model=chatUserList.get(position);

        String userName=model.getUserName();
        String trimUserName=userName.substring(1,userName.length()-1);
        holder.txtUserName.setText(trimUserName);
        String status=model.getStatus();
        String statusTrim=status.substring(1,status.length()-1);
        Log.e("status",model.getStatus());
        if (statusTrim.equalsIgnoreCase("Offline"))
        {

            holder.imgUserStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_info_grey));
        }
        else {
            holder.imgUserStatus.setImageResource(R.drawable.ic_info_yellow);
        }

    }

    @Override
    public int getItemCount() {
        return chatUserList.size();
    }

    class ChatViewHolder extends RecyclerView.ViewHolder {
        private TextView txtUserName;
        private ImageView imgUserStatus;
        private RelativeLayout rl_user;
        public ChatViewHolder(@NonNull View itemView) {
            super(itemView);
            txtUserName=itemView.findViewById(R.id.txtUserName);
            imgUserStatus=itemView.findViewById(R.id.imgUserStatus);
            rl_user=itemView.findViewById(R.id.rl_user);
            rl_user.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    click.onUserClick(getAdapterPosition());
                }
            });
        }
    }
    public void onUserClickListner(UserClickInterface click)
    {
        this.click=click;

    }
    public interface UserClickInterface{
        void onUserClick(int position);
    }
}
