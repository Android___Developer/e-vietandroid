package eviet.vlogic.evietandroidapp.utility;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Patterns;

public class CheckPermissions {
    public static final int CODE_SUCCESSFULL = 200;

    public static boolean checkCameraPermission(Context context)
    {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;

    }
    public static boolean CheckGalleryPermission(Context context)
    {
        return ContextCompat.checkSelfPermission(context,Manifest.permission.READ_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED;

        }


        public static void requestCameraPermission(Activity context,int requestCode)
        {
            ActivityCompat.requestPermissions(context,new String[]{Manifest.permission.CAMERA},requestCode);

            }

    public static void requestGalleryPermission(Activity context,int requestCode)
    {
        ActivityCompat.requestPermissions(context,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},requestCode);

    }


    public static boolean checkLocationPermission(Activity context)
    {
        return (ContextCompat.checkSelfPermission(context,Manifest.permission.ACCESS_COARSE_LOCATION)==PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context,Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED);
    }

    public static boolean checkWakelockPermission(Activity context)
    {
        return (ContextCompat.checkSelfPermission(context,Manifest.permission.WAKE_LOCK)==PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context,Manifest.permission.WAKE_LOCK)==PackageManager.PERMISSION_GRANTED);
    }


    public static void requestLocationPermission(Activity context,int requestCode)
    {
        ActivityCompat.requestPermissions(context,new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},requestCode);
    }
    public static boolean isValidEmail(String email)
    {
        return !TextUtils.isEmpty(email)&& Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }




}
