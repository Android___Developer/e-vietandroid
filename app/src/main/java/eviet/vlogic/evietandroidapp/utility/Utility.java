package eviet.vlogic.evietandroidapp.utility;

import android.content.Context;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import eviet.vlogic.evietandroidapp.R;

public class Utility {

    public String deviceId(Context context){
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }


    /*public static void changeFragment(Context context, Fragment fragment) {
        FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container,fragment).addToBackStack("tag").commit();
    }*/

    public static void changeFragment(Context context, Fragment fragment) {
        FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container,fragment).commit();
    }

    public static void changeFragmentWithString(Context context, Fragment fragment,String str) {
        FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container,fragment).addToBackStack(str).commit();
    }

}
