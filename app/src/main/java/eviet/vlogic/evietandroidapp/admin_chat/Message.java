package eviet.vlogic.evietandroidapp.admin_chat;

public class Message {
    public static final int TYPE_MESSAGE_LOCAL=0;
    public static final int TYPE_MESSAGE_REMOTE=1;


    private  int mType;
    private  String mMessage;
    private String mTime;
    private String fromMess;
    private String toMess;
    private String createdOn;

    public String getFromMess() {
        return fromMess;
    }

    public void setFromMess(String fromMess) {
        this.fromMess = fromMess;
    }

    public String getToMess() {
        return toMess;
    }

    public void setToMess(String toMess) {
        this.toMess = toMess;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Message() {
    }

    public int getmType() {
        return mType;
    }

    public void setmType(int mType) {
        this.mType = mType;
    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public String getmTime() {
        return mTime;
    }

    public void setmTime(String mTime) {
        this.mTime = mTime;
    }


    public static class Builder{
        private int type;
        private String message;
        private String time;


        public Builder(int type)
        {
            this.type=type;

        }
        public Builder time(String time)
        {
            this.time=time;
            return this;
        }

        public Builder message(String message){
            this.message=message;
            return this;
        }

        public Message build(){
            Message messageBuild=new Message();
            messageBuild.mType=type;
            messageBuild.mTime=time;
            messageBuild.mMessage=message;
            return messageBuild;

        }

    }
}

