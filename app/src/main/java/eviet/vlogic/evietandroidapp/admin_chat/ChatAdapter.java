package eviet.vlogic.evietandroidapp.admin_chat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import eviet.vlogic.evietandroidapp.R;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder> {
    private final String TAG=getClass().getName();
    private List<Message> messageList;
    private Context context;

    public ChatAdapter(List<Message> messageList) {
        this.messageList = messageList;
    }

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context=parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_adapter, parent, false);
        return new ChatViewHolder(v);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder holder, int position) {
        Log.d(TAG,"OKO");
        Message message = messageList.get(position);
        Log.d(TAG,"values//"+message.getmMessage()+"//"+message.getmTime());


        if (message.getmType() == Message.TYPE_MESSAGE_LOCAL) {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_END);
            holder.rlMessage.setLayoutParams(params);
            holder.rlMessage.setBackground(context.getResources().getDrawable(R.drawable.bg_chat_local));
            holder.txtMessage.setText(message.getmMessage());
            holder.txtMessage.setTextColor(context.getResources().getColor(R.color.colorWhite));
            holder.txtTime.setText(message.getmTime());
            holder.txtTime.setTextColor(context.getResources().getColor(R.color.colorWhite));
        }
        else {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_START);
            holder.rlMessage.setLayoutParams(params);
            holder.rlMessage.setBackground(context.getResources().getDrawable(R.drawable.bg_chat_remote));
            holder.txtMessage.setText(message.getmMessage());
            holder.txtMessage.setTextColor(context.getResources().getColor(R.color.textColor));
            holder.txtTime.setText(message.getmTime());
            holder.txtTime.setTextColor(context.getResources().getColor(R.color.textColor));
        }


    }

    @Override
    public int getItemCount() {
        Log.d(TAG,"size//"+messageList.size()+"");
        return messageList.size();
    }


    class ChatViewHolder extends RecyclerView.ViewHolder {
        private TextView txtMessage;
        private TextView txtTime;
        private RelativeLayout rlMessage;

        public ChatViewHolder(View itemView) {
            super(itemView);
            txtMessage = itemView.findViewById(R.id.txtMessage);
            txtTime = itemView.findViewById(R.id.txtTime);
            rlMessage = itemView.findViewById(R.id.rlMessage);

        }

    }

}
