package eviet.vlogic.evietandroidapp.admin_chat;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import eviet.vlogic.evietandroidapp.MainActivity;
import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.application.ChatApplication;
import eviet.vlogic.evietandroidapp.fragment.DashboardFragment;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.retrofit.RetrofitService;
import eviet.vlogic.evietandroidapp.retrofit.ServiceResponce;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;
import eviet.vlogic.evietandroidapp.utility.CheckPermissions;
import eviet.vlogic.evietandroidapp.utility.Utility;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener, ServiceResponce {
    private static final String TAG = "ChatActivity";

    private RecyclerView chatList;
    private ChatAdapter adapter;
    private List<Message> messageList = new ArrayList();
    private Socket mSocket;
    private EditText messageEditText;
    private String roomId = "";
    private boolean mTyping = false;
    private Handler mTypingHandler = new Handler();
    private Animation animFadeIn = null;
    private Animation animFadeOut = null;
    private TextView toolTitle;
    private ImageView sendMessage, img_sync;
    private String toUser = "admin";
    private int currentPage =1;
    private float total;
    private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
    private SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa", Locale.ENGLISH);
    private int totalItemCount;
    private int visibleItemCount;
    private int scrolledOutItemCount;
    private boolean isScrolling = false;
    private LinearLayoutManager manager;
    private TextView txtTyping;
    private final int bottomScroll = 100;
    private final int topScroll = 200;
    private boolean scrollTop = false;
    private String setUser = "";
    private ImageView imgBack,setting;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ChatApplication app = ((ChatApplication) getApplication());
        mSocket = app.getmSocket();

        mSocket.on("set-room", gettingRoomId);
        mSocket.on("chat-msg", onNewMessage);
        mSocket.on("typing", onTyping);

        if (!mSocket.connected()) {

            mSocket.connect();
            Log.d(TAG, mSocket.connected() + "scope");

        }

        mSocket.connect();

        setContentView(R.layout.activity_chat);

        messageList.clear();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (CheckPermissions.checkLocationPermission(ChatActivity.this)) {

            } else {

                CheckPermissions.requestLocationPermission(ChatActivity.this, 202);

            }

        }

        getOrCreateUser();
        setting = findViewById(R.id.setting);
        imgBack = findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.ic_back);
        imgBack.setOnClickListener(this);
//        setting.setImageResource(R.drawable.ic_add);

        toolTitle = findViewById(R.id.toolTitle);
        chatList = findViewById(R.id.chatList);
        messageEditText = findViewById(R.id.messageEditText);
        txtTyping = findViewById(R.id.txtTyping);
        txtTyping.setBackground(new ColorDrawable(Color.TRANSPARENT));
        txtTyping.setVisibility(View.GONE);
        sendMessage = findViewById(R.id.sendMessage);

        if(PreferencesHelper.getValue()!=null){
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                toolTitle.setText("Chatroom");
                messageEditText.setHint(getString(R.string.enterTheMessage_eng));

            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                toolTitle.setText("Câu hỏi");
                messageEditText.setHint(getString(R.string.enterTheMessage_viet));
            }
        }

//      img_sync = findViewById(R.id.img_sync);

        manager = new LinearLayoutManager(this);

        manager.setOrientation(LinearLayoutManager.VERTICAL);
       //manager.setSmoothScrollbarEnabled(true);
       // manager.setReverseLayout(true);
      //  manager.setStackFromEnd(true);

        chatList.setLayoutManager(manager);
        adapter = new ChatAdapter(messageList);
        chatList.setAdapter(adapter);
        chatList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = manager.getItemCount();
                visibleItemCount = manager.getChildCount();
//                scrolledOutItemCount = manager.findFirstVisibleItemPosition();
                scrolledOutItemCount = manager.findFirstVisibleItemPosition();
                Log.d(TAG, visibleItemCount + "//" + totalItemCount + "//" + scrolledOutItemCount);
                if (dy < 0) {
                    scrollTop = true;
                    if (isScrolling && scrolledOutItemCount == 2) {
                        Log.e(TAG, currentPage + "kokok1" + total);
                        if (currentPage < total) {
                            Log.d(TAG, "ok");
                            currentPage++;
                            getOldChats(roomId);
                        }
                    }
                }
            }
        });


        messageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!mSocket.connected()) return;

                Log.d("typng", "emitTyping");
                mSocket.emit("typing");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });
        sendMessage.setOnClickListener(this);
//        img_sync.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");


        if (!mSocket.connected()) {
            mSocket.connect();
            Log.d(TAG, "onStart: "+mSocket.connect());

        }
        mSocket.emit("set-user-data", setUser);


    }


    private void getOrCreateUser() {
        Log.d(TAG, "username //" + PreferencesHelper.getUserName() + "");

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userId", PreferencesHelper.getUserId());
        jsonObject.addProperty("username", PreferencesHelper.getUserName());
        jsonObject.addProperty("deviceToken", PreferencesHelper.getDeviceToken());

        new RetrofitService(this, ServiceUrls.URL_GET_OR_CREATE_USER, 5, 1, jsonObject, true, this).callService(true);
    }
//sockt
    private void attemptSend() {
        if (!mSocket.connected()) {
            mSocket.connect();
        }
        String message = messageEditText.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            messageEditText.requestFocus();
            return;
        }
        messageEditText.setText("");
        if(PreferencesHelper.getValue()!=null){
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                messageEditText.setHint(getString(R.string.enterTheMessage_eng));
            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                messageEditText.setHint(getString(R.string.enterTheMessage_viet));
            }
        }
        String time = Calendar.getInstance().getTime().toString();
        Log.d(TAG, "time//" + Calendar.getInstance().getTime());
        String setTime = new SimpleDateFormat("hh:mm aa", Locale.ENGLISH).format(Calendar.getInstance().getTime());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("msg", message);
            jsonObject.put("msgTo", toUser);
            jsonObject.put("date", time);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, jsonObject.toString() + "   emit this");
        addMessage(setTime, message, Message.TYPE_MESSAGE_LOCAL, bottomScroll);
        mSocket.emit("chat-msg", jsonObject);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        mSocket.off("set-room", gettingRoomId);
        mSocket.off("chat-msg", onNewMessage);
        mSocket.off("typing", onTyping);

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
        if (mSocket.connected()) {
            mSocket.disconnect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


        if (!mSocket.connected()) {
            mSocket.connect();
        }

        Log.d(TAG, mSocket.connected() + "//" + mSocket);

    }

    private Emitter.Listener gettingRoomId = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            ChatActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    roomId = args[0].toString();
                    Log.d(TAG, args[0] + "roomid");
                    getOldChats(roomId);

                }
            });
        }
    };

    private void getOldChats(String roomId) {
       // Log.d(TAG, "getOldChats: "+roomId);
        Log.e("pageno",currentPage+roomId);
        new RetrofitService(this, ServiceUrls.URL_GET_OLD_CHATS + roomId + "&pageSize=20&pageNo=" +currentPage
                ,1, 2, new JsonObject(), true, this).callService(true);
    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            ChatActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    JSONObject jsonObject = (JSONObject) args[0];
                    Log.d("onnewmsg", "//" + jsonObject.toString());
                    if (jsonObject.has("msgFrom") && jsonObject.optString("msgFrom").equalsIgnoreCase(toUser)) {
                        Log.d(TAG, "nwmsg" + jsonObject.toString() + " remote message");
                        String time;
                        String message;
                        String finalDate;
                        try {

                            time = jsonObject.getString("date");
                            message = jsonObject.getString("msg");


                        } catch (JSONException e) {
                            Log.d("nwmsgex", "new Message exception ");
                            return;

                        }
                        Date date = null;
                        try {
                            SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
                            date = df1.parse(time);
                            finalDate = sdf.format(date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                            Log.d(TAG, e.getMessage());
                            return;
                        }

                        Log.d(TAG, "nwmsg" + finalDate + "//" + message);


                        addMessage(finalDate, message, Message.TYPE_MESSAGE_REMOTE, bottomScroll);

                    }

                }
            });
        }
    };

    private void addMessage(String time, String message, int type, int scrollTo) {

        Log.d(TAG, "addMessage");
        messageList.add(messageList.size(),new Message.Builder(type).time(time).message(message).build());
       // adapter.notifyItemInserted(messageList.size() - 1);
        adapter.notifyDataSetChanged();
        if (scrollTo == bottomScroll) {
            scrollToBottom();
        }

    }

    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.d("typng", "onTyping");
            if (ChatActivity.this != null)
                ChatActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("typng", args[0] + "");
                        String typingMess = args[0].toString();

                        animFadeIn = AnimationUtils.loadAnimation(ChatActivity.this, R.anim.fade_in);
                        animFadeOut = AnimationUtils.loadAnimation(ChatActivity.this, R.anim.fade_out);

                        txtTyping.setAnimation(animFadeIn);
                        txtTyping.setVisibility(View.VISIBLE);

                        if (!mTyping)
                            mTyping = true;

                        if (mTyping) {

                            mTypingHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    txtTyping.setVisibility(View.GONE);
                                    txtTyping.setAnimation(animFadeOut);
                                    mTyping = false;

                                }

                            }, 1500);
                        }

                    }

                });

        }
    };

    private void scrollToBottom() {
        chatList.scrollToPosition(messageList.size()-1);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.sendMessage:

                attemptSend();

                break;

            case R.id.imgBack:
                onBackPressed();

//                Utility.changeFragment(ChatActivity.this,new DashboardFragment());

                break;

        }

    }

    @Override
    public void onServiceResponce(String result, int requestCode) {
        Log.e("ressss", requestCode + "//" + result);
        if (requestCode == 2) {
            Log.d(TAG, "old chat//" + result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("isSuccess") && jsonObject.optBoolean("isSuccess")) {
                    total = Float.valueOf(jsonObject.optString("total")) / Float.valueOf(jsonObject.optString("pageSize"));
                    Log.d(TAG, "totalPage//" + total);
                    JSONArray jsonArray = jsonObject.optJSONArray("items");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Message message = new Message();
                        JSONObject obj = jsonArray.optJSONObject(i);
                        message.setFromMess(obj.optString("fromMsg"));
                        message.setToMess(obj.optString("toMsg"));
                        message.setmMessage(obj.optString("msg"));

                        String createdOn = obj.optString("createdOn");

                        Date date = df.parse(createdOn);
                        String finalDate = sdf.format(date);
                        message.setmTime(finalDate);

                        if (obj.optString("fromMsg").equalsIgnoreCase(PreferencesHelper.getUserName())) {
                            message.setmType(Message.TYPE_MESSAGE_LOCAL);

                        } else if (obj.optString("fromMsg").equalsIgnoreCase(toUser)) {
                            message.setmType(Message.TYPE_MESSAGE_REMOTE);

                        }

                        messageList.add(0,message);
                       // messageList.add(0, message);
                    }
                    if (!messageList.isEmpty()) {
                        adapter.notifyDataSetChanged();
                        if (scrollTop) {

                            scrollTop = false;
                        } else {
                            scrollToBottom();
                        }


                    }


                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
                Log.d(TAG, "exception//" + e.getMessage());
            }
        }

        if (requestCode == 1) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optBoolean("isSuccess")) {
                    JSONObject obj = jsonObject.optJSONObject("message");
                    String username = obj.optString("username");
                    mSocket.emit("set-user-data", username);
                    setUser = username;
                    Log.d(TAG, "userName // " + username);

                    String currentRoom = username + "-" + toUser;
                    String reverseRoom = toUser + "-" + username;
                    JSONObject sendJson = new JSONObject();
                    try {
                        sendJson.accumulate("name1", currentRoom);
                        sendJson.accumulate("name2", reverseRoom);

                        // sendJson.put("name1",currentRoom);
                        // sendJson.put("name2",reverseRoom);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Log.d(TAG, jsonObject.toString());


                    mSocket.emit("set-room", sendJson);
                    Log.d(TAG, "set room/" + sendJson.toString());


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


    }
}
