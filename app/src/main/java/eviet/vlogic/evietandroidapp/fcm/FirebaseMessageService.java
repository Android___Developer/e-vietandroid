package eviet.vlogic.evietandroidapp.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.LanguageSelectionActivity;

/**
 * Created by VLOGIC on 5/30/2019.
 */

public class FirebaseMessageService{

} /*extends FirebaseMessagingService {
    private Map<String, String> data;
    private String message, title, type;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.e("fcmmsg", remoteMessage.getFrom() + "//" + remoteMessage.getData() + "//" + remoteMessage.getNotification());

        data = remoteMessage.getData();
        message = data.get("body");
        title = data.get("title");
        type = data.get("type");

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(getResources().getString(R.string.app_name));

        Intent intent = new Intent(this, LanguageSelectionActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.avatar);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"0")
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(getNotificationIcon())
                .setLargeIcon(largeIcon)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent)
                .setStyle(inboxStyle)
                .setAutoCancel(true)
                .setWhen(0);

        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
        notificationManager.notify(0, builder.build());

    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.avatar : R.drawable.avatar;
    }


    @Override
    public void onCreate() {
        super.onCreate();
    }

}*/
