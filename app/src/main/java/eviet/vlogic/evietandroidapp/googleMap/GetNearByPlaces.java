package eviet.vlogic.evietandroidapp.googleMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import eviet.vlogic.evietandroidapp.R;

public class GetNearByPlaces extends AsyncTask<Object,String,String> {
    private Context context;
    GoogleMap mMap;
    String url,data;
    InputStream inputStream;
    BufferedReader bufferedReader;
    StringBuilder stringBuilder;
    HttpURLConnection httpURLConnection;

    public GetNearByPlaces(Context context) {
        this.context= context;
    }

    @Override
    protected String doInBackground(Object... params) {
        mMap= (GoogleMap) params[0];
        url = (String)params[1];

        inputStream = null;
        httpURLConnection = null;

        try {
            URL myUrl = new URL(url);
            httpURLConnection = (HttpURLConnection) myUrl.openConnection();
            httpURLConnection.connect();
            inputStream = httpURLConnection.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line = "";
            stringBuilder = new StringBuilder();
            while ((line= bufferedReader.readLine())!=null)
            {
             stringBuilder.append(line);
            }

            data = stringBuilder.toString();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    @Override
    protected void onPostExecute(String data) {
        super.onPostExecute(data);
        try {
            JSONObject parentObject = new JSONObject(data);
            Log.e("find_results",data);
            JSONArray resultJsonArray = parentObject.getJSONArray("results");
            for (int i= 0; i<resultJsonArray.length(); i++){
                JSONObject jsonObject = resultJsonArray.getJSONObject(i);
                JSONObject locationObject = jsonObject.getJSONObject("geometry").getJSONObject("location");

                String latitude = locationObject.getString("lat");
                String longitude = locationObject.getString("lng");

                JSONObject nameObject = resultJsonArray.getJSONObject(i);

                String nameRestaurant = nameObject.getString("name");
                String vicinity = nameObject.getString("vicinity");
                LatLng latLng = new LatLng(Double.parseDouble(latitude),Double.parseDouble(longitude));

                mMap.addMarker(new MarkerOptions().position(latLng).title(nameRestaurant).draggable(true));

//                mMap.addMarker(new MarkerOptions().position(latLng).title(nameRestaurant)
//                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_food_marker))
//                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_pin))
//                        .icon(bitmapDescriptorFromVector(context, R.drawable.ic_camera))
//                        .draggable(false));

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
