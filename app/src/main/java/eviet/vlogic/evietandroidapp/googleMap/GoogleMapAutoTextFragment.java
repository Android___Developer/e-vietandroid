package eviet.vlogic.evietandroidapp.googleMap;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.BottomActivity;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class GoogleMapAutoTextFragment extends Fragment implements OnMapReadyCallback {

    GoogleMap mMap;
    private PlaceAutocompleteFragment place1;
    private FusedLocationProviderClient mFusedLocationClient;
    private SupportMapFragment mapFragment;
    private Location mCurrentLocation;
    private double latitude,longitude;
    private LatLng currentLocation;
    private Button findRestaurant;
    private CardView findRestaurantCard;
    private ImageView imgLocationPinUp;
    private TextView txtInfoFrag;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private int value = 0;
    private BottomSheetBehavior mBottomSheetBehavior;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_google_map_auto_text, container, false);
//        ((BottomActivity) getActivity()).navigation.setVisibility(View.GONE);
        imgLocationPinUp = rootView.findViewById(R.id.imgLocationPinUp);
        txtInfoFrag = rootView.findViewById(R.id.txtInfoFrag);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapFrag);
        FragmentManager fm = getChildFragmentManager();

        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.fragment_container, mapFragment).commit();
            fm.executePendingTransactions();
        }

      /*mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.addMarker(new MarkerOptions().position(latLng).title(""));

            }
        });*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();
        } else {
            getCurrentLocation();
        }

        mapFragment.getMapAsync(this);
        findRestaurant = rootView.findViewById(R.id.findRestaurant);
        findRestaurantCard = rootView.findViewById(R.id.findRestaurantCard);
        findRestaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findRestaurants();
            }
        });

        findRestaurantCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findRestaurants();
            }
        });


        place1 = (PlaceAutocompleteFragment) getActivity().getFragmentManager().findFragmentById(R.id.placeFrag);
        place1.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                AddPlace(place, 1);
            }

            @Override
            public void onError(Status status) {
            }
        });

        mBottomSheetBehavior = BottomSheetBehavior.from(rootView.findViewById(R.id.bottom_sheet));
        mBottomSheetBehavior.setPeekHeight(200);
        mBottomSheetBehavior.setHideable(false);

        if (((BottomActivity)getActivity()).navigation != null) {
            mBottomSheetBehavior.setPeekHeight(((BottomActivity) getActivity()).navigation.getHeight() + 90);

        }

        // set hideable or not

        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                Log.e("state",newState+""+bottomSheet);
                if(newState==BottomSheetBehavior.STATE_COLLAPSED){
                    Log.e("state1",newState+""+bottomSheet);
//                    mBottomSheetBehavior.setPeekHeight(90);
                }else{
                    Log.e("state2",newState+""+bottomSheet);
                   // mBottomSheetBehavior.setHideable(true);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                bottomSheet.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                mBottomSheetBehavior.setHideable(false);
            }
        });

        return rootView;

    }

    public void findRestaurants() {

        if (mMap != null)
            mMap.clear();
        Log.e("LATLONG1",latitude+"");
        Log.e("LATLONG2",longitude+"");

        StringBuilder stringBuilder = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        stringBuilder.append("&location=" + latitude + "," + longitude);
        stringBuilder.append("&radius=" + 1000);
        stringBuilder.append("&keyword=" + "restaurant");
        stringBuilder.append("&key=" + getResources().getString(R.string.google_places_key));
        String url = stringBuilder.toString();

        Object dataTransfer[] = new Object[2];
        dataTransfer[0] = mMap;
        dataTransfer[1] = url;
        GetNearByPlaces getNearByPlaces = new GetNearByPlaces(getActivity().getApplicationContext());
        getNearByPlaces.execute(dataTransfer);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity() != null) {
            Log.e("res", "place dlted");
            android.app.FragmentManager fragmentManager = getActivity().getFragmentManager();
            android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(place1);
            fragmentTransaction.commit();
            //Use commitAllowingStateLoss() if getting exception

            place1 = null;
        }

    }

    private void checkPermission() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
        } else {
            getCurrentLocation();

        }
    }

    private void getCurrentLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {

                if (location != null) {
                    Log.e("location", location.getLatitude() + "\\" + location.getLongitude() + "okok");
                    getMyLoc(location);
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

//                  latitude =location.getLatitude();
//                  longitude =location.getLongitude();
                    Log.e("LAT", latitude + "\n" + longitude);
                    Log.e("getLastLocation", location.toString());
                    Log.e("CurrentLocation", "getLatitude" + location.getLatitude() + "\n" + "getLongitude" + location.getLongitude() + "");

                }
            }

        });

    }

    private void getMyLoc(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
//      Log.e("loc", location.getLatitude() + "//" + location.getLongitude());
        mapFragment.getMapAsync(this);
    }

    private void requestPermission() {
        requestPermissions(new String[]{ACCESS_FINE_LOCATION}, REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    public void AddPlace(Place place, int pno) {
        try {
            if (mMap == null) {
                Toast.makeText(getActivity(), "Please check your API key for Google Places SDK and your internet conneciton", Toast.LENGTH_LONG).show();
            } else {
                mMap.clear();
//              mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 14));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 14.0f));
                //mMap.addMarker(new MarkerOptions().position(place.getLatLng()).title("Name:" + place.getName() + ". Address:" + place.getAddress()));
//                txtInfo.setText("Name:" + place.getName() + ". Address:" + place.getAddress());

            }
        } catch (Exception ex) {

            if (ex != null) {
                Toast.makeText(getActivity(), "Error:" + ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            getCurrentLocation();

        } else {

            Toast.makeText(getActivity(), "Permission denied", Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
//      Log.e("latLog", latitude + "\n" + longitude + "");
//      mMap.clear();
        currentLocation = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(currentLocation).title("current location").draggable(true));
//      .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin)));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 14.0f));
        setOnCameraMoveListner();
        setOnCameraIdleListener();

       /*if(mMap != null) {
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {

                    txtInfoFrag.setText(marker.getTitle().toString() + " Lat:" + marker.getPosition().latitude + " Long:" + marker.getPosition().longitude);
                    return false;
                }
            });
        }*/

    }

    private void setOnCameraMoveListner() {
        mMap.clear();
//      imgLocationPinUp.setVisibility(View.VISIBLE);
    }

    private void setOnCameraIdleListener() {
//        imgLocationPinUp.setVisibility(View.GONE);
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
//                mMap.clear();

                //get latlng at the center by calling
                LatLng midLatLng = mMap.getCameraPosition().target;
                Log.e("midLatLng", midLatLng.toString());

                Double latt1 = midLatLng.latitude;
                Double logg1 = midLatLng.longitude;
                latitude = latt1;
                longitude = logg1;

                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                List<Address> addresses = null;
                try {
                    addresses = geocoder.getFromLocation(latt1, logg1, 1);
                    if (!addresses.isEmpty()) {
                        String cityNamee = addresses.get(0).getAddressLine(0);
                        String stateName = addresses.get(0).getAddressLine(1);
                        String countryName = addresses.get(0).getAddressLine(2);

                        txtInfoFrag.setText(cityNamee);
                        Log.e("cityNamee", cityNamee);
//                    mMap.addMarker(new MarkerOptions().position(midLatLng).title(cityNamee));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


    }


}
