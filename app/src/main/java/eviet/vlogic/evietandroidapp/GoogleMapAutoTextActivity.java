package eviet.vlogic.evietandroidapp;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GoogleMapAutoTextActivity extends AppCompatActivity {
    private GoogleMap mMap;
    PlaceAutocompleteFragment place1;
    TextView txtInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_map_auto_text);
//        txtInfo= findViewById(R.id.txtInfo);
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
//        place1 = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place1);
//
//        place1.setOnPlaceSelectedListener(new PlaceSelectionListener() {
//            @Override
//            public void onPlaceSelected(Place place) {
//                AddPlace(place, 1);
//            }
//
//            @Override
//            public void onError(Status status) {
//            }
//        });

    }

//    public void AddPlace(Place place, int pno) {
//        try {
//            if (mMap == null) {
//                Toast.makeText(GoogleMapAutoTextActivity.this, "Please check your API key for Google Places SDK and your internet conneciton", Toast.LENGTH_LONG).show();
//            } else {
//
//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 4));
//
//                mMap.addMarker(new MarkerOptions().position(place.getLatLng()).title("Name:" + place.getName() + ". Address:" + place.getAddress()));
//                txtInfo.setText("Name:" + place.getName() + ". Address:" + place.getAddress());
//
//            }
//        } catch (Exception ex) {
//
//            if (ex != null) {
//                Toast.makeText(GoogleMapAutoTextActivity.this, "Error:" + ex.getMessage().toString(), Toast.LENGTH_LONG).show();
//            }
//        }
//    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//
//        if(mMap!=null)
//        {
//            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//                @Override
//                public boolean onMarkerClick(Marker marker) {
//                    txtInfo.setText(marker.getTitle().toString() + " Lat:" + marker.getPosition().latitude + " Long:" + marker.getPosition().longitude);
//                    return false;
//                }
//            });
//        }
//    }

}