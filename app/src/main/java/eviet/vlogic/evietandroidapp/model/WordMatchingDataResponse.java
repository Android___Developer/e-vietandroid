package eviet.vlogic.evietandroidapp.model;

/**
 * Created by VLOGIC on 5/22/2019.
 */

public class WordMatchingDataResponse {
    private String total_questions;
    private String total_nbrof_ans;
    private String total_matching_word_question;
    private String total_matching_answered;

    public String getTotal_nbrof_ans() {
        return total_nbrof_ans;
    }

    public void setTotal_nbrof_ans(String total_nbrof_ans) {
        this.total_nbrof_ans = total_nbrof_ans;
    }

    public String getTotal_questions() {
        return total_questions;
    }

    public void setTotal_questions(String total_questions) {
        this.total_questions = total_questions;
    }

    public String getTotal_matching_word_question() {
        return total_matching_word_question;
    }

    public void setTotal_matching_word_question(String total_matching_word_question) {
        this.total_matching_word_question = total_matching_word_question;
    }

    public String getTotal_matching_answered() {
        return total_matching_answered;
    }

    public void setTotal_matching_answered(String total_matching_answered) {
        this.total_matching_answered = total_matching_answered;
    }
}
