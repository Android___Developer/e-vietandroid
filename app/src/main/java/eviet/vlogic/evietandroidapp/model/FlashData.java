package eviet.vlogic.evietandroidapp.model;

/**
 * Created by VLOGIC on 5/14/2019.
 */

public class FlashData {
    private String card_id;
    private String name;
    private String image;
    private boolean status ;

    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
