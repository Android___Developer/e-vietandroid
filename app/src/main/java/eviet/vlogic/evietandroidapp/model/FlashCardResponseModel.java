package eviet.vlogic.evietandroidapp.model;


import java.util.ArrayList;

public class FlashCardResponseModel {

    private String total_nbrof_questions;
    private String total_nbrof_answered;
    private String total_flashcard_questions;
    private String total_flashcard_answered;
    private String q_id;
    private String question;

    public String getTotal_nbrof_questions() {
        return total_nbrof_questions;
    }

    public void setTotal_nbrof_questions(String total_nbrof_questions) {
        this.total_nbrof_questions = total_nbrof_questions;
    }

    public String getTotal_nbrof_answered() {
        return total_nbrof_answered;
    }

    public void setTotal_nbrof_answered(String total_nbrof_answered) {
        this.total_nbrof_answered = total_nbrof_answered;
    }

    private ArrayList<FlashData> flashDatas;

    public ArrayList<FlashData> getFlashDatas() {
        return flashDatas;
    }

    public void setFlashDatas(ArrayList<FlashData> flashDatas) {
        this.flashDatas = flashDatas;
    }

    public String getQ_id() {
        return q_id;
    }

    public void setQ_id(String q_id) {
        this.q_id = q_id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getTotal_flashcard_questions() {
        return total_flashcard_questions;
    }

    public void setTotal_flashcard_questions(String total_flashcard_questions) {
        this.total_flashcard_questions = total_flashcard_questions;
    }

    public String getTotal_flashcard_answered() {
        return total_flashcard_answered;
    }

    public void setTotal_flashcard_answered(String total_flashcard_answered) {
        this.total_flashcard_answered = total_flashcard_answered;
    }
}
