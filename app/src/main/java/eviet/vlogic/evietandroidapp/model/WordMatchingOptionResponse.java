package eviet.vlogic.evietandroidapp.model;

/**
 * Created by VLOGIC on 5/14/2019.
 */

public class WordMatchingOptionResponse {
    private String opt_id;
    private String options;

    public String getOpt_id() {
        return opt_id;
    }

    public void setOpt_id(String opt_id) {
        this.opt_id = opt_id;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }
}
