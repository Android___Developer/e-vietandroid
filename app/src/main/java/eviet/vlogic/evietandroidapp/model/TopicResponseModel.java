package eviet.vlogic.evietandroidapp.model;

/**
 * Created by VLOGIC on 5/10/2019.
 */

public class TopicResponseModel {
    private String topic_id;
    private String topic_name;
    private String topic_language;
    private String topic_image;

    public TopicResponseModel() {
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getTopic_name() {
        return topic_name;
    }

    public void setTopic_name(String topic_name) {
        this.topic_name = topic_name;
    }

    public String getTopic_language() {
        return topic_language;
    }

    public void setTopic_language(String topic_language) {
        this.topic_language = topic_language;
    }

    public String getTopic_image() {
        return topic_image;
    }

    public void setTopic_image(String topic_image) {
        this.topic_image = topic_image;
    }
}
