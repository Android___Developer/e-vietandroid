package eviet.vlogic.evietandroidapp.model;

public class MessageModel {
    private String receiverId;
    private String receiverName;
    private String receiverAttachement;
    private String receiverTruck;
    private String senderId;
    private String senderName;
    private String senderAttachement;
    private String senderTruck;
    private String userType;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverAttachement() {
        return receiverAttachement;
    }

    public void setReceiverAttachement(String receiverAttachement) {
        this.receiverAttachement = receiverAttachement;
    }

    public String getReceiverTruck() {
        return receiverTruck;
    }

    public void setReceiverTruck(String receiverTruck) {
        this.receiverTruck = receiverTruck;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderAttachement() {
        return senderAttachement;
    }

    public void setSenderAttachement(String senderAttachement) {
        this.senderAttachement = senderAttachement;
    }

    public String getSenderTruck() {
        return senderTruck;
    }

    public void setSenderTruck(String senderTruck) {
        this.senderTruck = senderTruck;
    }


}
