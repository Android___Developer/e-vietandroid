package eviet.vlogic.evietandroidapp.model;

/**
 * Created by VLOGIC on 6/19/2019.
 */

public class ChatUserListModel {
    private String userName;
    private String status;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String toString(){
        return userName+"//"+status;
    }
}
