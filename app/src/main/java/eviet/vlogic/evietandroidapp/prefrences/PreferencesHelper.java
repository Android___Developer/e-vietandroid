package eviet.vlogic.evietandroidapp.prefrences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import eviet.vlogic.evietandroidapp.R;

public class PreferencesHelper {
    private static Context mContext;
    private static SharedPreferences preferences;
    private static PreferencesHelper prefHelper = null;
    private static final String USER_ID = "user_id";
    private static final String USER_NAME = "username";
    private static final String TOPIC_ID = "topic_id";
    private static final String VALUE = "value";
    private static final String USER_NUMBER = "user_number";
    private static final String DEVICE_TOKEN = "user_token";

    public static PreferencesHelper getInstance(Context mcontext) {
        if (prefHelper == null) {
            preferences = PreferenceManager.getDefaultSharedPreferences(mcontext);
            prefHelper = new PreferencesHelper();
        }
        return prefHelper;
    }
    public static String getUserName() {
        return preferences.getString(USER_NAME, null);
    }

    public PreferencesHelper setUserName(String value) {
        preferences.edit().putString(USER_NAME, value).commit();
        return prefHelper;
    }

    public static String getTopicId() {
        return preferences.getString(TOPIC_ID, null);
    }

    public PreferencesHelper setTopicId(String value) {
        preferences.edit().putString(TOPIC_ID, value).commit();
        return prefHelper;
    }

    public static String getValue() {
        return preferences.getString(VALUE, null);
    }

    public PreferencesHelper setValue(String value) {
        preferences.edit().putString(VALUE, value).commit();
        return prefHelper;
    }

    public static String getUserId() {
        return preferences.getString(USER_ID, null);
    }

    public PreferencesHelper setUserId(String value) {
        preferences.edit().putString(USER_ID, value).commit();
        return prefHelper;
    }


    public static String getDeviceToken() {
        return preferences.getString(DEVICE_TOKEN, "");
    }

    public PreferencesHelper setDeviceToken(String value) {
        preferences.edit().putString(DEVICE_TOKEN, value).commit();
        return prefHelper;
    }

    public static String getUserNumber() {
        return preferences.getString(USER_NUMBER, null);
    }

    public PreferencesHelper setUserNumber(String value) {
        preferences.edit().putString(USER_NUMBER, value).commit();
        return prefHelper;
    }


    public static void clearPreferencess() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(USER_ID);
        editor.remove(USER_NUMBER);
        editor.clear();
        editor.commit();

    }

    public static void clearValueFromPreferences() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(VALUE);
        editor.clear();
        editor.commit();

    }
}