package eviet.vlogic.evietandroidapp.application;

/**
 * Created by VLOGIC on 6/19/2019.
 */

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.io.File;
import java.net.URISyntaxException;
import eviet.vlogic.evietandroidapp.receiver.MyNetworkReceiver;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;


public class ChatApplication extends Application {
    private final String TAG="ChatApplication";
    private Socket mSocket;
    public  static Activity mActivity;
    private MyNetworkReceiver mNetworkReceiver;

    {
        try {
            mSocket = IO.socket(ServiceUrls.URL_CHAT_SERVER);
        } catch (URISyntaxException e)

        {
            throw new RuntimeException(e);
        }

    }

    public Socket getmSocket() {
        return mSocket;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
//                mNetworkReceiver=new MyNetworkReceiver();

            }

            @Override
            public void onActivityStarted(Activity activity) {
//                mActivity=activity;


            }

            @Override
            public void onActivityResumed(Activity activity) {
//                Log.d(TAG,mActivity.getLocalClassName());
//                mActivity=activity;
//                registerBroadcastForNaugot();

            }

            @Override
            public void onActivityPaused(Activity activity) {
//                mActivity=null;
//                unregisterReceiver(mNetworkReceiver);

            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        deleteCache(this);
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {

        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

   /* private void registerBroadcastForNaugot() {
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
            registerReceiver(mNetworkReceiver,new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        }
    }*/

    @Override
    public void registerActivityLifecycleCallbacks(ActivityLifecycleCallbacks callback) {
        super.registerActivityLifecycleCallbacks(callback);
    }

}
