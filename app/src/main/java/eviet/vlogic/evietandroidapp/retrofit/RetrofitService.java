package eviet.vlogic.evietandroidapp.retrofit;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import eviet.vlogic.evietandroidapp.fragment.EditProfileFragment;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.facebook.GraphRequest.TAG;

public class RetrofitService {

    private Context mContext;
    private String mUrl;
    private int mValue, mRequestCode;
    private MultipartBody.Part photoPart;
    private HashMap<String, RequestBody> resBody;
    //    private HashMap<String, RequestBody> mMap;
    private ServiceResponce mResponce;
    private Call<ResponseBody> mCall;
    private ArrayList<MultipartBody.Part> mImageList;
    //private ProgressDialog dialog;
    private String mHeader;
    private String mUserName;
    private String password;
    private String token;
    private String tokenType = "";
    private MultipartBody.Part bolPart;
    private MultipartBody.Part ticketPart;
    private JsonObject mJSonObject;
    private MultipartBody photopart;
    private boolean forChat;

    //for get request
    public RetrofitService(Context mContext, String mUrl, int mValue, int mRequestCode, ServiceResponce mResponce) {
        this.mContext = mContext;
        this.mUrl = mUrl;
        this.mValue = mValue;
        this.mRequestCode = mRequestCode;
        this.mResponce = mResponce;
    }


    //for login
    public RetrofitService(Context mContext, String mUrl, int mValue, int mRequestCode, ServiceResponce mResponce, String mUserName, String password) {
        this.mContext = mContext;
        this.mUrl = mUrl;
        this.mValue = mValue;
        this.mRequestCode = mRequestCode;
        this.mResponce = mResponce;
        this.mUserName = mUserName;
        this.password = password;
    }

    // for chat
    public RetrofitService(Context mContext, String mUrl, int mValue, int mRequestCode, JsonObject mJSonObject,boolean forChat,ServiceResponce mResponce) {
        this.mContext = mContext;
        this.mUrl = mUrl;
        this.mValue = mValue;
        this.mRequestCode = mRequestCode;
        this.mResponce = mResponce;
        this.mJSonObject = mJSonObject;
        this.forChat=forChat;
    }


    //for post request by Ashu for requestbody
    public RetrofitService(Context mContext, String tokenType, String token, String mUrl, int mValue, int mRequestCode, HashMap<String, RequestBody> mMap, ServiceResponce mResponce) {
        Log.e("vvv", tokenType + "" + token + "" + mUrl + "" + mValue + "," + mRequestCode + "" + mMap + "");
        this.mContext = mContext;
        this.mUrl = mUrl;
        this.mValue = mValue;
        this.mRequestCode = mRequestCode;
        this.resBody = mMap;
        this.mResponce = mResponce;
        this.token = token;
        this.tokenType = tokenType;

    }
//single image ayushi

    public RetrofitService(Context mContext, String mUrl, int mValue, int mRequestCode,
                           HashMap<String, RequestBody> resBody, MultipartBody.Part photoPart, ServiceResponce mResponce) {

        this.mContext=mContext;
        this.mUrl=mUrl;
        this.mValue=mValue;
        this.mRequestCode=mRequestCode;
        this.resBody = resBody;
        this.mResponce = mResponce;
        this.photoPart = photoPart;


    }

    //for post image request
    public RetrofitService(Context mContext, String tokenType, String token, String mUrl,MultipartBody.Part photoPart,
                           int mValue, int mRequestCode, HashMap<String, RequestBody> mMap, ArrayList<MultipartBody.Part> mImageList, ServiceResponce mResponce) {
        this.mContext = mContext;
        this.mUrl = mUrl;
        this.tokenType = tokenType;
        this.token = token;
        this.mValue = mValue;
        this.mRequestCode = mRequestCode;
        this.resBody = mMap;
        this.mResponce = mResponce;
        this.mImageList = mImageList;
    }

    //for post image request
    public RetrofitService(Context mContext, String tokenType, String token, String mUrl, int mValue, int mRequestCode, HashMap<String, RequestBody> mMap, MultipartBody.Part bolPart, MultipartBody.Part ticketPart, ServiceResponce mResponce) {
        this.mContext = mContext;
        this.mUrl = mUrl;
        this.tokenType = tokenType;
        this.token = token;
        this.mValue = mValue;
        this.mRequestCode = mRequestCode;
        this.resBody = mMap;
        this.bolPart = bolPart;
        this.ticketPart = ticketPart;
        this.mResponce = mResponce;
        Log.e("resss", tokenType + "/" + token + "/" + mUrl + "/" + mValue + "/" + mRequestCode);
    }

//todo for call API of POST type

    public RetrofitService(Context context, String url, int apiType, int requestCode, HashMap<String, RequestBody> body, ServiceResponce responce) {
        this.mContext = context;
        this.mUrl = url;
        this.mValue = apiType;
        this.mRequestCode = requestCode;
        this.resBody = body;
        this.mResponce = responce;
    }



    public void callService(boolean showProgress) {
        final ProgressDialog dialog = new ProgressDialog(mContext);

        if (showProgress) {
            dialog.setMessage("Loading/Tải.....");
            dialog.setCancelable(false);
            dialog.show();
        }

        //todo for time out
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(4, TimeUnit.MINUTES)
                .readTimeout(3, TimeUnit.MINUTES).build();

        Retrofit retrofit;
        if (forChat) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(ServiceUrls.URL_CHAT_BASE)
                    .client(okHttpClient).addConverterFactory(GsonConverterFactory.create()).build();
        } else {
            retrofit = new Retrofit.Builder()
                    .baseUrl(ServiceUrls.BASEURL)
                    .client(okHttpClient).addConverterFactory(GsonConverterFactory.create()).build();
        }


        ServiceClient retrofitApi = retrofit.create(ServiceClient.class);

        if (mValue == 1) {
            mCall = retrofitApi.makeGetRequest(mUrl);
        }

        if (mValue == 2) {
            Log.e("rtytr", resBody.toString() + "//");
            mCall = retrofitApi.makePostRequest(mUrl, resBody);
        }

        if (mValue == 3) {
            mCall = retrofitApi.makePostRequestBody(mUrl, tokenType + " " + token, resBody);
        }

        if (mValue == 4) {

            Log.e("okdlsk", bolPart.toString() + "//" + ticketPart.toString() + "//" + resBody.toString());
            mCall = retrofitApi.makePostImageRequest(mUrl, tokenType + " " + token, resBody, bolPart, ticketPart);
            //mCall = retrofitApi.makePostImageRequest(mUrl, resBody, mImageList);

        }

        if (mValue == 5) {
            mCall = retrofitApi.sendJsonData(mUrl, "application/json", mJSonObject);
        }
        else if (mValue == 6)
        {
            mCall = retrofitApi.makeSingleImgUpload(mUrl,resBody
                    , photoPart);
        }


        mCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.e("rescode", response.code() + "//");

                if (response.isSuccessful()) {
                    try {

                        if (dialog != null && dialog.isShowing()) {
                            dialog.cancel();
                        }

                        String res = response.body().string();
                        Log.e("retrofttt", res + "");
                        mResponce.onServiceResponce(res, mRequestCode);

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                } else {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.cancel();
                    }
                    Toast.makeText(mContext, "Server error", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("fvcfvv", "fcvdfc");

                if (dialog != null && dialog.isShowing()) {

                    dialog.cancel();
                }

            }
        });

    }

    private void alertTimeOut(Call<ResponseBody> call1, Callback<ResponseBody> callback, String message) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callService(true);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        //dialog.show();


    }

}
