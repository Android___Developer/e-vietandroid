package eviet.vlogic.evietandroidapp.retrofit;

import java.util.ArrayList;

public class ServiceUrls {
    public static final String BASEURL = "http://93.188.167.68/projects/eviet/api/";
    public static String BASE_REQ_URL = "https://translate.yandex.net/api/v1.5/tr.json/"; //google trnslat
    public static ArrayList<String> LANGUAGE_CODES = new ArrayList<String>();

    public static final String DEVICE_URL = "login_with_device.php";
    //RegistrationActivity
    public static final String REGISTER_URL = "register.php";
    //loginActivity
    public static final String LOGIN_URL = "login.php"; //1
    public static final String FORGOT_PASSWORD_URL = "forget_pass.php";
    public static final String REGISTER_FACEBOOK_URL = "login_with_facebook.php";
    public static final String GOOGLE_TRANSLATE = "gtranslate.php";
    public static final String TOPIC_VIEW= "view_topics.php";
    public static final String FLASH_CARD= "flash_cards.php";
    public static final String FLASH_CARD_ANS = "ans_card.php";
    public static final String WORD_MATCHING= "matching_word.php";
    public static final String WORD_MATCHING_ANS= "answer_matching_word.php";
    public static final String VIEW_PROFILE= "view_profile.php";
    public static final String EDIT_PROFILE= "edit_profile.php";
    public static final String UPDATE_PROFILE="update_profile.php";
    public static final String VIDEO_VIEW="videos.php";
    public static final String WATCHED_VIDEO="watched_video.php";
    //
    public static final String FORMING_QUESTIONS="view_form_sentence.php";
    public static final String FORMING_ANSWERE="forming_answer.php";
    //Pronunciation
    public static final String PRONUNCIATION_QUESTIONS="view_pronunciation.php";
    public static final String PRONUNCIATION_ANSWERE="pronunciation_answer.php";
    //dashboard
    public static final String DASHBOARD_API="dashboard.php";
    public static final String REGISTER_GMAIL_URL="login_with_gmail.php";//http://93.188.167.68:7500
    public static final String URL_CHAT_SERVER = "http://13.250.96.251:7200";//http://13.250.96.251:7200sockt
    public static final String URL_CHAT_BASE = "http://13.250.96.251:7200/api/";
    public static final String URL_GET_OR_CREATE_USER = "users/createOrGet";
    public static final String URL_GET_OLD_CHAT="messages/getOldChat";
    public static final String URL_GET_OLD_CHATS="messages/getOldChat?room_id=";
}
