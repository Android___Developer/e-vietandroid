package eviet.vlogic.evietandroidapp.retrofit;

import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Url;

public interface ServiceClient {

    @GET
    Call<ResponseBody> makeGetRequest(@Url String url);

    @FormUrlEncoded
    @POST
    Call<ResponseBody> makePostRequestLogin(@Url String url, @Header("Content-Type") String data, @Field("grant_type") String auth, @Field("username") String username, @Field("password") String pass);

    @Multipart
    @POST
    Call<ResponseBody> makePostRequest(@Url String url,@PartMap HashMap<String,RequestBody> body);

    @Multipart
    @POST
    Call<ResponseBody> makePostRequestBody(@Url String url, @Header("Authorization") String auth1, @PartMap HashMap<String, RequestBody> body);

    @Multipart
    @POST
    Call<ResponseBody> makePostImageRequest(@Url String url, @Header("Authorization") String auth1, @PartMap HashMap<String, RequestBody> body, @Part MultipartBody.Part bolPart, @Part MultipartBody.Part ticketPart);

    @POST
    Call<ResponseBody> sendJsonData(@Url String url,@Header("Content-Type") String header, @Body JsonObject body);
   /*@Multipart
    @POST
    Call<ResponseBody> makePostImageRequest(@Url String url, @PartMap HashMap<String,RequestBody> body, @Part ArrayList<MultipartBody.Part> imageList);
*/
   @Multipart
   @POST
   Call<ResponseBody> makeSingleImgUpload(@Url String url, @PartMap Map<String, RequestBody> bodyMap,
                                           @Part MultipartBody.Part photo);

}
