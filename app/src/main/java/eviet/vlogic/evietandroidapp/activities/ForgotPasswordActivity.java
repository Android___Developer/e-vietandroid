package eviet.vlogic.evietandroidapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.fragment.SettingFragment;
import eviet.vlogic.evietandroidapp.retrofit.RetrofitService;
import eviet.vlogic.evietandroidapp.retrofit.ServiceResponce;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;
import eviet.vlogic.evietandroidapp.utility.Utility;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener,ServiceResponce {
    private EditText forgotEmail;
    private Toolbar toolbar;
    private TextView toolTitle;
    private ImageView imgBack;
    private LinearLayout forgotSendButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initillizeId();
        clickedListner();
    }

    private void initillizeId() {
        toolbar = findViewById(R.id.toolbar);
        forgotEmail = findViewById(R.id.forgotEmail);
        forgotSendButton = findViewById(R.id.forgotSendButton); 
        toolTitle = findViewById(R.id.toolTitle);
        toolTitle.setText("Forgot Password");
        imgBack = findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.ic_back);
    }

    private void clickedListner() {
        imgBack.setOnClickListener(this);
        forgotSendButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.forgotSendButton:
                validation();
                break;
        }
    }

    private boolean validation() {
        boolean valid = true;
        String email = forgotEmail.getText().toString().trim();
        if (email.isEmpty() && email.equalsIgnoreCase("")) {
            forgotEmail.setError("Please enter email address");
            valid = false;
        }
        else if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            forgotEmail.setError("Please enter valid email address");
            valid = false;
        }else {
            getLoginUser();
        }
        return valid;
    }

    private void getLoginUser() {
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("email", RequestBody.create(MediaType.parse("multipart/form-data"), forgotEmail.getText().toString().trim()));
        Log.e("bdy",body.toString());
        new RetrofitService(this, ServiceUrls.FORGOT_PASSWORD_URL, 2, 1, body, ForgotPasswordActivity.this).callService(true);
    }

    @Override
    public void onServiceResponce(String result, int requestCode) {
        Log.e("forgot_res", result + "//"+requestCode);
    }
}
