package eviet.vlogic.evietandroidapp.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import eviet.vlogic.evietandroidapp.fragment.AskQuestionFragment;
import eviet.vlogic.evietandroidapp.fragment.DashboardFragment;
import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.fragment.FlashCardApiFragment;
import eviet.vlogic.evietandroidapp.fragment.FormingSentanceFragment;
import eviet.vlogic.evietandroidapp.fragment.MessageFragment;
import eviet.vlogic.evietandroidapp.fragment.NotificationsFragment;
import eviet.vlogic.evietandroidapp.fragment.ProfileFragment;
import eviet.vlogic.evietandroidapp.fragment.PronunciationFragment;
import eviet.vlogic.evietandroidapp.fragment.TopicFragment;
import eviet.vlogic.evietandroidapp.fragment.VideoTutorialsFragment;
import eviet.vlogic.evietandroidapp.fragment.WordsMatchFragment;
import eviet.vlogic.evietandroidapp.socketio.FragmentChatMain;
import eviet.vlogic.evietandroidapp.utility.Utility;

public class BottomActivity extends AppCompatActivity {
    private TextView mTextMessage;
    //Fragment fragment = null;
    public BottomNavigationView navigation;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {

                case R.id.navigation_dashboard:
                    Utility.changeFragment(BottomActivity.this, new DashboardFragment());
                    break;

                case R.id.navigation_chat:
                    Utility.changeFragmentWithString(BottomActivity.this, new AskQuestionFragment(), "Questions");

                    break;

                case R.id.navigation_notifications:
                    Utility.changeFragmentWithString(BottomActivity.this, new NotificationsFragment(), "Notification");

                    break;

                case R.id.navigation_profile:
                    Utility.changeFragmentWithString(BottomActivity.this, new ProfileFragment(), "Profile");
                    break;

            }
            return true;

        }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom);
        Utility.changeFragment(BottomActivity.this, new DashboardFragment());

        if (savedInstanceState == null) {

        }

        mTextMessage = findViewById(R.id.message);
        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        BottomNavigationViewHelper.disableShiftMode(navigation);
//        navigation.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.e("back", getSupportFragmentManager().getFragments().toString() + "//");
        Log.e("back1", getSupportFragmentManager().getBackStackEntryCount() + "//");
        Log.e("instance",(getSupportFragmentManager().getFragments() instanceof DashboardFragment)+"//");
        Log.e("navvv",(getSupportFragmentManager().findFragmentById(R.id.fragment_container))+"//");
        Log.e("navv45",(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof DashboardFragment)+"//");

        if(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof DashboardFragment){
            navigation.setSelectedItemId(R.id.navigation_dashboard);

        }else if(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof TopicFragment){
            navigation.setSelectedItemId(R.id.navigation_dashboard);
        }else if(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof FlashCardApiFragment){
            navigation.setSelectedItemId(R.id.navigation_dashboard);
        }else if(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof WordsMatchFragment){
            navigation.setSelectedItemId(R.id.navigation_dashboard);
        }else if(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof FormingSentanceFragment){
            navigation.setSelectedItemId(R.id.navigation_dashboard);
        }else if(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof PronunciationFragment){
            navigation.setSelectedItemId(R.id.navigation_dashboard);
        }else if(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof VideoTutorialsFragment){
            navigation.setSelectedItemId(R.id.navigation_dashboard);

        }else if(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof AskQuestionFragment){
            navigation.setSelectedItemId(R.id.navigation_chat);
        }else if(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof MessageFragment){
            navigation.setSelectedItemId(R.id.navigation_chat);
        }else if(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof FragmentChatMain){
            navigation.setSelectedItemId(R.id.navigation_chat);
        }else if(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof NotificationsFragment){
            navigation.setSelectedItemId(R.id.navigation_notifications);
        }else if(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof ProfileFragment){
            navigation.setSelectedItemId(R.id.navigation_profile);
        }

    }

}
