package eviet.vlogic.evietandroidapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.provider.Settings.Secure;
import android.widget.Toast;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.rilixtech.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.retrofit.RetrofitService;
import eviet.vlogic.evietandroidapp.retrofit.ServiceResponce;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;
import eviet.vlogic.evietandroidapp.utility.Utility;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener, ServiceResponce,GoogleApiClient.OnConnectionFailedListener {
    private Toolbar toolbar;
    private TextView toolTitle;
    CountryCodePicker ccp;
    String m_androidId = "";
    private EditText signupUsername, signupEmail, signupPassword, signupConfirmPass, signupMobile;
    private Button signupButton, fbLoginButton, googleLoginButton, LoginButton;
    private TextView loginAccount;
    private Intent intent;
    private CallbackManager callbackManager;
    private GoogleApiClient googleApiClient;
    private static final int REQ_CODE= 9001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(RegistrationActivity.this);
        setContentView(R.layout.activity_registration);
        callbackManager = CallbackManager.Factory.create();
        initializeId();
        setClickListner();
        m_androidId = new Utility().deviceId(this);

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this,this).addApi(Auth.GOOGLE_SIGN_IN_API,googleSignInOptions).build();
    }

    private void initializeId() {
        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        toolbar = findViewById(R.id.toolbar);
        toolTitle = findViewById(R.id.toolTitle);
        toolTitle.setText("Create Profile");
        signupUsername = findViewById(R.id.signupUsername);
        signupEmail = findViewById(R.id.signupEmail);
        signupPassword = findViewById(R.id.signupPassword);
        signupConfirmPass = findViewById(R.id.signupConfirmPass);
        signupMobile = findViewById(R.id.signupMobile);
        signupButton = findViewById(R.id.signupButton);
        fbLoginButton = findViewById(R.id.fbLoginButton);
        googleLoginButton = findViewById(R.id.googleLoginButton);
        LoginButton = findViewById(R.id.LoginButton);
    }

    private void setClickListner() {
        signupButton.setOnClickListener(this);
        LoginButton.setOnClickListener(this);
        fbLoginButton.setOnClickListener(this);
        googleLoginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.signupButton:
                validate();
                break;

            case R.id.LoginButton:
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                break;

            case R.id.fbLoginButton:
                facebookLogin();
                break;

            case R.id.googleLoginButton:
                signIn();
                break;
        }

    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, REQ_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQ_CODE){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(result);
            updateUI(true);
        }else {
            updateUI(false );
        }
    }

    private void updateUI(boolean isLogin){

    }

    private void handleResult(GoogleSignInResult result){
        if(result.isSuccess()){
            GoogleSignInAccount account = result.getSignInAccount();
            if (account != null) {
                String personName = account.getDisplayName();
                String personGivenName = account.getGivenName();
                String personFamilyName = account.getFamilyName();
                String personEmail = account.getEmail();
                String personId = account.getId();
                Uri personPhoto = account.getPhotoUrl();
                Log.e("gmailData", "personName" + "\t" + personName + "\t\t\t" + "personGivenName" + "\t" + personGivenName + "\t\t\t" + "personFamilyName" + "\t" + personFamilyName + "\t\t\t" + "personEmail" + "\t" + personEmail + "\t\t\t" + "personId" + "\t" + personId);

                getGmailLoginApi(personId,personEmail);
            }
        }

    }

    private void getGmailLoginApi(String google_id, String google_email) {
        HashMap<String, RequestBody> body = new HashMap<>();
        Log.e("google_registration", "");
        body.put("google_id", RequestBody.create(MediaType.parse("multipart/form-data"), google_id));
        body.put("google_email", RequestBody.create(MediaType.parse("multipart/form-data"), google_email));
        new RetrofitService(this, ServiceUrls.REGISTER_GMAIL_URL, 2, 3, body,
                RegistrationActivity.this).callService(true);

    }

    private void facebookLogin() {
        // Set permissions
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email"/*,"user_photos"*//*,"public_profile"*/));

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.e("Success", "Success");
                        System.out.println("onSuccess");

                        String accessToken = loginResult.getAccessToken().getToken();
                        Log.i("accessToken", accessToken);

                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                String str = response.toString();
                                Log.e("facebookData", str + "//"+object.toString());

                                String id =object.optString("id");
                                String first_name = object.optString("first_name");
                                String last_name = object.optString("last_name");
                                String email = object.optString("email");

                                getFacebookApi(id,email);

                                Log.e("PARSE",id+email+first_name+last_name);

                                Bundle bFacebookData = getFacebookData(object);
                            }
                        });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location"); // Parámetros que pedimos a facebook
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        Log.d("", "On cancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.d("", error.toString());
                    }
                });
    }

    private void getFacebookApi(String facebook_id,String facebook_email ) {
        HashMap<String, RequestBody> body = new HashMap<>();
        Log.e("facebook_registration", "");
        body.put("facebook_id", RequestBody.create(MediaType.parse("multipart/form-data"), facebook_id));
        body.put("facebook_email", RequestBody.create(MediaType.parse("multipart/form-data"), facebook_email));
        new RetrofitService(this, ServiceUrls.REGISTER_FACEBOOK_URL, 2, 2, body, RegistrationActivity.this).callService(true);
    }

    private Bundle getFacebookData(JSONObject object) {

        try {
            Bundle bundle = new Bundle();
            String id = object.getString("id");

            try {
                URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=150");
                Log.i("profile_pic", profile_pic + "");
                bundle.putString("profile_pic", profile_pic.toString());

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            }

            bundle.putString("idFacebook", id);
            if (object.has("first_name"))
                bundle.putString("first_name", object.getString("first_name"));
            if (object.has("last_name"))
                bundle.putString("last_name", object.getString("last_name"));
            if (object.has("email"))
                bundle.putString("email", object.getString("email"));
            if (object.has("gender"))
                bundle.putString("gender", object.getString("gender"));
            if (object.has("birthday"))
                bundle.putString("birthday", object.getString("birthday"));
            if (object.has("location"))
                bundle.putString("location", object.getJSONObject("location").getString("name"));

            return bundle;
        } catch (JSONException e) {
            Log.d("error", "Error parsing JSON");
        }
        return null;
    }

    public boolean validate() {
        boolean valid = true;
        String username = signupUsername.getText().toString();
        String email = signupEmail.getText().toString();
        String password = signupPassword.getText().toString();
        String confirmPass = signupConfirmPass.getText().toString();
        String mobile = signupMobile.getText().toString();
        //String countrycode = ccp.ge().toString();

        if (username.isEmpty() && username.equalsIgnoreCase("")) {
            signupUsername.setError("Please enter Username address");
            valid = false;
        } else if (email.isEmpty() && email.equalsIgnoreCase("")) {
            signupEmail.setError("Please enter email address");
            valid = false;
        } else if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            signupEmail.setError("Please enter valid email address");
            valid = false;
        } else if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            signupPassword.setError("Password should be 4 to 10 character");
            valid = false;

        } else if (confirmPass.isEmpty() && email.equalsIgnoreCase("")) {
            signupConfirmPass.setError("Please enter confirm password");
            valid = false;

        } else if (!confirmPass.equals(password)) {
            signupConfirmPass.setError("Please enter correct password");
            valid = false;

        } else if (mobile.isEmpty() && mobile.equalsIgnoreCase("")) {
            signupMobile.setError("Please enter mobile number");
            valid = false;

        }else if (ccp.equals("")){
            Toast.makeText(this, "Please Enter country code", Toast.LENGTH_SHORT).show();
        } else {
            getRegisterNewUser();
        }
        return valid;
    }

    private void getRegisterNewUser() {
        HashMap<String, RequestBody> body = new HashMap<>();
        Log.e("request_registration", "" + m_androidId + signupUsername.getText().toString().trim() + signupEmail.getText().toString().trim() + signupPassword.getText().toString().trim() + signupMobile.getText().toString().trim());
        body.put("device_id", RequestBody.create(MediaType.parse("multipart/form-data"), m_androidId));
        body.put("username", RequestBody.create(MediaType.parse("multipart/form-data"), signupUsername.getText().toString().trim()));
        body.put("email", RequestBody.create(MediaType.parse("multipart/form-data"), signupEmail.getText().toString().trim()));
        body.put("password", RequestBody.create(MediaType.parse("multipart/form-data"), signupPassword.getText().toString().trim()));
        body.put("mobile", RequestBody.create(MediaType.parse("multipart/form-data"), signupMobile.getText().toString().trim()));
        // new RetrofitService(this, ServiceUrls.REGISTER_URL, 2, 1, body, RegistrationActivity.this).callService(true);
        new RetrofitService(this, ServiceUrls.REGISTER_URL, 2, 1, body, RegistrationActivity.this).callService(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public void onServiceResponce(String result, int requestCode) {
        Log.e("register_res", result + "//" + requestCode);
        if (requestCode == 1) {
            Log.e("registration_login", result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("failed") && jsonObject.has("message") && jsonObject.optString("message").equalsIgnoreCase("Email exists already..!")) {
                    Toast.makeText(this, "Email Already exist", Toast.LENGTH_SHORT).show();
                }
                else if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                    startActivity(new Intent(this, LoginActivity.class));
                    Toast.makeText(this, "Register Succesfully", Toast.LENGTH_SHORT).show();
                    finish();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }if(requestCode == 2){
            Log.e("facebook_login", result);
//            E/facebook_login: {"status":"success","user_id":"38"}

            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                   String user_id = jsonObject.optString("user_id");
                    Log.e("user_id",user_id);
                    Toast.makeText(getApplicationContext(),""+jsonObject.optString("status"),Toast.LENGTH_SHORT).show();
                    PreferencesHelper.getInstance(getApplicationContext()).setUserId(user_id);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } if(requestCode == 3){
            Log.e("gmail_login", result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                    String user_id = jsonObject.optString("user_id");
                    Log.e("user_id",user_id);
                    Toast.makeText(getApplicationContext(),""+jsonObject.optString("status"),Toast.LENGTH_SHORT).show();
                    PreferencesHelper.getInstance(getApplicationContext()).setUserId(user_id);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
