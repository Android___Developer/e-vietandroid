package eviet.vlogic.evietandroidapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;

public class DictionaryActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView toolTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);
        toolbar = findViewById(R.id.toolbar);
        toolTitle = findViewById(R.id.toolTitle);

        if(PreferencesHelper.getValue()!=null){
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                toolTitle.setText("Dictionary");

            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                toolTitle.setText("Từ điển");
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
