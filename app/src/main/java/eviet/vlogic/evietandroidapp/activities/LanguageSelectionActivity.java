package eviet.vlogic.evietandroidapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.retrofit.RetrofitService;
import eviet.vlogic.evietandroidapp.retrofit.ServiceResponce;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;
import eviet.vlogic.evietandroidapp.utility.Utility;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class LanguageSelectionActivity extends AppCompatActivity implements View.OnClickListener, ServiceResponce {
    private LinearLayout vietnameseLangLayout, englishLangLayout;
    private Toolbar toolbar;
    private TextView toolTitle;
    String m_androidId = "";
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_selection);
        initView();

        if(PreferencesHelper.getInstance(LanguageSelectionActivity.this).getValue()!=null){
            navigateToBottomActivity();
        }

        if (PreferencesHelper.getInstance(LanguageSelectionActivity.this).getUserId() == null) {
            m_androidId = new Utility().deviceId(this);
            getDeviceId();
        }
        clickedListner();
    }

    private void getDeviceId() {
        HashMap<String, RequestBody> body = new HashMap<>();
//        Log.e("device_id", m_androidId+"//"+ServiceUrls.DEVICE_URL+PreferencesHelper.getDeviceToken());
        body.put("device_id", RequestBody.create(MediaType.parse("multipart/form-data"), m_androidId));
//        body.put("token", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getDeviceToken()+""));
        new RetrofitService(LanguageSelectionActivity.this, ServiceUrls.DEVICE_URL, 2, 1, body, LanguageSelectionActivity.this).callService(true);
    }

    private void clickedListner() {
        vietnameseLangLayout.setOnClickListener(this);
        englishLangLayout.setOnClickListener(this);
    }

    private void initView() {
        vietnameseLangLayout = findViewById(R.id.vietnameseLangLayout);
        englishLangLayout = findViewById(R.id.englishLangLayout);
        toolbar = findViewById(R.id.toolbar);
        toolTitle = findViewById(R.id.toolTitle);
        toolTitle.setText("Language Selection / Sự lựa chọn ngôn ngữ");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.englishLangLayout:
                PreferencesHelper.getInstance(LanguageSelectionActivity.this).setValue("English");
                navigateToBottomActivity();
                break;
            case R.id.vietnameseLangLayout:
                PreferencesHelper.getInstance(LanguageSelectionActivity.this).setValue("Vietnamese");
                navigateToBottomActivity();
                break;

        }
    }

    private void navigateToBottomActivity() {
        startActivity(intent = new Intent(LanguageSelectionActivity.this, BottomActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onServiceResponce(String result, int requestCode) {
        Log.e("device_id_res", result + "//" + requestCode);
//        {"data":[{"user_id":47,"username":"guest47"}],"status":"success","message":"Welcome to E - Viet..!"}
        if (requestCode == 1) {
            Log.e("device_id_res", result);

            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                    String message = jsonObject.optString("message");
                    Toast.makeText(LanguageSelectionActivity.this, "" + message, Toast.LENGTH_SHORT).show();

                    JSONArray array = jsonObject.optJSONArray("data");

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.optJSONObject(i);
                        Log.e("json_obj", "" + object);
                        String user_id = object.optString("user_id");
                        PreferencesHelper.getInstance(getApplicationContext()).setUserId(user_id);
                        PreferencesHelper.getInstance(getApplicationContext()).setUserName(object.optString("username"));
                    }

                    String user_number = "guest";
                    PreferencesHelper.getInstance(LanguageSelectionActivity.this).setUserNumber(user_number);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }
}
