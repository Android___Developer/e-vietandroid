package eviet.vlogic.evietandroidapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;

public class RewardActivity extends AppCompatActivity implements View.OnClickListener {
private TextView total_pointsText,totalPt;
    private Button redeemNowButton,redeemCountinue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward);
        redeemNowButton=(Button)findViewById(R.id.redeemNowButton);
        redeemCountinue=(Button)findViewById(R.id.redeemCountinue);
        Bundle bundle = getIntent().getExtras();
        String total_points = bundle.getString("total_points");
        total_pointsText=findViewById(R.id.total_pointsText);
        totalPt=findViewById(R.id.totalPt);
        total_pointsText.setText(total_points);

        redeemNowButton.setOnClickListener(this);
        redeemCountinue.setOnClickListener(this);


        if(PreferencesHelper.getValue()!=null){
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                redeemCountinue.setText(R.string.continue_eng);
                redeemNowButton.setText(R.string.redeem_now_eng);
                totalPt.setText("Thank you you have earned"+"\t"+total_points +"\t\t"+"points by completing the survey. Keep in eye out for future surveys to earn more points");

            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                redeemCountinue.setText(R.string.continue_viet);
                redeemNowButton.setText(R.string.redeem_now_viet);
                totalPt.setText("Cảm ơn bạn đã kiếm được"+"\t"+total_points +"\t\t"+"điểm bằng cách hoàn thành khảo sát. Theo dõi các cuộc khảo sát trong tương lai để kiếm thêm điểm");
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.redeemNowButton:
                startActivity(new Intent(RewardActivity.this,BottomActivity.class));
                finish();
                break;
            case R.id.redeemCountinue:
                startActivity(new Intent(RewardActivity.this,BottomActivity.class));
                finish();
                break;
        }
    }
}
