package eviet.vlogic.evietandroidapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import eviet.vlogic.evietandroidapp.R;

public class QuestionsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
