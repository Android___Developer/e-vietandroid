package eviet.vlogic.evietandroidapp.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import eviet.vlogic.evietandroidapp.R;

public class GoogleMapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener, GoogleApiClient.ConnectionCallbacks {
    private SupportMapFragment mMapFragment;
    private GoogleMap mGoogleMap;
    private MarkerOptions place;
    private FusedLocationProviderClient mFusedLocationClient; // Object used to receive location updates
    GoogleApiClient mGoogleApiClient;
    Location mLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_map);
      //place = new MarkerOptions().position(new LatLng(28.644800, 77.216721)).title("Location");
        mMapFragment= (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                //  .addApi(Drive.API)
                //  .addScope(Drive.SCOPE_FILE)
                .addConnectionCallbacks(this).build();
        // .addOnConnectionFailedListener(this).build();

        mGoogleApiClient.connect();
    }

    public void checkPermission() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ) {//Can add more as per requirement

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    123);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode==123){

            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    //  .addApi(Drive.API)
                    //  .addScope(Drive.SCOPE_FILE)
                    .addConnectionCallbacks(this).build();
            // .addOnConnectionFailedListener(this).build();

            mGoogleApiClient.connect();
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mGoogleMap = googleMap;
        Toast.makeText(this, "map ready", Toast.LENGTH_SHORT).show();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        //Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-16.710858, -49.285672);
       // mGoogleMap.addMarker(new MarkerOptions().position(sydney).title("current location"));
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 10.0f));

        /*CircleOptions circle = new CircleOptions()
                .center(new LatLng(-16.710858, -49.285672))
                .radius(10000)
                .strokeColor(Color.RED)
                .fillColor(Color.GRAY);
        mGoogleMap.addCircle(circle);*/

        //Camera(CameraUpdateFactory.newLatLng(sydney));

       /* mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        mGoogleMap.setMyLocationEnabled(true);
        LatLng sydney = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
        Log.d("location",mLocation.getLatitude()+mLocation.getLongitude()+"");
        mGoogleMap.addMarker(new MarkerOptions().position(sydney).title("Sharan in map"));*/

        mGoogleMap.setOnMapClickListener(this);
        mGoogleMap.setOnMarkerClickListener(this);

        /*if (mLocation != null) {

            Log.e("locatn", mLocation.getLatitude() + "//" + mLocation.getLongitude());
            LatLng sydney1 = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
            mGoogleMap.addMarker(new MarkerOptions().position(sydney1).title("Sharan in map"));
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney1));
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney1, 17.0f));

        } else {

            Toast.makeText(this, "mlocation null", Toast.LENGTH_SHORT).show();

        }
*/
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        Toast.makeText(this, "OnConnected", Toast.LENGTH_SHORT).show();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLocation != null) {
            Log.e("locatn", mLocation.getLatitude() + "//" + mLocation.getLongitude());
            /*LatLng sydney = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
            mGoogleMap.addMarker(new MarkerOptions().position(sydney).title("Sharan in map"));
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 17.0f));*/

        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this, "OnConnectionSuspended", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }
}

