package eviet.vlogic.evietandroidapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import java.util.ArrayList;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.adapters.DemoAdapter;
import eviet.vlogic.evietandroidapp.adapters.MessageAdapter;

public class DemoActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView toolTitle;
    private RecyclerView demoRecycleView;
    private ArrayList<String> demoArrayList=new ArrayList<>();
    private DemoAdapter demoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);
        toolbar = findViewById(R.id.toolbar);
        toolTitle = findViewById(R.id.toolTitle);
        toolTitle.setText("Topics");
        demoRecycleView =findViewById(R.id.demoRecycleView);

        final GridLayoutManager gridLayoutManager = new GridLayoutManager(this,3);
        gridLayoutManager.setReverseLayout(false);
        demoRecycleView.setLayoutManager(gridLayoutManager);

//        final GridLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        linearLayoutManager.setReverseLayout(false);
//        demoRecycleView.setLayoutManager(linearLayoutManager);


        demoAdapter = new DemoAdapter(this, demoArrayList);
        demoRecycleView.setAdapter(demoAdapter);

    }
}
