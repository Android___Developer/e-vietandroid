package eviet.vlogic.evietandroidapp.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import eviet.vlogic.evietandroidapp.CustomVideoPlayer.CustomVideoView;
import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.retrofit.RetrofitService;
import eviet.vlogic.evietandroidapp.retrofit.ServiceResponce;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class ViewVideoActivity extends AppCompatActivity implements View.OnClickListener,ServiceResponce {
    CustomVideoView playVideoTutorial;
    private Bundle bundle;
    private ImageView playStopVideo;
    MediaController mc;
    String uri, video_id,topic_name,total_points;
    private boolean value = false;
    private boolean timvalue = false;
    private double startTime = 0;
    private double finalTime = 0, lastPos = 0.0;
    private int firstTime = 0;
    private TextView startTimeText;
    private Handler myHandler = new Handler();
    final Context context = this;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_video);
        MediaController mc = new MediaController(this);

        bundle = getIntent().getExtras();
        uri = bundle.getString("video");
        video_id = bundle.getString("video_id");

        playVideoTutorial = findViewById(R.id.playVideoTutorial);
        playStopVideo = findViewById(R.id.playStopVideo);
        startTimeText = findViewById(R.id.startTimeText);
        playStopVideo.setOnClickListener(this);

        try {
            //  Uri video = Uri.parse("http://93.188.167.68/projects/eviet/assets/e_viet_videos/Love-Proposal-Video.mp4");

            mc = new MediaController(this);
            playVideoTutorial.setMediaController(mc);
            Uri video = Uri.parse(uri);
            playVideoTutorial.setVideoURI(video);
            playVideoTutorial.requestFocus();
        } catch (Exception ex) {

        }
        playVideoTutorial.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                value = true;
                //mp.setLooping(true);
                playVideoTutorial.start();
                playVideoTutorial.getDuration();
                Log.e("video_duration", playVideoTutorial.getDuration() + "");
                playVideoTutorial.getCurrentPosition();
                Log.e("video_dCurrentPosition", playVideoTutorial.getCurrentPosition() + "");
                playStopVideo.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pause));
            }
        });

        playVideoTutorial.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                playStopVideo.setBackground(ContextCompat.getDrawable(ViewVideoActivity.this, R.drawable.ic_play));
                Log.e("onCompletion", "okkk");
                mp.stop();
                playVideoTutorial.pause();
//                playVideoTutorial.stopPlayback();
                getVideoWatchStatusApi();
            }
        });


    }

    private void getVideoWatchStatusApi() {
        Log.e("video_id", video_id);
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getInstance(this).getUserId()));
        body.put("video_id", RequestBody.create(MediaType.parse("multipart/form-data"), video_id));
        body.put("status", RequestBody.create(MediaType.parse("multipart/form-data"), "1"));
        body.put("language", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getValue()));
        new RetrofitService(ViewVideoActivity.this, ServiceUrls.WATCHED_VIDEO, 2, 1, body, this).callService(true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.playStopVideo:
                if (playVideoTutorial.isPlaying()) {

                    startTime = playVideoTutorial.getCurrentPosition();
                    finalTime = playVideoTutorial.getDuration();

                    startTimeText.setText(String.format("%d min, %d sec",
                            TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                            TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                            startTime)))
                    );

                    startTimeText.setText(String.format("%d min, %d sec",
                            TimeUnit.MILLISECONDS.toMinutes((long) finalTime),
                            TimeUnit.MILLISECONDS.toSeconds((long) finalTime) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                            finalTime)))
                    );

                    if (firstTime == 0) {

//                        seekbar.setMax((int) finalTime);
                        firstTime = 1;

                    }

//                    Log.e("ok", "//" + playVideoTutorial.getTrackInfo());

                    if (lastPos != 0.0) {

                        startTime = lastPos;
//                        seekbar.setProgress((int) lastPos);
                        playVideoTutorial.seekTo((int) lastPos);

                    } else {

//                        seekbar.setProgress((int) startTime);

                    }

                }

//                myHandler.postDelayed(UpdateVideoTime, 100); // TODO: 6/7/2019


                if (playVideoTutorial.isPlaying()) {
                    playVideoTutorial.pause();
                    playStopVideo.setBackground(ContextCompat.getDrawable(this, R.drawable.ic_play));
                } else {
                    playVideoTutorial.start();
                    playStopVideo.setBackground(ContextCompat.getDrawable(this, R.drawable.ic_pause));
                }

                break;
        }

    }

    @Override
    public void onServiceResponce(String result, int requestCode) {

            Log.e("watch_video_response", result + requestCode);
//            E/watch_video_response: {"status":"success","message":"Congratulations! Nice job..!","data":[{"total_topics":4,"total_completed_topics":0,"total_points":5,"badges":"0"}]}1

//            [{"topic_name":"Thiên nhiên","total_topics":4,"total_completed_topics":0,"total_points":20,"badges":"0"}]
            if (requestCode == 1) {
                Log.e("forming_res", result + requestCode);

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                        String message = jsonObject.optString("message");
                        Toast.makeText(ViewVideoActivity.this, "" + message, Toast.LENGTH_SHORT).show();

                        JSONArray array = jsonObject.optJSONArray("data");
                        Log.e("json_array", "" + array);
                        /*{"topic_name":"Thiên nhiên","total_topics":4,"total_completed_topics":0,"total_points":20,"badges":"0"}*/
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.optJSONObject(i);
                            topic_name = object.optString("topic_name");
                            String total_topics = object.optString("total_topics");
                            String total_completed_topics = object.optString("total_completed_topics");
                            total_points = object.optString("total_points");
                            String badges = object.optString("badges");
                        }
                        openCustomDialog();

                    } else if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("failed")) {
//                 {"status":"failed","message":"Didn't find any sentence..!"}
                        String message = jsonObject.optString("message");
                        Log.e("message", message);
                        Toast.makeText(ViewVideoActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

    }

    private void openCustomDialog() {
        // custom dialog
        final Dialog dialog = new Dialog(context);

        dialog.setContentView(R.layout.custom_alert);
        TextView customTopicName =(TextView)dialog.findViewById(R.id.customTopicName);
        Button dialogButton = (Button) dialog.findViewById(R.id.topicCompleteContinue);

        if(PreferencesHelper.getValue()!=null){
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                dialogButton.setText(R.string.continue_eng);
                customTopicName.setText(topic_name+"\t"+"Level 1 is Completed");

            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                dialogButton.setText(R.string.continue_viet);
                customTopicName.setText(topic_name+"\t"+"Cấp 1 đã hoàn thành");
            }
        }

        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent= new Intent(ViewVideoActivity.this,RewardActivity.class);
                intent.putExtra("total_points",total_points);
                startActivity(intent);

            }
        });
        dialog.show();

    }


}
