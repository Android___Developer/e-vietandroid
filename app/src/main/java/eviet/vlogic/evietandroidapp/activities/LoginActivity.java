package eviet.vlogic.evietandroidapp.activities;

import android.content.Intent;
import android.preference.Preference;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.fcm.FirebaseService;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.retrofit.RetrofitService;
import eviet.vlogic.evietandroidapp.retrofit.ServiceResponce;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;
import eviet.vlogic.evietandroidapp.utility.Utility;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, ServiceResponce {
    private static final String TAG = "LoginActivity";
    public EditText loginEmail, loginPassword;
    public Button loginButton;
    public TextView loginAccount,loginForgot;
    public Intent intent;
    private Toolbar toolbar;
    private TextView toolTitle,skip;
    String m_androidId="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initializeId();
        m_androidId =  new Utility().deviceId(this);
        setClickListner();

      /*  FirebaseService firebaseService = new FirebaseService();
        firebaseService.onTokenRefresh();*/
    }

    private void setClickListner() {
        loginButton.setOnClickListener(this);
        loginAccount.setOnClickListener(this);
        loginForgot.setOnClickListener(this);
        skip.setOnClickListener(this);
    }

    private void initializeId() {
        toolbar = findViewById(R.id.toolbar);
        toolTitle = findViewById(R.id.toolTitle);
        skip = findViewById(R.id.skip);
        skip.setText("Skip");
        skip.setVisibility(View.VISIBLE);
        toolTitle.setText("Login");
        loginEmail =findViewById(R.id.loginEmail);
        loginPassword=findViewById(R.id.loginPassword);
        loginButton=findViewById(R.id.loginButton);
        loginAccount=findViewById(R.id.loginAccount);
        loginForgot=findViewById(R.id.loginForgot);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.loginButton:

                validate();

                break;

            case R.id.loginAccount:

                startActivity(new Intent(this,RegistrationActivity.class));
                finish();

                break;

            case R.id.skip:
                startActivity(new Intent(this,LanguageSelectionActivity.class));
                finish();

                break;

            case R.id.loginForgot:

                startActivity(new Intent(this,ForgotPasswordActivity.class));
                finish();

                break;

        }

    }

    public boolean validate() {

        boolean valid = true;

        String email = loginEmail.getText().toString();
        String password = loginPassword.getText().toString();

        if (email.isEmpty() && email.equalsIgnoreCase("")) {
            loginEmail.setError("Please enter email address");
            valid = false;
        }
        else if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            loginEmail.setError("Please enter valid email address");
            valid = false;
        }
        else if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            loginPassword.setError("Password should be 4 to 10 character");
            valid = false;
        }else {
            getLoginUser();
        }

        return valid;

    }

    private void getLoginUser() {
        Log.d(TAG, "resultlogin=" + m_androidId+loginEmail+loginPassword);
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("device_id", RequestBody.create(MediaType.parse("multipart/form-data"), m_androidId));
        body.put("token", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getDeviceToken()));
        body.put("email", RequestBody.create(MediaType.parse("multipart/form-data"), loginEmail.getText().toString().trim()));
        body.put("password", RequestBody.create(MediaType.parse("multipart/form-data"), loginPassword.getText().toString().trim()));
        new RetrofitService(this, ServiceUrls.LOGIN_URL, 2, 1, body, LoginActivity.this).
                callService(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onServiceResponce(String result, int requestCode) {
        Log.e("login_res", result + "//"+requestCode);

        if (requestCode == 1) {
            Log.e("SAVE_SHIPMENT_STATUS", result);
            // {"status":"success","data":[{"user_id":"6","username":"","email":"ab@mailinator.com","mobile":"1234567890","profile_image":"http:\/\/93.188.167.68\/projects\/eviet\/assets\/user_images\/"}]}
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("status") && jsonObject.optString("status").
                        equalsIgnoreCase("success")) {
                    JSONArray array = jsonObject.optJSONArray("data");
                    Log.e("json_array", ""+array);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.optJSONObject(i);
                        Log.e("json_obj", ""+object);
                        String user_id = object.optString("user_id");
                        String username = object.optString("username");
                        PreferencesHelper.getInstance(getApplicationContext()).setUserId(user_id);
                        PreferencesHelper.getInstance(getApplicationContext()).setUserName(username);
                    }

                    String user_number = "login";
                    PreferencesHelper.getInstance(getApplicationContext()).setUserNumber(user_number);
                    //intent
                    startActivity( new Intent(this,LanguageSelectionActivity.class));
                    finish();
                } else {

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

}
