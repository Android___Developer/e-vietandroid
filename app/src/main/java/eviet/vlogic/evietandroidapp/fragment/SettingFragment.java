package eviet.vlogic.evietandroidapp.fragment;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import java.util.HashMap;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.LanguageSelectionActivity;
import eviet.vlogic.evietandroidapp.activities.LoginActivity;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.retrofit.RetrofitService;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;
import eviet.vlogic.evietandroidapp.utility.Utility;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class SettingFragment extends Fragment implements View.OnClickListener {

    private final static String APP_TITLE = "eViet";

    private final static String APP_NAME = "eviet.vlogic.evietandroidapp";
     SharedPreferences.Editor editor;
 View rootView;
 Context mContext;
    private Toolbar toolbar;
    private TextView toolTitle,editProfText,langChangeText,remainderText,rateAppText,logoutText;
    private CardView editProfileCard,editChangeLanguageCard,editRemainderCard,editRateCard,editLogoutCard;
    private ImageView imgBack;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView=inflater.inflate(R.layout.fragment_setting, container, false);
        toolbar = rootView.findViewById(R.id.toolbar);
        toolTitle = rootView.findViewById(R.id.toolTitle);
        imgBack = rootView.findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.ic_back);
        editProfileCard=rootView.findViewById(R.id.editProfileCard);
        editRemainderCard =rootView.findViewById(R.id.editRemainderCard);
        editRateCard = rootView.findViewById(R.id.editRateCard);
        editChangeLanguageCard=rootView.findViewById(R.id.editChangeLanguageCard);
        editLogoutCard=rootView.findViewById(R.id.editLogoutCard);

        editProfText=rootView.findViewById(R.id.editProfText);
        langChangeText=rootView.findViewById(R.id.langChangeText);
        remainderText=rootView.findViewById(R.id.remainderText);
        rateAppText=rootView.findViewById(R.id.rateAppText);
        logoutText=rootView.findViewById(R.id.logoutText);

        editChangeLanguageCard.setOnClickListener(this);
        editRateCard.setOnClickListener(this);
        editRemainderCard.setOnClickListener(this);
        setVisibilityUser();
        imgBack.setOnClickListener(this);

        if(PreferencesHelper.getValue()!=null){
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                toolTitle.setText(R.string.setting_eng);
                editProfText.setText(R.string.editProfile_eng);
                langChangeText.setText(R.string.change_lang_eng);
                remainderText.setText(R.string.reminder_eng);
                rateAppText.setText(R.string.rateApp_eng);
                logoutText.setText(R.string.logout_eng);

            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                toolTitle.setText(R.string.setting_viet);
                editProfText.setText(R.string.editProfile_viet);
                langChangeText.setText(R.string.change_lang_viet);
                remainderText.setText(R.string.reminder_viet);
                rateAppText.setText(R.string.rateApp_viet);
                logoutText.setText(R.string.logout_viet);
            }
        }
        return rootView;
    }

    private void setVisibilityUser() {
        if(PreferencesHelper.getUserNumber().equalsIgnoreCase("login")){
            editProfileCard.setVisibility(View.VISIBLE);
            editLogoutCard.setVisibility(View.VISIBLE);
            editProfileCard.setOnClickListener(this);
            editLogoutCard.setOnClickListener(this);
        }else  if(PreferencesHelper.getUserNumber().equalsIgnoreCase("guest")){
            editProfileCard.setVisibility(View.GONE);
            editLogoutCard.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.editChangeLanguageCard:
                PreferencesHelper.getInstance(getActivity()).clearValueFromPreferences();
                startActivity(new Intent(getActivity(), LanguageSelectionActivity.class));
                getActivity().finish();
                break;

            case R.id.imgBack:
                Utility.changeFragment(getActivity(),new ProfileFragment());
                break;

            case R.id.editProfileCard:
                   Utility.changeFragment(getActivity(),new EditProfileFragment());
           break;

            case R.id.editLogoutCard:
                    showlogoutDialog();
                break;
                case R.id.editRateCard:
                    showRateDialog();
                    //Utility.changeFragment(getActivity(),new RateAppFragment());
                break;
                case R.id.editRemainderCard:
                    Utility.changeFragment(getActivity(),new PractiseRemainderFragment());
                break;
        }
    }


    private void doLogoutUser() {
        //PreferencesHelper.getInstance(getActivity()).setDeviceToken("");
        PreferencesHelper.getInstance(getActivity()).clearPreferencess();
        startActivity(new Intent(getActivity(), LoginActivity.class));
        getActivity().finish();

    }

    public void showRateDialog() {
        if (getActivity()!=null){
            AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
            builder.setTitle("Rate " + APP_TITLE);
            builder.setMessage("If you enjoy using " + APP_TITLE + ", please take a moment to rate it. Thanks for your support!");
            builder.setPositiveButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            builder.setNegativeButton("Remind me later", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    // Reset days and app launch count
                    dialogInterface.dismiss();
                }
            });

            builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("market://details?id="+"com.android.chrome")));
                    } catch (Exception e) {
                        startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("http://play.google.com/store/apps/details?id=")));
                        e.printStackTrace();
                    }
                    // mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_NAME)));
                    editor.putBoolean("dontShowAgain", true);
                    editor.apply();
                }
            });
            builder.show();
        }
        }


    public void showlogoutDialog() {
        if (getActivity()!=null){
            AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
            builder.setTitle("Logout");
            builder.setMessage("Do you really want to log out");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    doLogoutUser();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.show();
        }
    }

}
