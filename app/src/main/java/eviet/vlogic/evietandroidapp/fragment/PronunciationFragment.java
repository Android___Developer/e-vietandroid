package eviet.vlogic.evietandroidapp.fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.BottomActivity;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.retrofit.RetrofitService;
import eviet.vlogic.evietandroidapp.retrofit.ServiceResponce;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;
import eviet.vlogic.evietandroidapp.utility.Utility;
import okhttp3.MediaType;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;

public class PronunciationFragment extends Fragment implements View.OnClickListener,ServiceResponce {
    private Toolbar toolbar;
    private TextView toolTitle,testQuestionPron,pronProgressText,translateSentText,translateLangText;
    private Button pronunciationCardCheck;
    private TextView voiceInput,testQuestion;
    private ImageView speakButton;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    private String topic_id="",total_pronunciation="",total_answered_pronunciation="",pron_id="";
    private LinearLayout pronunciationLayout;
    private ProgressBar pronProgressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_pronunciation, container, false);
        initView(rootView);
        topic_id = getArguments().getString("topic_id");
        getPronunciationAPI();
        return rootView;
    }

    private void getPronunciationAPI() {
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getUserId()));
        body.put("topic_id", RequestBody.create(MediaType.parse("multipart/form-data"), topic_id));
        body.put("language", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getValue()));
        new RetrofitService(getContext(), ServiceUrls.PRONUNCIATION_QUESTIONS, 2, 1, body, this).callService(true);
    }

    private void initView(View rootView) {
        toolbar = rootView.findViewById(R.id.toolbar);
        toolTitle = rootView.findViewById(R.id.toolTitle);
        voiceInput = rootView.findViewById(R.id.voiceInput);
        speakButton = rootView.findViewById(R.id.btnSpeak);
//        testQuestion = rootView.findViewById(R.id.testQuestion);
        pronunciationLayout = rootView.findViewById(R.id.pronunciationLayout);
        testQuestionPron = rootView.findViewById(R.id.testQuestionPron);
        pronProgressText = rootView.findViewById(R.id.pronProgressText);
        pronProgressBar = rootView.findViewById(R.id.pronProgressBar);
        pronunciationCardCheck = rootView.findViewById(R.id.pronunciationCardCheck);
        translateSentText = rootView.findViewById(R.id.translateSentText);
        translateLangText = rootView.findViewById(R.id.translateLangText);


        if(PreferencesHelper.getValue()!=null){
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                toolTitle.setText("Pronunciation");
                pronunciationCardCheck.setText(R.string.continue_eng);
                translateSentText.setText(R.string.translate_sentence_eng);
                translateLangText.setText("In Vietnamese");

            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                toolTitle.setText("Cách phát âm");
                pronunciationCardCheck.setText(R.string.continue_viet);
                translateSentText.setText(R.string.translate_sentence_viet);
                translateLangText.setText("Bằng tiếng Anh");
            }
        }
        //todo for hide the navigation view
        ((BottomActivity) getActivity()).navigation.setVisibility(View.GONE);



        pronunciationCardCheck.setOnClickListener(this);
        speakButton.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.pronunciationCardCheck:
                //startActivity(new Intent(getActivity(), VideoTutorialsActivity.class));
//                Utility.changeFragment(getActivity(), new VideoTutorialsFragment());

                if(voiceInput.getText().toString().trim().equalsIgnoreCase("")){
                    Toast.makeText(getActivity(),"Please click on audio button and answer this question by voice before continue ",Toast.LENGTH_SHORT).show();
                }else {
                    getPronunciationAnswere();
                }
                break;

            case R.id.btnSpeak:
                askSpeechInput();
                break;
        }
    }

    private void getPronunciationAnswere() {
      /*  user_id:12
        pron_id:2
        language:English
        pronunciation:hello.*/
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getUserId()));
        body.put("pron_id", RequestBody.create(MediaType.parse("multipart/form-data"), pron_id));
        body.put("language", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getValue()));
        body.put("pronunciation", RequestBody.create(MediaType.parse("multipart/form-data"),voiceInput.getText().toString().trim()));

        new RetrofitService(getContext(), ServiceUrls.PRONUNCIATION_ANSWERE, 2, 2, body, this).callService(true);
    }

    // Showing google speech input dialog

    private void askSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Hi speak something");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {

        }
    }

    // Receiving speech input

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

//                    if (result == null) {
//                        voiceInput.setText("No voice results");
//                    } else {
                        voiceInput.setText(result.get(0));
//                        if(testQuestionPron.getText().toString().trim().equalsIgnoreCase(voiceInput.getText().toString().trim())){
//                            voiceInput.setTextColor(getResources().getColor(R.color.colorGreen));
//                            Toast.makeText(getActivity(),"Congress! Right Answer",Toast.LENGTH_SHORT).show();
//                        }else {
//                            voiceInput.setTextColor(getResources().getColor(R.color.colorRed));
//                            Toast.makeText(getActivity(),"Sorry! Wrong Answer",Toast.LENGTH_SHORT).show();
//                        }
                    }
                    break;
                }

            }
        }
//    }

    @Override
    public void onServiceResponce(String result, int requestCode) {
        /*{
            "status": "success",
                "data": [
            {
                "total_questions": 6,
                    "total_answer": 0,
                    "total_pronunciation": 3,
                    "total_answered_pronunciation": 0,
                    "question_type": "4",
                    "word": "sữa",
                    "pron_id": "2"
            }
    ]
        }*/

        if (requestCode == 1) {
            Log.e("forming_res", result + requestCode);

            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                    pronunciationLayout.setVisibility(View.VISIBLE);
                    JSONArray array = jsonObject.optJSONArray("data");
                    Log.e("json_array", "" + array);

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.optJSONObject(i);
                        String total_questions = object.optString("total_questions");
                        String total_answer = object.optString("total_answer");
                        total_pronunciation = object.optString("total_pronunciation");
                        total_answered_pronunciation = object.optString("total_answered_pronunciation");
                        String question_type = object.optString("question_type");
                        String word = object.optString("word");
                        pron_id = object.optString("pron_id");

                        testQuestionPron.setText(word);
                        pronProgressText.setText(total_answer+"/"+total_questions);
                        pronProgressBar.setMax(Integer.valueOf(total_questions));
                        pronProgressBar.setProgress(Integer.valueOf(total_answer));

                    /*    double progress=((Double.parseDouble(total_answer)/Double.parseDouble(total_questions))*100);
                        Log.e("rtr",Integer.valueOf(total_answer)/Integer.valueOf(total_questions)+"//"+total_answer+"//"+total_questions+"//"+progress+"//"+pronProgressBar.getMax());
                      //  pronProgressBar.setProgress(progress);*/



                    }
                }else if(jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("failed")){
//                    {"status":"failed","message":"Didn't find any sentence..!"}
                    pronunciationLayout.setVisibility(View.GONE);
                    String message = jsonObject.optString("message");
                    Log.e("form_message2",message);
                    navigateToVideoFragment();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(requestCode == 2){
            Log.e("forming_res_ans", result + requestCode);

//            {"status":"warning","message":"Sorry! Wrong answer."}
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                    String message = jsonObject.optString("message");
                    Log.e("form_message1",message);
//                    Toast.makeText(getActivity(),""+message,Toast.LENGTH_SHORT).show();
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle("Success !!");
                    alert.setMessage(message);
                    alert.setPositiveButton("OK",null);
                    alert.show();
                    if (total_answered_pronunciation.equalsIgnoreCase(total_pronunciation)) {
                        navigateToVideoFragment();
                    } else {
                        voiceInput.setText("");
                        getPronunciationAPI();
                    }

                }else if(jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("warning")){
                    String message = jsonObject.optString("message");
                    Log.e("form_message2",message);
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle("Warning !!");
                    alert.setMessage(message);
                    alert.setPositiveButton("OK",null);
                    alert.show();
                    if (total_answered_pronunciation.equalsIgnoreCase(total_pronunciation)) {
                        navigateToVideoFragment();
                    } else {
                        voiceInput.setText("");
                        getPronunciationAPI();
                    }
                }else if(jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("failed")) {

                    if (total_answered_pronunciation.equalsIgnoreCase(total_pronunciation)) {
                        navigateToVideoFragment();
                    } else {
                        voiceInput.setText("");
                        getPronunciationAPI();
                    }
//                {"status":"failed","message":"Didn't find any sentence..!"}
                    String message = jsonObject.optString("message");
                    Log.e("form_message2",message);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void navigateToVideoFragment() {
        VideoTutorialsFragment videoTutorialsFragment = new VideoTutorialsFragment();
        Bundle args = new Bundle();
        args.putString("topic_id", topic_id);
        videoTutorialsFragment.setArguments(args);
        Utility.changeFragment(getActivity(),videoTutorialsFragment);
    }
}
