package eviet.vlogic.evietandroidapp.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.RewardActivity;
import eviet.vlogic.evietandroidapp.utility.Utility;

public class TopicCompletedFragment extends Fragment implements View.OnClickListener {
    private Button topicCompleteContinue;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       super.onCreateView(inflater, container, savedInstanceState);

       View v=inflater.inflate(R.layout.custom_alert,container,false);
       initView(v);
       return v;
    }

    private void initView(View v) {
        topicCompleteContinue=v.findViewById(R.id.topicCompleteContinue);
        topicCompleteContinue.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.topicCompleteContinue:

                Utility.changeFragmentWithString(getActivity(),new RewardFragment(),"Reward" );

                break;

        } 

    }

}
