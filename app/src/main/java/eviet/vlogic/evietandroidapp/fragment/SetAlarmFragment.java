package eviet.vlogic.evietandroidapp.fragment;


import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import eviet.vlogic.evietandroidapp.MyBroadcastReceiver;
import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.utility.Utility;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class SetAlarmFragment extends Fragment {

    private EditText edtEvent, edtDate, edtTime;
    TextView setreminderTool;
    ImageView backremainder;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private Button button;
    private AlarmManager am;
    private String eventEntered;
    private String timeEntered;
    private String dateEntered;
    private String formattedTime;
    private Long tsCurrent, tsSet;
    private Calendar c1, c2;
    private String ts;
    private String toParse;
    private SimpleDateFormat formatter;
    private Date date;
    private RelativeLayout rlSpeak;
    private String timeToNotify;


    public SetAlarmFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_set_alarm, container, false);
        edtEvent = (EditText)view.findViewById(R.id.event);
        edtTime = (EditText)view.findViewById(R.id.time);
        edtDate = (EditText)view.findViewById(R.id.date);
        button = (Button)view.findViewById(R.id.button);
        backremainder = view.findViewById(R.id.backremainder);
        setreminderTool=view.findViewById(R.id.setreminderTool);
        rlSpeak = (RelativeLayout)view.findViewById(R.id.speak);
        if(PreferencesHelper.getValue()!=null){
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                setreminderTool.setText("Set Your Remainder");
                button.setText("Submit");

            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                setreminderTool.setText(R.string.setyourremainder);
                button.setText(R.string.submit);
            }
        }
      backremainder.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              Utility.changeFragment(getActivity(), new SettingFragment());
          }
      });
        rlSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(
                        RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");

                try {
                    startActivityForResult(intent, 1);

                } catch (ActivityNotFoundException a) {
                    Toast t = Toast.makeText(getActivity(),
                            "Your device doesn't support Speech to Text",
                            Toast.LENGTH_SHORT);
                    t.show();
                }
            }
        });


        edtTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                c1 = Calendar.getInstance();
                mHour = c1.get(Calendar.HOUR_OF_DAY);
                mMinute = c1.get(Calendar.MINUTE);

                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                                timeToNotify = hourOfDay+":"+minute;
                                formattedTime = FormatTime(hourOfDay, minute);
                                edtTime.setText(formattedTime);
                            }
                        }, mHour, mMinute, false);

                timePickerDialog.show();
            }

        });

        edtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                c2 = Calendar.getInstance();
                mYear = c2.get(Calendar.YEAR);
                mMonth = c2.get(Calendar.MONTH);
                mDay = c2.get(Calendar.DAY_OF_MONTH);

                // Launch Date Picker Dialog
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                edtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);

                datePickerDialog.show();

            }
        });


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtEvent.getText().toString().equals("")){
                    edtEvent.setError("please fill Details to set Remainder");
                }else if (edtDate.getText().toString().equals("")){
                    edtDate.setError("please Select Date");
                }else if (edtTime.getText().toString().equals("")){
                    edtTime.setError("please Select Time");
                }else {
                    scheduleAlarm();

                    eventEntered= edtEvent.getText().toString();
                    timeEntered= edtTime.getText().toString();
                    dateEntered= edtDate.getText().toString();

                    tsCurrent = System.currentTimeMillis();
                    ts = tsCurrent.toString();
                    //  eventDetails.setTimestamp(ts);

                    am = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
                    setOneTimeAlarm();
                    Toast.makeText(getActivity().getBaseContext(), "Reminder Set for new Event", Toast.LENGTH_LONG).show();
                }

            }

        });
        return view;
    }



    //setting

    private void scheduleAlarm() {
        Intent alarmIntent = new Intent(getContext(),MyBroadcastReceiver.class);
        alarmIntent.putExtra("data", edtEvent.getText().toString());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, alarmIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);

        long afterTwoMinutes = SystemClock.elapsedRealtime() + 2 * 60 * 1000;



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            alarmManager.setExactAndAllowWhileIdle
                    (AlarmManager.RTC_WAKEUP,
                            afterTwoMinutes, pendingIntent);
        else
            alarmManager.setExact
                    (AlarmManager.RTC_WAKEUP,
                            afterTwoMinutes, pendingIntent);
    }

    public void setOneTimeAlarm() {
        Intent intent = new Intent(getContext(), MyBroadcastReceiver.class);

        intent.putExtra("event", eventEntered);
        intent.putExtra("time", timeEntered);
        intent.putExtra("date", dateEntered);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        try {
            // Convert the set date and time to timestamp
            toParse = dateEntered + " " + timeToNotify;
            formatter = new SimpleDateFormat("d-M-yyyy hh:mm");
            date = formatter.parse(toParse);
            tsSet = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        am.set(AlarmManager.RTC_WAKEUP, tsSet, pendingIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> text = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                    edtEvent.setText(text.get(0));
                }
                break;
            }
        }
    }

    public String FormatTime(int hour, int minute){

        String time;
        time = "";
        String formattedMinute;

        if(minute/10 == 0){
            formattedMinute = "0"+minute;
        }
        else{
            formattedMinute = ""+minute;
        }


        if(hour == 0){
            time = "12" + ":" + formattedMinute + " AM";
        }
        else if(hour < 12){
            time = hour + ":" + formattedMinute + " AM";
        }
        else if(hour == 12){
            time = "12" + ":" + formattedMinute + " PM";
        }
        else{
            int temp = hour - 12;
            time = temp + ":" + formattedMinute + " PM";
        }



        return time;
    }

}
