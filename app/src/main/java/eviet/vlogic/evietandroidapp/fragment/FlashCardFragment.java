package eviet.vlogic.evietandroidapp.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import eviet.vlogic.evietandroidapp.ImageConverter;
import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.BottomActivity;
import eviet.vlogic.evietandroidapp.utility.Utility;

public class FlashCardFragment extends Fragment implements View.OnClickListener {
    View rootView;
    private Toolbar toolbar;
    private TextView toolTitle;
    private Button fleshCardCheck;
    private ImageView juiceImg, breadImg, manImg, womenImg;
    private Bitmap juiceCircularBitmap, breadCircularBitmap, manCircularBitmap, womenCircularBitmap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_flash_card, container, false);

        fleshCardCheck = rootView.findViewById(R.id.fleshCardCheck);
        fleshCardCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//        startActivity(new Intent(getActivity(), WordsMatchActivity.class));
          Utility.changeFragment(getActivity(), new WordsMatchFragment());

            }
        });

        ((BottomActivity)getActivity()).navigation.setVisibility(View.GONE);
        juiceImg = rootView.findViewById(R.id.juiceImg);
        breadImg = rootView.findViewById(R.id.breadImg);
        manImg = rootView.findViewById(R.id.manImg);
        womenImg = rootView.findViewById(R.id.womenImg);

        juiceCircularBitmap = ImageConverter.getRoundedCornerBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.bread), 100);
        breadCircularBitmap = ImageConverter.getRoundedCornerBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.bread), 100);
        manCircularBitmap = ImageConverter.getRoundedCornerBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.man), 100);
        womenCircularBitmap = ImageConverter.getRoundedCornerBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.women), 100);

        juiceImg.setImageBitmap(juiceCircularBitmap);
        breadImg.setImageBitmap(breadCircularBitmap);
        manImg.setImageBitmap(manCircularBitmap);
        womenImg.setImageBitmap(womenCircularBitmap);

        juiceImg.setOnClickListener(this);
        breadImg.setOnClickListener(this);
        manImg.setOnClickListener(this);
        womenImg.setOnClickListener(this);

        toolbar = rootView.findViewById(R.id.toolbar);
        toolTitle = rootView.findViewById(R.id.toolTitle);
        toolTitle.setText("Flash Cards");

        return rootView;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.flashCardView:

              //startActivity(new Intent(getActivity(),WordsMatchActivity.class));
                Utility.changeFragment(getActivity(), new WordsMatchFragment());

                break;

            case R.id.juiceImg:
                juiceImg.setBackground(getResources().getDrawable(R.drawable.water_background_flashcard));
                breadImg.setBackground(getResources().getDrawable(R.drawable.white_background_flashcard));
                manImg.setBackground(getResources().getDrawable(R.drawable.white_background_flashcard));
                womenImg.setBackground(getResources().getDrawable(R.drawable.white_background_flashcard));

                break;

            case R.id.breadImg:
                breadImg.setBackground(getResources().getDrawable(R.drawable.water_background_flashcard));
                juiceImg.setBackground(getResources().getDrawable(R.drawable.white_background_flashcard));
                manImg.setBackground(getResources().getDrawable(R.drawable.white_background_flashcard));
                womenImg.setBackground(getResources().getDrawable(R.drawable.white_background_flashcard));

                break;

            case R.id.manImg:
                manImg.setBackground(getResources().getDrawable(R.drawable.water_background_flashcard));
                breadImg.setBackground(getResources().getDrawable(R.drawable.white_background_flashcard));
                juiceImg.setBackground(getResources().getDrawable(R.drawable.white_background_flashcard));
                womenImg.setBackground(getResources().getDrawable(R.drawable.white_background_flashcard));

                break;

            case R.id.womenImg:
                manImg.setBackground(getResources().getDrawable(R.drawable.white_background_flashcard));
                breadImg.setBackground(getResources().getDrawable(R.drawable.white_background_flashcard));
                juiceImg.setBackground(getResources().getDrawable(R.drawable.white_background_flashcard));
                womenImg.setBackground(getResources().getDrawable(R.drawable.water_background_flashcard));

                break;

        }

    }

}
