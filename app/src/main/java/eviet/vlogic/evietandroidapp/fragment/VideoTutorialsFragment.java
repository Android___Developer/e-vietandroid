package eviet.vlogic.evietandroidapp.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.BottomActivity;
import eviet.vlogic.evietandroidapp.adapters.VideoAdapter;
import eviet.vlogic.evietandroidapp.model.TopicResponseModel;
import eviet.vlogic.evietandroidapp.model.VideoResponseModel;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.retrofit.RetrofitService;
import eviet.vlogic.evietandroidapp.retrofit.ServiceResponce;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;
import eviet.vlogic.evietandroidapp.utility.Utility;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class VideoTutorialsFragment extends Fragment implements View.OnClickListener,ServiceResponce {

    private Toolbar toolbar;
    private LinearLayout videoFailedLayout;
    private TextView toolTitle,videoProgressText,videoFailedText;
    private ImageView imgBack;
    private RecyclerView videoList;
    private VideoAdapter adapter;
    private String topic_id="";
    private ArrayList<VideoResponseModel> videoArrayList = new ArrayList<>();
    private ProgressBar videoProgressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_video_tutorials, container, false);
        initView(rootView);
        topic_id = getArguments().getString("topic_id");
        getVideos();
        return rootView;
    }

    private void getVideos() {
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getUserId()));
        body.put("topic_id", RequestBody.create(MediaType.parse("multipart/form-data"),topic_id));
        body.put("language", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getValue()));
        new RetrofitService(getContext(), ServiceUrls.VIDEO_VIEW, 2, 1, body, this).callService(true);
    }

    private void initView(View rootView) {
        //todo for hide navigation view
        ((BottomActivity) getActivity()).navigation.setVisibility(View.GONE);

        toolbar = rootView.findViewById(R.id.toolbar);
        toolTitle = rootView.findViewById(R.id.toolTitle);
        imgBack = rootView.findViewById(R.id.imgBack);
        videoFailedLayout=rootView.findViewById(R.id.videoFailedLayout);
        videoFailedText =rootView.findViewById(R.id.videoFailedText);

        if(PreferencesHelper.getValue()!=null){
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                toolTitle.setText("Video Tutorials");
            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                toolTitle.setText("Video hướng dẫn");
            }
        }
        videoList = rootView.findViewById(R.id.videoList);
        videoProgressBar = rootView.findViewById(R.id.videoProgressBar);
        videoProgressText = rootView.findViewById(R.id.videoProgressText);

        videoList.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new VideoAdapter(getActivity(),videoArrayList);
        videoList.setAdapter(adapter);

        imgBack.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.imgBack:
                Utility.changeFragment(getActivity(), new DashboardFragment());
                break;
        }

    }

    @Override
    public void onServiceResponce(String result, int requestCode) {

        if (requestCode == 1) {
            Log.e("video_ res", result + "//" + requestCode);
            videoArrayList.clear();

            try {
                JSONObject jsonObject = new JSONObject(result);

                if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                    JSONArray array = jsonObject.optJSONArray("data");

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.optJSONObject(i);
                        String total_questions = object.optString("total_questions");
                        String total_answer = object.optString("total_answer");
                        String total_video = object.optString("total_video");
                        String total_watched_videos = object.optString("total_watched_videos");
                        String question_type = object.optString("question_type");
                        String video_id = object.optString("video_id");
                        String video_title = object.optString("video_title");
                        String language = object.optString("language");
                        String video = object.optString("video");
                        String video_image = object.getString("video_image");

                        videoProgressText.setText(total_answer+"/"+total_questions);
                        videoProgressBar.setMax(Integer.valueOf(total_questions));
                        videoProgressBar.setProgress(Integer.valueOf(total_answer));

                        VideoResponseModel videoResponseModel = new VideoResponseModel();
                        videoResponseModel.setVideo_id(object.optString("video_id"));
                        videoResponseModel.setVideo_title(video_title);
                        videoResponseModel.setLanguage(language);
                        videoResponseModel.setVideo(video);
                        videoResponseModel.setVideo_image(video_image);

                        videoArrayList.add(videoResponseModel);
                    }

                    adapter.notifyDataSetChanged();
                }else if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("failed")) {
                    String message = jsonObject.optString("message");
                    videoFailedLayout.setVisibility(View.VISIBLE);
                    videoFailedText.setText(message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
