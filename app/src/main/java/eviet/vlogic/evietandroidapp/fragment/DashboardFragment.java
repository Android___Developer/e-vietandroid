package eviet.vlogic.evietandroidapp.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.BottomActivity;
import eviet.vlogic.evietandroidapp.googleMap.GoogleMapAutoTextFragment;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.retrofit.RetrofitService;
import eviet.vlogic.evietandroidapp.retrofit.ServiceResponce;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;
import eviet.vlogic.evietandroidapp.utility.Utility;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class DashboardFragment extends Fragment implements View.OnClickListener,ServiceResponce {
    View rootView;
    private Toolbar toolbar;
    private TextView toolTitle,total_completed_topics,total_completed_topics2,badges,badges2,total_points,total_points2,total_topics,total_topics2;
    private LinearLayout topicLayout, dictionaryLayout, findLayout, chatLayout;
    private Button startButton;
    private ImageView imgBack;
    private ProgressBar dashboardProgressBar;
    private TextView topicsText,dictionaryText,findText,chatText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.fragment_dashboard, container, false);
        ((BottomActivity)getActivity()).navigation.setVisibility(View.VISIBLE);
        initillizeId();
        clickedListner();
        validationCheckForLang();

        if(PreferencesHelper.getUserId()!=null){
            getDashboardData();
        }

        return rootView;
    }

    private void validationCheckForLang() {
        if(PreferencesHelper.getValue()!=null){
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                toolTitle.setText(R.string.learn_viet_eng);
                total_completed_topics2.setText(R.string.topic_comp_eng);
                badges2.setText(R.string.badges_eng);
                total_points2.setText(R.string.total_points_eng);
                total_topics2.setText(R.string.topics_eng);
                startButton.setText(R.string.start_eng);
                topicsText.setText(R.string.topic_eng);
                dictionaryText.setText(R.string.dictionary_eng);
                findText.setText(R.string.find_eng);
                chatText.setText(R.string.chat_eng);

            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                toolTitle.setText(R.string.learn_eng_viet);
                total_completed_topics2.setText(R.string.topic_comp_viet);
                badges2.setText(R.string.badges_viet);
                total_points2.setText(R.string.total_points_viet);
                total_topics2.setText(R.string.topics_viet);
                startButton.setText(R.string.start_viet);
                topicsText.setText(R.string.topic_vit);
                dictionaryText.setText(R.string.dictionary_vit);
                findText.setText(R.string.find_vit);
                chatText.setText(R.string.chat_vit);
            }
        }

    }

    private void getDashboardData() {
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getInstance(getActivity()).getUserId()));
        body.put("language", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getInstance(getActivity()).getValue()));
        new RetrofitService(getActivity(), ServiceUrls.DASHBOARD_API, 2, 1, body, this).callService(true);
    }

    private void clickedListner() {
        topicLayout.setOnClickListener(this);
        dictionaryLayout.setOnClickListener(this);
        findLayout.setOnClickListener(this);
        startButton.setOnClickListener(this);
        chatLayout.setOnClickListener(this);

    }

    private void initillizeId() {
        topicsText = rootView.findViewById(R.id.topicsText);
        dictionaryText = rootView.findViewById(R.id.dictionaryText);
        findText = rootView.findViewById(R.id.findText);
        chatText = rootView.findViewById(R.id.chatText);

        toolbar = rootView.findViewById(R.id.toolbar);
        toolTitle = rootView.findViewById(R.id.toolTitle);
        topicLayout = rootView.findViewById(R.id.topicLayout);
        startButton = rootView.findViewById(R.id.startButton);
        imgBack = rootView.findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.uk_flag);
        chatLayout = rootView.findViewById(R.id.chatLayout);
        dictionaryLayout = rootView.findViewById(R.id.dictionaryLayout);
        findLayout = rootView.findViewById(R.id.findLayout);
        total_completed_topics = rootView.findViewById(R.id.total_completed_topics);
        badges =rootView.findViewById(R.id.badges);
        total_points = rootView.findViewById(R.id.total_points);
        total_topics = rootView.findViewById(R.id.total_topics);

        total_completed_topics2 = rootView.findViewById(R.id.total_completed_topics2);
        badges2 =rootView.findViewById(R.id.badges2);
        total_points2 = rootView.findViewById(R.id.total_points2);
        total_topics2 = rootView.findViewById(R.id.total_topics2);
        dashboardProgressBar = rootView.findViewById(R.id.dashboardProgressBar);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.topicLayout:
                Utility.changeFragmentWithString(getActivity(), new TopicFragment(),"Topics");
                break;
            case R.id.dictionaryLayout:
                Utility.changeFragmentWithString(getActivity(), new TranslateDictionaryFragment(),"Dictionary");
                break;
            case R.id.findLayout:
                Utility.changeFragmentWithString(getActivity(), new GoogleMapAutoTextFragment(),"Map");
                break;
            case R.id.chatLayout:
                Utility.changeFragmentWithString(getActivity(), new AskQuestionFragment(),"Chat");
                break;
            case R.id.startButton:
                Utility.changeFragment(getActivity(), new TopicFragment());
                break;
        }

    }

    @Override
    public void onServiceResponce(String result, int requestCode) {
        if (requestCode == 1) {
            Log.e("dashboard_res", result + requestCode);

            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                    JSONArray array = jsonObject.optJSONArray("data");
                    Log.e("json_array", "" + array);

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.optJSONObject(i);
                        String total_topicss = object.optString("total_topics");
                        String total_completed_topicss = object.optString("total_completed_topics");
                        String total_pointss = object.optString("total_points");
                        String badgess = object.optString("badges");
                        total_completed_topics.setText(total_completed_topicss);
                        badges.setText(badgess);
                        total_points.setText(total_pointss);
                        total_topics.setText(total_completed_topicss+"/"+total_topicss);
                        dashboardProgressBar.setMax(Integer.valueOf(total_topicss));
                        dashboardProgressBar.setProgress(Integer.valueOf(total_completed_topicss));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
