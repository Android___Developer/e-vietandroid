package eviet.vlogic.evietandroidapp.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.LanguageSelectionActivity;
import eviet.vlogic.evietandroidapp.admin_chat.ChatActivity;
import eviet.vlogic.evietandroidapp.chatModule.MessageModule;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.utility.Utility;

public class AskQuestionFragment extends Fragment implements View.OnClickListener {
    View rootView;
    private TextView askedText,answeredText,noQuestText,noQuestDesc;
    private Button askQuestionBtn;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView=inflater.inflate(R.layout.fragment_ask_question, container, false);

        askedText=rootView.findViewById(R.id.askedText);
        answeredText=rootView.findViewById(R.id.answeredText);
        noQuestText=rootView.findViewById(R.id.noQuestText);
        noQuestDesc=rootView.findViewById(R.id.noQuestDesc);
        askQuestionBtn=rootView.findViewById(R.id.askQuestionBtn);
        askQuestionBtn.setOnClickListener(this);

        if(PreferencesHelper.getValue()!=null){
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                askedText.setText(R.string.asked_eng);
                answeredText.setText(R.string.answered_eng);
                noQuestText.setText(R.string.noQuest_eng);
                noQuestDesc.setText(R.string.noQuestDesc_eng);
                askQuestionBtn.setText(R.string.askedQue_eng);

            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                askedText.setText(R.string.asked_viet);
                answeredText.setText(R.string.answered_viet);
                noQuestText.setText(R.string.noQuest_viet);
                noQuestDesc.setText(R.string.noQuestDesc_viet);
                askQuestionBtn.setText(R.string.askedQue_viet);
            }
        }
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.askQuestionBtn:
                startActivity(new Intent(getActivity(), ChatActivity.class));
//                Utility.changeFragment(getActivity(),new MessageFragment());
/*
                Intent intent = new Intent(getActivity(),ChatActivity.class);
                if(askQuestionBtn.isClickable()==true){
//                    intent.putExtra("value0",0);
//                    intent.putExtra("value1",1);
                    String clickedValue = "true";
                    PreferencesHelper.getInstance(getActivity()).setValue(clickedValue);
                }else if(askQuestionBtn.isClickable()==false)
                startActivity(intent);*/
            break;
        }

    }
}
