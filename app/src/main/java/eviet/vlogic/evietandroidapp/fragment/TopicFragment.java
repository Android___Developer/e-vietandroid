package eviet.vlogic.evietandroidapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.BottomActivity;
import eviet.vlogic.evietandroidapp.activities.LoginActivity;
import eviet.vlogic.evietandroidapp.adapters.TopicAdapter;
import eviet.vlogic.evietandroidapp.model.TopicResponseModel;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.retrofit.RetrofitService;
import eviet.vlogic.evietandroidapp.retrofit.ServiceResponce;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;
import eviet.vlogic.evietandroidapp.utility.CheckPermissions;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class TopicFragment extends Fragment implements ServiceResponce {
    View rootView;
    private Toolbar toolbar;
    private TextView toolTitle, notFoundText;
    private RecyclerView topicRecycleView;
    private ArrayList<TopicResponseModel> topicArrayList = new ArrayList<>();
    private TopicAdapter topicAdapter;
    private LinearLayout topicLayout, noFoundLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_topic, container, false);
        init();
        if (PreferencesHelper.getValue() != null) {
            if (PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")) {
                toolTitle.setText(R.string.topic_eng);

            } else if (PreferencesHelper.getValue().equalsIgnoreCase("English")) {
                toolTitle.setText(R.string.topic_vit);
            }
        }
        if (PreferencesHelper.getUserId() != null) {
            getTopics();
        } else {
            startActivity(new Intent(getActivity(), LoginActivity.class));
        }

        return rootView;
    }

    private void getTopics() {
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getUserId()));
        body.put("language", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getValue()));
        Log.e("Topic_value", PreferencesHelper.getUserId() + "//" + PreferencesHelper.getValue());
        new RetrofitService(getContext(), ServiceUrls.TOPIC_VIEW, 2, 1, body, this).callService(true);
    }

    private void init() {
        toolbar = rootView.findViewById(R.id.toolbar);
        toolTitle = rootView.findViewById(R.id.toolTitle);
        notFoundText = rootView.findViewById(R.id.notFoundText);
        notFoundText.setText("Data not Found");
        topicLayout = rootView.findViewById(R.id.topicLayout);
        topicLayout.setVisibility(View.GONE);
        noFoundLayout = rootView.findViewById(R.id.noFoundLayout);
        noFoundLayout.setVisibility(View.GONE);
        topicRecycleView = rootView.findViewById(R.id.topicRecycleView);

        ((BottomActivity) getActivity()).navigation.setVisibility(View.VISIBLE);
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        gridLayoutManager.setReverseLayout(false);
        topicRecycleView.setLayoutManager(gridLayoutManager);

        topicAdapter = new TopicAdapter(getActivity(), topicArrayList);
        topicRecycleView.setAdapter(topicAdapter);
    }
    @Override
    public void onServiceResponce(String result, int requestCode) {
        //{"status":"success","data":[{"user_id":"6","username":"","email":"ab@mailinator.com","mobile":"1234567890","profile_image":"http:\/\/93.188.167.68\/projects\/eviet\/assets\/user_images\/"}]}

        if (requestCode == 1) {
            Log.e("topic_res", result + "//" + requestCode);
            topicArrayList.clear();
            //{"status":"success","data":[{"topic_id":"2","topic_name":"Food","topic_language":"English","topic_image":"http:\/\/93.188.167.68\/projects\/eviet\/assets\/topic_images\/5ccbcc43ab6bc2.45432254.jpg"},{"topic_id":"3","topic_name":"Animal","topic_language":"English","topic_image":"http:\/\/93.188.167.68\/projects\/eviet\/assets\/topic_images\/5ccbd9a79fbc67.46833403.jpg"},{"topic_id":"4","topic_name":"Mens","topic_language":"English","topic_image":"http:\/\/93.188.167.68\/projects\/eviet\/assets\/topic_images\/5ccbd9c337f9b9.98785247.jpg"},{"topic_id":"8","topic_name":"Economics","topic_language":"English","topic_image":"http:\/\/93.188.167.68\/projects\/eviet\/assets\/topic_images\/5cd51444a731a7.62236220.png"},{"topic_id":"9","topic_name":"Money","topic_language":"English","topic_image":"http:\/\/93.188.167.68\/projects\/eviet\/assets\/topic_images\/5cd516a3842f94.63059857.png"}]}

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                        topicLayout.setVisibility(View.VISIBLE);
                        noFoundLayout.setVisibility(View.GONE);
                        JSONArray array = jsonObject.optJSONArray("data");
                        Log.e("json_array", "" + array);
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.optJSONObject(i);
                            Log.e("json_obj", "" + object);
                            String topic_id = object.optString("topic_id");
                            String topic_name = object.optString("topic_name");
                            String topic_language = object.optString("topic_language");
                            String topic_image = object.optString("topic_image");

                            TopicResponseModel topicResponseModel = new TopicResponseModel();
                            topicResponseModel.setTopic_id(topic_id);
                            topicResponseModel.setTopic_name(topic_name);
                            topicResponseModel.setTopic_language(topic_language);
                            topicResponseModel.setTopic_image(topic_image);

                            topicArrayList.add(topicResponseModel);
                            // topicArrayList.notify();

                        }

                        topicAdapter.notifyDataSetChanged();


                    } else if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase
                            ("failed")) {
                        topicLayout.setVisibility(View.GONE);
                        noFoundLayout.setVisibility(View.VISIBLE);
                        notFoundText.setText(jsonObject.optString("message"));
                    } else {
                        noFoundLayout.setVisibility(View.VISIBLE);
                        notFoundText.setText("Topics not Available");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
//            startActivity( new Intent(getActivity(),LanguageSelectionActivity.class));
//            getActivity().finish();
        }
    }
