package eviet.vlogic.evietandroidapp.fragment;


import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toolbar;

import org.w3c.dom.Text;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.utility.Utility;

/**
 * A simple {@link Fragment} subclass.
 */
public class PractiseRemainderFragment extends Fragment {
    LinearLayout remainderclick;
    TextView askedText;
    TextView clickit,clickremainde;
    public PractiseRemainderFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_practise_remainder, container, false);
        clickremainde=view.findViewById(R.id.clickremainde);
        askedText=view.findViewById(R.id.askedText);
        clickit=view.findViewById(R.id.clickit);
        remainderclick=view.findViewById(R.id.remainderclick);
        remainderclick.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Utility.changeFragment(getActivity(),new SetAlarmFragment());

             }
         });

        if(PreferencesHelper.getValue()!=null){
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                askedText.setText("Remind Me");
                clickit.setText("Click");
                clickremainde.setText("to set a remainder");

            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                askedText.setText(R.string.Remainder);
                clickit.setText(R.string.click);
                clickremainde.setText(R.string.tosetaremainder);
            }
        }
        return view;
    }



}
