package eviet.vlogic.evietandroidapp.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.utility.Utility;

public class RewardFragment extends Fragment implements View.OnClickListener {
    private Button redeemNowButton, redeemCountinue;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.activity_reward, container, false);
        initView(v);
        return v;
    }

    private void initView(View v) {
        redeemNowButton = v.findViewById(R.id.redeemNowButton);
        redeemCountinue = v.findViewById(R.id.redeemCountinue);
        redeemCountinue.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.redeemCountinue:
                Utility.changeFragmentWithString(getActivity(), new DashboardFragment(), "Reedem");
                break;
        }
    }
}
