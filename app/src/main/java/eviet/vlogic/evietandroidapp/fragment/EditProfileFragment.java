package eviet.vlogic.evietandroidapp.fragment;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import eviet.vlogic.evietandroidapp.ImageConverter;
import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.ImagePickerActivity;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.retrofit.RetrofitService;
import eviet.vlogic.evietandroidapp.retrofit.ServiceResponce;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;
import eviet.vlogic.evietandroidapp.utility.Utility;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;

public class EditProfileFragment extends Fragment implements View.OnClickListener,ServiceResponce {
    private static final String TAG = "EditProfileFragment";
    View rootView;
    private Toolbar toolbar;
    private TextView toolTitle;
    private ImageView editProfileImg,imgBack;
    private Bitmap juiceCircularBitmap;
    private EditText editProfUsername,editProfEmail,editProfPhone;
    private Button updateButton;
    String strprofile;

    private MultipartBody.Part imagePart ;

    private static final int REQUEST_PICK_IMAGE = 1002;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView=inflater.inflate(R.layout.fragment_edit_profile, container, false);

        init();
        getViewProfile();
        setClick();

        juiceCircularBitmap = ImageConverter.getRoundedCornerBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.avatar), 100);
        editProfileImg.setImageBitmap(juiceCircularBitmap);
        return rootView;
    }

    private void getViewProfile() {
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getUserId()));
        new RetrofitService(getContext(), ServiceUrls.VIEW_PROFILE, 2, 1, body,this).callService(true);

    }

    private void setClick() {
        imgBack.setOnClickListener(this);
        updateButton.setOnClickListener(this);
        editProfileImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPermissionGranted()) {
                    pickImage();
                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 1);
                }
            }
        });
    }

    private void init() {

        toolbar = rootView.findViewById(R.id.toolbar);
        toolTitle = rootView.findViewById(R.id.toolTitle);
        toolTitle.setText("Edit Profile");
        imgBack = rootView.findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.ic_back);
        editProfileImg=rootView.findViewById(R.id.editProfileImg);
        updateButton = rootView.findViewById(R.id.updateButton);

        editProfUsername = rootView.findViewById(R.id.editProfUsername);
        editProfEmail = rootView.findViewById(R.id.editProfEmail);
        editProfPhone = rootView.findViewById(R.id.editProfPhone);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.updateButton:
                if(editProfUsername.getText().toString().trim().equals("")){
                    editProfUsername.setError("Please Enter Username");
                }else if(editProfEmail.getText().toString().trim().equals("")){
                    editProfEmail.setError("Please Enter Email");
                }else if( editProfPhone.getText().toString().trim().equals("")) {
                    editProfPhone.setError("Please Enter Phone Number");
                }else if ( editProfileImg.getDrawable().toString().equals("")){
                    Toast.makeText(getActivity(), "please Select profile image", Toast.LENGTH_SHORT).show();
                } else{
                    getUpdateApi();
                }
                break;
            case R.id.imgBack:
                Utility.changeFragment(getActivity(), new SettingFragment());
                break;

        }
    }

    private void getUpdateApi() {
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getUserId()));
        body.put("username", RequestBody.create(MediaType.parse("multipart/form-data"), editProfUsername.getText().toString().trim()));
        body.put("email", RequestBody.create(MediaType.parse("multipart/form-data"), editProfEmail.getText().toString().trim()));
        body.put("mobile", RequestBody.create(MediaType.parse("multipart/form-data"), editProfPhone.getText().toString().trim()));
        body.put("profile_image", RequestBody.create(MediaType.parse("multipart/form-data"),editProfileImg.getDrawable().toString()));


        Bitmap photo=((BitmapDrawable)editProfileImg.getDrawable()).getBitmap();
        if (photo!=null)
        {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG, 40, stream);
            byte[] photoByteArray = stream.toByteArray();
            RequestBody requestBody = RequestBody.create(MediaType.parse("file/*"), photoByteArray);
            Random random = new Random();
            imagePart= MultipartBody.Part.createFormData("profile_image",
                    "abc" + random.nextInt(1000) + ".jpg", requestBody);
        }
        new RetrofitService(getContext(), ServiceUrls.UPDATE_PROFILE, 6, 2, body, imagePart,this)
                .callService(true);

    }

    @Override
    public void onServiceResponce(String result, int requestCode) {
        if (requestCode == 1) {
            Log.e("view_profile", result + requestCode);

            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                    JSONArray array = jsonObject.optJSONArray("data");
                    Log.e("json_array", "" + array);

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.optJSONObject(i);
                        Log.e("json_obj", "" + object);
                        editProfUsername.setText(object.optString("username"));
                        editProfEmail.setText(object.optString("email"));
                        editProfPhone.setText(object.optString("mobile"));
                        String strprofile = object.optString("profile_image");

                        Picasso.get()
                                .load(strprofile)
                                .placeholder(R.drawable.avatar)
                                .error(R.drawable.avatar)
                                .into(editProfileImg);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(requestCode == 2) {
            Log.e("update_profile", result + requestCode);

            try {
                JSONObject jsonObject = new JSONObject(result);
              /*  if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                    JSONArray array = jsonObject.optJSONArray("data");
                    Log.e("json_array", "" + array);

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.optJSONObject(i);
                        Log.e("json_obj", "" + object);
                        String profile_image = object.optString("profile_image");

                        editProfUsername.setText(object.optString("username"));
                        editProfEmail.setText(object.optString("email"));
                        editProfPhone.setText(object.optString("mobile"));

                    }
                }*/

                String message= jsonObject.optString("message");
                Toast.makeText(getActivity().getApplicationContext(),""+message,Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public void pickImage() {
        startActivityForResult(new Intent(getActivity(), ImagePickerActivity.class), REQUEST_PICK_IMAGE);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (permissions[0].equals(Manifest.permission.CAMERA) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            pickImage();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_PICK_IMAGE:
                    String imagePath = data.getStringExtra("image_path");
                    setImage(imagePath);
                    break;
            }
        } else {

            System.out.println("Failed to load image");
        }
    }
    private void setImage(String imagePath) {

        editProfileImg.setImageBitmap(getImageFromStorage(imagePath));
    }

    private Bitmap getImageFromStorage(String path){

        try {

            File f = new File(path);
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = false;
            options.inSampleSize = calculateInSampleSize(options, 512, 512);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
            return b;

        } catch (FileNotFoundException e){

            e.printStackTrace();
        }
        return null;
    }

    private int calculateInSampleSize(

            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }
}
