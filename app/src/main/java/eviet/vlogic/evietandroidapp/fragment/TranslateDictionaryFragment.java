package eviet.vlogic.evietandroidapp.fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import eviet.vlogic.evietandroidapp.DictionaryDefination;
import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.utility.QueryUtils;

import static android.app.Activity.RESULT_OK;
import static eviet.vlogic.evietandroidapp.retrofit.ServiceUrls.BASE_REQ_URL;
import static eviet.vlogic.evietandroidapp.utility.QueryUtils.LOG_TAG;

public class TranslateDictionaryFragment extends Fragment implements View.OnClickListener, TextToSpeech.OnInitListener {
    View rootView;
    private EditText typeInputEdit;
    private TextView textView2,text1,text2,languageText1,languageText2,dictionaryText,toolTitle,typeInputText, speakToText1, speakToText2;
    private String str_text, mLanguageCodeTo = "vi", mLanguageCodeFrom = "en", str_input = "";
    private ImageView languageChangeImg,translateImg, textToSpeakImg1, textToSpeakImg2, clearTextImg1, clearTextImg2, clearTextImg3, speakToTextImg1, speakToTextImg2;
    private TextToSpeech mTextToSpeech;
    HashMap<String, String> map = new HashMap<>();//    To track status of current activity
    private static final String API_KEY = "AIzaSyCD_wC5Y5LLRM3LOYW8DBdzDkUOWFmkSnU";
    private String value="0";
    String url;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_translate_dictionary, container, false);
        initView();
        mTextToSpeech = new TextToSpeech(getActivity(), this);
        clickListeners();

        if (PreferencesHelper.getValue() != null) {
            if (PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")) {
                toolTitle.setText("Dictionary");

            } else if (PreferencesHelper.getValue().equalsIgnoreCase("English")) {
                toolTitle.setText("Từ điển");
            }
        }
        return rootView;
    }
    private String dictionaryEntries() {
        final String language = "en-gb";
        final String word = typeInputEdit.getText().toString();
        final String fields = "definitions";
        final String strictMatch = "false";
        final String word_id = word.toLowerCase();
        return "https://od-api.oxforddictionaries.com:443/api/v2/entries/" + language + "/" + word_id + "?" + "fields=" + fields + "&strictMatch=" + strictMatch;
    }

    private void clickListeners() {
        languageChangeImg.setOnClickListener(this);
        translateImg.setOnClickListener(this);
        textToSpeakImg1.setOnClickListener(this);
        textToSpeakImg2.setOnClickListener(this);
        clearTextImg1.setOnClickListener(this);
        clearTextImg2.setOnClickListener(this);
        clearTextImg3.setOnClickListener(this);
        speakToTextImg1.setOnClickListener(this);
        speakToTextImg2.setOnClickListener(this);
    }

    private void initView() {
        dictionaryText = rootView.findViewById(R.id.dictionaryText);
        languageChangeImg = rootView.findViewById(R.id.languageChangeImg);
        text1= rootView.findViewById(R.id.text1);
        textView2= rootView.findViewById(R.id.textView2);
        text2=rootView.findViewById(R.id.text2);
        languageText1=rootView.findViewById(R.id.languageText1);
        languageText2=rootView.findViewById(R.id.languageText2);
        toolTitle = rootView.findViewById(R.id.toolTitle);
        typeInputEdit = rootView.findViewById(R.id.typeInputEdit);
        typeInputText = rootView.findViewById(R.id.typeInputText);
        speakToText1 = rootView.findViewById(R.id.speakToText1);
        speakToText2 = rootView.findViewById(R.id.speakToText2);
        textToSpeakImg1 = rootView.findViewById(R.id.textToSpeakImg1);
        textToSpeakImg2 = rootView.findViewById(R.id.textToSpeakImg2);
        clearTextImg1 = rootView.findViewById(R.id.clearTextImg1);
        clearTextImg2 = rootView.findViewById(R.id.clearTextImg2);
        clearTextImg3 = rootView.findViewById(R.id.clearTextImg3);
        speakToTextImg1 = rootView.findViewById(R.id.speakToTextImg1);
        speakToTextImg2 = rootView.findViewById(R.id.speakToTextImg2);
        translateImg = rootView.findViewById(R.id.translateImg);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.textToSpeakImg1:
                str_input = typeInputEdit.getText().toString().trim();
                map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "UniqueID");
                mTextToSpeech.speak(str_input, TextToSpeech.QUEUE_FLUSH, map);
                speakOut();
                break;
            case R.id.textToSpeakImg2:
                speakOut2();
                break;
            case R.id.languageChangeImg:
                if(value.equals("0")){
                    value="1";
                    text1.setText(R.string.vietnamese);
                    text2.setText(R.string.english);
                    languageText1.setText(R.string.vietnamese);
                    languageText2.setText(R.string.english_simplified);
                    dictionaryText.setText(R.string.viet_dictionary);

                }else if(value.equals("1")){
                    value="0";

                    text1.setText(R.string.english);
                    text2.setText(R.string.vietnamese);
                    languageText1.setText(R.string.english);
                    languageText2.setText(R.string.vietnamese_simplified);
                    dictionaryText.setText(R.string.dictionary);
                }

                break;
            case R.id.clearTextImg1:
                typeInputEdit.setText("");
                typeInputText.setText("");
                break;
            case R.id.clearTextImg2:
                typeInputText.setText("");
                break;
            case R.id.clearTextImg3:
                break;
            case R.id.speakToTextImg1:
                askSpeechInput(10);
                break;
            case R.id.speakToTextImg2:
                askSpeechInput(11);
                break;
            case R.id.translateImg:
                str_input = typeInputEdit.getText().toString();
                if (str_input.equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please type any word before", Toast.LENGTH_SHORT).show();
                } else {
//                    new TranslateText().execute(str_input);

                    if(value.equals("0")){
                        translateTextVietnamese(str_input);
                        DictionaryDefination dd = new DictionaryDefination(getActivity(),textView2);
                        url = dictionaryEntries();
                        dd.execute(url);
                    }else if(value.equals("1")){
                        translateTextEnglish(str_input);
                    }
                }
                break;
        }
    }


    private void askSpeechInput(int value) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Hi speak something");
        try {
            // startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
            startActivityForResult(intent, value);
        } catch (ActivityNotFoundException a) {

        }
    }

    // Receiving speech input

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case 10:

                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                    if (result == null) {
                        speakToText1.setText("No voice results");
                    } else {
                        speakToText1.setText(result.get(0));
                    }
                }

                break;

            case 11:

                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                    if (result == null) {
                        speakToText2.setText("No voice results");
                    } else {
                        speakToText2.setText(result.get(0));
                    }
                    break;


                }
        }
    }

    private void speakOut2() {
        int result = mTextToSpeech.setLanguage(new Locale(mLanguageCodeTo));
        Log.e("Inside", "speakOut " + mLanguageCodeTo + " " + result);
        if (result == TextToSpeech.LANG_MISSING_DATA) {
            Toast.makeText(getActivity(), getString(R.string.language_pack_missing), Toast.LENGTH_SHORT).show();
            Intent installIntent = new Intent();
            installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
            startActivity(installIntent);

        } else if (result == TextToSpeech.LANG_NOT_SUPPORTED) {
            Toast.makeText(getActivity(), getString(R.string.language_not_supported), Toast.LENGTH_SHORT).show();
        } else {
            str_text = typeInputText.getText().toString();
            if (str_text.equalsIgnoreCase("")) {
                Toast.makeText(getActivity(), "No word", Toast.LENGTH_SHORT).show();
            } else {
//                str_input = typeInputEdit.getText().toString().trim();
                map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "UniqueID");
                mTextToSpeech.speak(str_text, TextToSpeech.QUEUE_FLUSH, map);
            }

        }
    }


    private void speakOut() {
        int result = mTextToSpeech.setLanguage(new Locale(mLanguageCodeTo));
        Log.e("Inside", "speakOut " + mLanguageCodeTo + " " + result);
        if (result == TextToSpeech.LANG_MISSING_DATA) {
            Toast.makeText(getActivity(), getString(R.string.language_pack_missing), Toast.LENGTH_SHORT).show();
            Intent installIntent = new Intent();
            installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
            startActivity(installIntent);

        } else if (result == TextToSpeech.LANG_NOT_SUPPORTED) {
            Toast.makeText(getActivity(), getString(R.string.language_not_supported), Toast.LENGTH_SHORT).show();
        } else {
            if (str_input.equalsIgnoreCase("")) {
                Toast.makeText(getActivity(), "Please type any word before", Toast.LENGTH_SHORT).show();
            } else {
                str_input = typeInputEdit.getText().toString().trim();
                map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "UniqueID");
                mTextToSpeech.speak(str_input, TextToSpeech.QUEUE_FLUSH, map);
            }

        }
    }

    @Override
    public void onInit(int status) {
        Log.e("Inside----->", "onInit");
        if (status == TextToSpeech.SUCCESS) {
            int result = mTextToSpeech.setLanguage(new Locale("vi"));
            if (result == TextToSpeech.LANG_MISSING_DATA) {
                Toast.makeText(getActivity(), getString(R.string.language_pack_missing), Toast.LENGTH_SHORT).show();
            } else if (result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Toast.makeText(getActivity(), getString(R.string.language_not_supported), Toast.LENGTH_SHORT).show();
            }
            /**/
            textToSpeakImg1.setEnabled(true);
            textToSpeakImg2.setEnabled(true);
            mTextToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                @Override
                public void onStart(String utteranceId) {
                    Log.e("Inside", "OnStart");
//                    process_tts.hide();
                }

                @Override
                public void onDone(String utteranceId) {
                }

                @Override
                public void onError(String utteranceId) {
                }
            });
        } else {
            Log.e(LOG_TAG, "TTS Initilization Failed");
        }

    }

    //  SUBCLASS TO TRANSLATE TEXT ON BACKGROUND THREAD
    private class TranslateText extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... input) {
            Uri baseUri = Uri.parse(BASE_REQ_URL);
            Uri.Builder uriBuilder = baseUri.buildUpon();
            uriBuilder.appendPath("translate")
                    .appendQueryParameter("key", getString(R.string.API_KEY))
                    .appendQueryParameter("lang", mLanguageCodeFrom + "-" + mLanguageCodeTo)
                    .appendQueryParameter("text", input[0]);
            Log.e("String Url ---->", uriBuilder.toString());
            return QueryUtils.fetchTranslation(uriBuilder.toString());
        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("result", result);
            if (!result.equalsIgnoreCase("")) {
                typeInputText.setText(result);
                typeInputText.setTextColor(getResources().getColor(R.color.colorWhite));

            }
        }
    }
    private void translateTextVietnamese(final String str_input){
        final Handler textViewHandler = new Handler();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                TranslateOptions options = TranslateOptions.newBuilder()
                        .setApiKey(API_KEY)
                        .build();
                Translate translate = options.getService();
                final Translation translation =
                        translate.translate(str_input,
                                Translate.TranslateOption.targetLanguage("vi"));
                textViewHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (typeInputText != null) {
                            typeInputText.setText(translation.getTranslatedText());
                            typeInputText.setTextColor(getResources().getColor(R.color.colorWhite));
                        }
                    }
                });
                return null;
            }
        }.execute();
    }

    private void translateTextEnglish(final String str_input){
        final Handler textViewHandler = new Handler();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                TranslateOptions options = TranslateOptions.newBuilder()
                        .setApiKey(API_KEY)
                        .build();
                Translate translate = options.getService();
                final Translation translation =
                        translate.translate(str_input,
                                Translate.TranslateOption.targetLanguage("en"));
                textViewHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (typeInputText != null) {
                            typeInputText.setText(translation.getTranslatedText());
                            typeInputText.setTextColor(getResources().getColor(R.color.colorWhite));
                        }
                    }
                });
                return null;
            }
        }.execute();
    }

}
