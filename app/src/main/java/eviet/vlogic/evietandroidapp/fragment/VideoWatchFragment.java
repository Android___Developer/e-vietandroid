package eviet.vlogic.evietandroidapp.fragment;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import eviet.vlogic.evietandroidapp.CustomVideoPlayer.CustomVideoView;
import eviet.vlogic.evietandroidapp.R;

/**
 * Created by VLOGIC on 5/16/2019.
 */

public class VideoWatchFragment extends Fragment {
    CustomVideoView playVideoTutorial;
    private Bundle bundle;
    String value;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_watch_video, container, false);

        playVideoTutorial = rootView.findViewById(R.id.playVideoTutorial);
        bundle = getArguments();
        if (bundle != null) {
            value = getArguments().getString("video");

        }
        try {
            //  Uri video = Uri.parse("http://93.188.167.68/projects/eviet/assets/e_viet_videos/Love-Proposal-Video.mp4");
            Uri video = Uri.parse(value);
            playVideoTutorial.setVideoURI(video);
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } catch (Exception ex) {

        }
        playVideoTutorial.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                playVideoTutorial.start();
                playVideoTutorial.pause();
            }
        });

        return rootView;
    }


}
