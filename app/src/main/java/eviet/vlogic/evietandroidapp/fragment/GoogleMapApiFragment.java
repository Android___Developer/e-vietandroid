package eviet.vlogic.evietandroidapp.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.exoplayer2.drm.DefaultDrmSessionManager;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import eviet.vlogic.evietandroidapp.R;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * Created by VLOGIC on 5/23/2019.
 */

public class GoogleMapApiFragment extends Fragment implements OnMapReadyCallback {
    View rootView;
    private GoogleMap googleMap;
    private FusedLocationProviderClient mFusedLocationClient;
    private SupportMapFragment mapFragment;
    private Location mCurrentLocation;
    private double latitude;
    private double longitude;
    private LatLng currentLocation;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    // // TODO: 6/12/2019
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_google_map, container, false);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();
        } else {
            getCurrentLocation();
        }


        return rootView;
    }


    private void checkPermission() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
        } else {
            getCurrentLocation();

        }


    }

    private void getCurrentLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                getMyLoc(location);
                if (location != null) {
                    Log.e("location", location.getLatitude() + "\\" + location.getLongitude() + "okok");

                    latitude = location.getLatitude();
                    longitude = location.getLongitude();



//                latitude =location.getLatitude();
//                    longitude =location.getLongitude();
                    Log.e("LAT", latitude + "\n" + longitude);
                    Log.e("getLastLocation", location.toString());
                    Log.e("CurrentLocation", "getLatitude" + location.getLatitude() + "\n" + "getLongitude" + location.getLongitude() + "");


                }
            }

        });

    }

    private void getMyLoc(Location location) {
        latitude=location.getLatitude();
        longitude=location.getLongitude();
        Log.e("loc",location.getLatitude()+"//"+location.getLongitude());
        mapFragment.getMapAsync(this);
    }

    private void requestPermission() {
        requestPermissions(new String[]{ACCESS_FINE_LOCATION}, REQUEST_PERMISSIONS_REQUEST_CODE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getCurrentLocation();

        } else {
            Toast.makeText(getActivity(), "Permission denied", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMapp) {
        googleMap = googleMapp;
        Log.e("latLog", latitude + "\n" + longitude + "");
        currentLocation=new LatLng(latitude,longitude);
       // googleMap.addMarker(new MarkerOptions().position(currentLocation).title("current location"));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 20.0f));

    }
    
}
