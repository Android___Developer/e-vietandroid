package eviet.vlogic.evietandroidapp.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.BottomActivity;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.retrofit.RetrofitService;
import eviet.vlogic.evietandroidapp.retrofit.ServiceResponce;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;
import eviet.vlogic.evietandroidapp.utility.Utility;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class FormingSentanceFragment extends Fragment implements View.OnClickListener,ServiceResponce {

    private Toolbar toolbar;
    private TextView toolTitle,textInput,formingProgressText,translateLangText,formingTextFailed;
    private EditText editInput;
    private Button formingCardCheck,formingTranslateBtn;
    private String mLanguageCodeFrom = "vi";                //    Language Code (From)
    private String total_forming_sentence="",total_forming_answered="",topic_id="",mLanguageCodeTo = "en", sentence_id;
    private LinearLayout progressBarLayout,formingLayout,formingLayoutFailed;
    private ProgressBar formingProgressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_forming_sentance, container, false);
        initView(rootView);
        topic_id = getArguments().getString("topic_id");
        progressBarLayout = rootView.findViewById(R.id.progressBarLayout);
        getFormingSentancesAPI();
        return rootView;
    }

    private void getFormingSentancesAPI() {
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getUserId()));
        body.put("topic_id", RequestBody.create(MediaType.parse("multipart/form-data"), topic_id));
        body.put("language", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getValue()));
        new RetrofitService(getContext(), ServiceUrls.FORMING_QUESTIONS, 2, 1, body, this).callService(true);
    }

    private void initView(View rootView) {
        toolbar = rootView.findViewById(R.id.toolbar);
        toolTitle = rootView.findViewById(R.id.toolTitle);
        editInput= rootView.findViewById(R.id.editInput);
        textInput= rootView.findViewById(R.id.textInput);
        formingLayout = rootView.findViewById(R.id.formingLayout);
        formingLayoutFailed = rootView.findViewById(R.id.formingLayoutFailed);
        formingTextFailed = rootView.findViewById(R.id.formingTextFailed);
        formingProgressText= rootView.findViewById(R.id.formingProgressText);
        formingProgressBar = rootView.findViewById(R.id.formingProgressBar);
        translateLangText = rootView.findViewById(R.id.translateLangText);

        //todo for hide the navigation view
        ((BottomActivity) getActivity()).navigation.setVisibility(View.GONE);

        formingCardCheck = rootView.findViewById(R.id.formingCardCheck);
        formingTranslateBtn = rootView.findViewById(R.id.formingTranslateBtn);
        formingCardCheck.setOnClickListener(this);
        formingTranslateBtn.setOnClickListener(this);

        if(PreferencesHelper.getValue()!=null){
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                toolTitle.setText("Forming Sentences");
                formingCardCheck.setText(R.string.continue_eng);
                translateLangText.setText("In Vietnamese");

            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                toolTitle.setText("Hình thành câu");
                formingCardCheck.setText(R.string.continue_viet);
                translateLangText.setText("Bằng tiếng Anh");
            }
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.formingCardCheck:
                //startActivity(new Intent(getActivity(), PronunciationActivity.class));
                if(editInput.getText().toString().trim().equalsIgnoreCase("")){
                    Toast.makeText(getActivity(),"Please type answer before continue",Toast.LENGTH_SHORT).show();
                }else{
                    getFormingSentancesAnswere();
                }

                editInput.setText("");
                break;

            case R.id.formingTranslateBtn:
//                String input = editInput.getText().toString();
            /*    String input = "Chào mừng bạn đến câu đố hình thành này";
                if(!input.equalsIgnoreCase("")){
                    progressBarLayout.setVisibility(View.VISIBLE);
                    new TranslateText().execute(input);
                }else{
                    progressBarLayout.setVisibility(View.GONE);
                }*/
                break;

        }

    }

    private void getFormingSentancesAnswere() {
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getUserId()));
        body.put("language", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getValue()));
        body.put("sentence_id", RequestBody.create(MediaType.parse("multipart/form-data"), sentence_id));
        body.put("sentence", RequestBody.create(MediaType.parse("multipart/form-data"), editInput.getText().toString().trim()));

        new RetrofitService(getContext(), ServiceUrls.FORMING_ANSWERE, 2, 2, body, this).callService(true);
    }


    @Override
    public void onServiceResponce(String result, int requestCode) {
        if (requestCode == 1) {
            Log.e("forming_res", result + requestCode);

            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                    formingLayout.setVisibility(View.VISIBLE);
                    formingLayoutFailed.setVisibility(View.GONE);
                    JSONArray array = jsonObject.optJSONArray("data");
                    Log.e("json_array", "" + array);

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.optJSONObject(i);
                        String total_questions = object.optString("total_questions");
                        String total_answer = object.optString("total_answer");
                        total_forming_sentence = object.optString("total_forming_sentence");
                        total_forming_answered = object.optString("total_forming_answered");
                        String question_type = object.optString("question_type");
                        String sentence = object.optString("sentence");
                        sentence_id = object.optString("sentence_id");

                        textInput.setText(sentence);
                        formingProgressText.setText(total_answer+"/"+total_questions);
                        formingProgressBar.setMax(Integer.valueOf(total_questions));
                        formingProgressBar.setProgress(Integer.valueOf(total_answer));

                    }
                }else if(jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("failed")){
//                    {"status":"failed","message":"Didn't find any sentence..!"}
                    formingLayout.setVisibility(View.GONE);
                    formingLayoutFailed.setVisibility(View.VISIBLE);
                    String message = jsonObject.optString("message");
                    formingTextFailed.setText(message);
                    Log.e("form_message2",message);
                    navigateToPronunciationFragment();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 2) {
            Log.e("forming_ans", result + requestCode);
            try {
                JSONObject jsonObject = new JSONObject(result);
              /*  {"status":"warning","message":"Sorry! Wrong answer."}
                {"status":"warning","message":"You have formed this sentence already..!"}
                {"status":"success","message":"Congratulations! You have earned 5 points."}*/
                if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                    String message = jsonObject.optString("message");
                    Log.e("form_message1",message);

                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle("Success !!");
                    alert.setMessage(message);
                    alert.setPositiveButton("OK",null);
                    alert.show();

                    if (total_forming_answered.equalsIgnoreCase(total_forming_sentence)) {
                        navigateToPronunciationFragment();
                    } else {
                        editInput.setText("");
                        getFormingSentancesAPI();
                    }

                }else if(jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("warning")){
                    String message = jsonObject.optString("message");
                    Log.e("form_message2",message);
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle("Warning!!");
                    alert.setMessage(message);
                    alert.setPositiveButton("OK",null);
                    alert.show();
//                    Toast.makeText(getActivity(),""+message,Toast.LENGTH_SHORT).show();
                    if (total_forming_answered.equalsIgnoreCase(total_forming_sentence)) {
                        navigateToPronunciationFragment();
                    } else {
                        editInput.setText("");
                        getFormingSentancesAPI();
                    }
                }else if(jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("failed")) {
                    if (total_forming_answered.equalsIgnoreCase(total_forming_sentence)) {
                        navigateToPronunciationFragment();
                    } else {
                        textInput.setText("");
                        getFormingSentancesAPI();
                    }

                    //
                    // {"status":"failed","message":"Didn't find any sentence..!"}
                    String message = jsonObject.optString("message");
                    Log.e("form_message2",message);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void navigateToPronunciationFragment() {
        PronunciationFragment pronunciationFragment = new PronunciationFragment();
        Bundle args = new Bundle();
        args.putString("topic_id", topic_id);
        pronunciationFragment.setArguments(args);
        Utility.changeFragment(getActivity(),pronunciationFragment);
    }

    //  SUBCLASS TO TRANSLATE TEXT ON BACKGROUND THREAD
  /*  private class TranslateText extends AsyncTask<String,Integer,String> {

        @Override
        protected String doInBackground(String... input) {
            Uri baseUri = Uri.parse(BASE_REQ_URL);
            Uri.Builder uriBuilder = baseUri.buildUpon();
            uriBuilder.appendPath("translate")
                    .appendQueryParameter("key",getString(R.string.API_KEY))
                    .appendQueryParameter("lang",mLanguageCodeFrom+"-"+mLanguageCodeTo)
                    .appendQueryParameter("text",input[0]);
            Log.e("String Url ---->",uriBuilder.toString());

            return QueryUtils.fetchTranslation(uriBuilder.toString());
        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("result",result);

           *//* if(result.equals(editInput.getText().toString().trim())){
                editInput.setTextColor(getActivity().getColor(R.color.colorGreen));
            }else {
                editInput.setTextColor(getActivity().getColor(R.color.colorRed));
            }
*//*
            progressBarLayout.setVisibility(View.GONE);
            editInput.setText(result);
        }
    }*/
}
