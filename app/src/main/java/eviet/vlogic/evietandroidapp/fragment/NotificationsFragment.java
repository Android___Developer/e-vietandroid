package eviet.vlogic.evietandroidapp.fragment;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import eviet.vlogic.evietandroidapp.MainActivity;
import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.BottomActivity;
import eviet.vlogic.evietandroidapp.adapters.NotificationAdapter;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationsFragment extends Fragment implements View.OnClickListener {
    private RecyclerView notificationList;
    private NotificationAdapter adapter;
    private ImageView imgBack;
    private TextView toolTitle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_notifications, container, false);
        initView(v);
        return v;
    }

    private void initView(View v) {
        notificationList = v.findViewById(R.id.notificationList);
        imgBack = v.findViewById(R.id.imgBack);
        toolTitle = v.findViewById(R.id.toolTitle);
        if(PreferencesHelper.getValue()!=null){
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                toolTitle.setText("Notification");

            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                toolTitle.setText("Thông báo");
            }
        }
        imgBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));

        notificationList.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new NotificationAdapter(getActivity());
        notificationList.setAdapter(adapter);



        imgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.imgBack:
                getActivity().onBackPressed();
                break;

        }

    }

}
