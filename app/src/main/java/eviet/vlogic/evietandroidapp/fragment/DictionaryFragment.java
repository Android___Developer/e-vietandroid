package eviet.vlogic.evietandroidapp.fragment;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.ClipboardManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.utility.QueryUtils;
import static android.app.Activity.RESULT_OK;
import static eviet.vlogic.evietandroidapp.retrofit.ServiceUrls.BASE_REQ_URL;
import static eviet.vlogic.evietandroidapp.utility.QueryUtils.LOG_TAG;

public class DictionaryFragment extends Fragment implements View.OnClickListener,TextToSpeech.OnInitListener {
    View rootView;
    private Toolbar toolbar;
    private TextView toolTitle,textVoiceSet,textViewResult,copyText;
    private String mLanguageCodeFrom = "en",input="",str_speakToSpeak="";
    private String mLanguageCodeTo = "vi";
    private EditText dictionaryText,dictionaryTextToSpeak;
    private ImageView voiceRecorder, textSoundImg, cancelText, starImage,cancelTextImage,copyTextImg;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    private ImageButton translateBtn, translateBtnToSpeak;
    private TextToSpeech mTextToSpeech;
    private Dialog process_tts;
    volatile boolean activityRunning;
    HashMap<String, String> map = new HashMap<>();//    To track status of current activity

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_dictionary, container, false);
        toolbar = rootView.findViewById(R.id.toolbar);
        toolTitle = rootView.findViewById(R.id.toolTitle);
        textVoiceSet = rootView.findViewById(R.id.textVoiceSet);
        voiceRecorder = rootView.findViewById(R.id.voiceRecorder);
        translateBtn = rootView.findViewById(R.id.translateBtn);
        translateBtnToSpeak = rootView.findViewById(R.id.translateBtnToSpeak);
        textSoundImg = rootView.findViewById(R.id.textSoundImg);
        cancelText = rootView.findViewById(R.id.cancelText);
        cancelTextImage =rootView.findViewById(R.id.cancelTextImage);
        starImage = rootView.findViewById(R.id.starImage);
        dictionaryText = rootView.findViewById(R.id.dictionaryText);
        dictionaryTextToSpeak = rootView.findViewById(R.id.dictionaryTextToSpeak);
        copyTextImg = rootView.findViewById(R.id.copyTextImg);
        textViewResult = rootView.findViewById(R.id.textViewResult);
        copyText = rootView.findViewById(R.id.copyText);

        voiceRecorder.setOnClickListener(this);
        translateBtn.setOnClickListener(this);
        translateBtnToSpeak.setOnClickListener(this);
        textSoundImg.setOnClickListener(this);
        cancelText.setOnClickListener(this);
        cancelTextImage.setOnClickListener(this);
        starImage.setOnClickListener(this);
        copyTextImg.setOnClickListener(this);
        mTextToSpeech = new TextToSpeech(getActivity(), this);

        if (PreferencesHelper.getValue() != null) {
            if (PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")) {
                toolTitle.setText("Dictionary");

            } else if (PreferencesHelper.getValue().equalsIgnoreCase("English")) {
                toolTitle.setText("Từ điển");
            }
        }
//        new TranslateText().execute();
        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        map.clear();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        map.clear();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        map.clear();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.voiceRecorder:
                askSpeechInput();
                break;
            case R.id.translateBtn:
//                String input = textVoiceSet.getText().toString();
//                new TranslateText().execute(input);
                input =   dictionaryText.getText().toString();
                if (!input.equalsIgnoreCase("")) {
                    new TranslateText().execute(input);
                } else {
                    Toast.makeText(getActivity(),"Please type any word before",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.translateBtnToSpeak:

                str_speakToSpeak = dictionaryTextToSpeak.getText().toString().trim();
                if(str_speakToSpeak.equalsIgnoreCase("")){
                    Toast.makeText(getActivity(),"Please type any word before",Toast.LENGTH_SHORT).show();
                }else{
                    new TranslateText().execute(str_speakToSpeak);
                }
                break;

            case R.id.copyTextImg:
               /* ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(textVoiceSet.getText().toString().trim(), copyText.getText().toString().trim());
                clipboard.setPrimaryClip(clip);*/

                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", "HELLO");
                if (clipboard == null)
                    return;
                clipboard.setPrimaryClip(clip);
                break;

            case R.id.cancelText:
                textVoiceSet.setText("");
                break;

            case R.id.cancelTextImage:
                textViewResult.setText("");
                dictionaryText.setText("");
                break;

            case R.id.starImage:
//                starImage.setBackground(R.drawable.ic_star_dark);
                break;
            case R.id.textSoundImg:
                speakOut();
                break;
        }

    }

    private void speakOut() {
        int result = mTextToSpeech.setLanguage(new Locale(mLanguageCodeTo));
        Log.e("Inside", "speakOut " + mLanguageCodeTo + " " + result);
        if (result == TextToSpeech.LANG_MISSING_DATA) {
            Toast.makeText(getActivity(), getString(R.string.language_pack_missing), Toast.LENGTH_SHORT).show();
            Intent installIntent = new Intent();
            installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
            startActivity(installIntent);

        } else if (result == TextToSpeech.LANG_NOT_SUPPORTED) {
            Toast.makeText(getActivity(), getString(R.string.language_not_supported), Toast.LENGTH_SHORT).show();
        } else {

            String textMessage = "hello and welcome";
//            String textMessage = textVoiceSet.getText().toString();
            Toast.makeText(getActivity(), ""+textMessage, Toast.LENGTH_SHORT).show();
            map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "UniqueID");
            mTextToSpeech.speak(textMessage, TextToSpeech.QUEUE_FLUSH, map);
        }
    }

    private void askSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Hi speak something");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {

        }
    }

    // Receiving speech input

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                    if (result == null) {
                        textVoiceSet.setText("No voice results");
                    } else {
                        textVoiceSet.setText(result.get(0));
                    }
                    break;
                }

            }
        }
    }

    @Override
    public void onInit(int status) {
        Log.e("Inside----->", "onInit");
        if (status == TextToSpeech.SUCCESS) {
            int result = mTextToSpeech.setLanguage(new Locale("vi"));
            if (result == TextToSpeech.LANG_MISSING_DATA) {
                Toast.makeText(getActivity(), getString(R.string.language_pack_missing), Toast.LENGTH_SHORT).show();
            } else if (result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Toast.makeText(getActivity(), getString(R.string.language_not_supported), Toast.LENGTH_SHORT).show();
            }
            textSoundImg.setEnabled(true);
            mTextToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                @Override
                public void onStart(String utteranceId) {
                    Log.e("Inside", "OnStart");
//                    process_tts.hide();
                }

                @Override
                public void onDone(String utteranceId) {
                }

                @Override
                public void onError(String utteranceId) {
                }
            });
        } else {
            Log.e(LOG_TAG, "TTS Initilization Failed");
        }

    }

    //  SUBCLASS TO TRANSLATE TEXT ON BACKGROUND THREAD
    private class TranslateText extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... input) {
            Uri baseUri = Uri.parse(BASE_REQ_URL);
            Uri.Builder uriBuilder = baseUri.buildUpon();
            uriBuilder.appendPath("translate")
                    .appendQueryParameter("key", getString(R.string.API_KEY))
                    .appendQueryParameter("lang", mLanguageCodeFrom + "-" + mLanguageCodeTo)
                    .appendQueryParameter("text", input[0]);
            Log.e("String Url ---->", uriBuilder.toString());
            return QueryUtils.fetchTranslation(uriBuilder.toString());
        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("result", result);
            if(!result.equalsIgnoreCase("")) {
                textViewResult.setText(result);
                textViewResult.setTextColor(getActivity().getColor(R.color.colorPrimary));
            }
        }
    }
}

