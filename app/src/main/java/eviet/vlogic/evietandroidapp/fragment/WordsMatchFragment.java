package eviet.vlogic.evietandroidapp.fragment;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.BottomActivity;
import eviet.vlogic.evietandroidapp.activities.DragDropActivity;
import eviet.vlogic.evietandroidapp.adapters.WordMatchAdapter;
import eviet.vlogic.evietandroidapp.model.WordMatchingDataResponse;
import eviet.vlogic.evietandroidapp.model.WordMatchingOptionResponse;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.retrofit.RetrofitService;
import eviet.vlogic.evietandroidapp.retrofit.ServiceResponce;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;
import eviet.vlogic.evietandroidapp.utility.Utility;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class WordsMatchFragment extends Fragment implements View.OnClickListener, ServiceResponce {
    private Toolbar toolbar;
    private TextView errorData,toolTitle,progressStatuss, option1, option2, option3, option4, choice,matchWordTextFailed;
    private Button wordMatchCheck;
    private RecyclerView wordMatchRecycleView;
    private WordMatchAdapter wordMatchAdapter;
    private ArrayList<WordMatchingOptionResponse> marchResponseModelArrayList;
    private LinearLayout matchCardLayout,matchWordLayout,matchWordLayoutFailed;
    private ProgressBar wordMatchProgressBar;
    View rootView;
    private ImageView wordMatchImage,custom_cancel_img;
    private String  topic_id,total_questions="",total_answer="",total_matching_word="",total_matching_answered="",word = "",word_id="",optionsId="",word_image="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_words_match, container, false);
        initializeId();
        topic_id = getArguments().getString("topic_id");
        getWordMatchingQuestion();
        clickedListner();

        //todo for hide navigation view
        ((BottomActivity) getActivity()).navigation.setVisibility(View.GONE);
        toolbar = rootView.findViewById(R.id.toolbar);
        toolTitle = rootView.findViewById(R.id.toolTitle);
        if(PreferencesHelper.getValue()!=null){
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                toolTitle.setText("Words Matching");
                wordMatchCheck.setText(R.string.continue_eng);

            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                toolTitle.setText("Từ phù hợp");
                wordMatchCheck.setText(R.string.continue_viet);
            }
        }
        matchCardLayout = rootView.findViewById(R.id.matchCardLayout);
        errorData = rootView.findViewById(R.id.errorData);
        wordMatchProgressBar = rootView.findViewById(R.id.wordMatchProgressBar);
        progressStatuss = rootView.findViewById(R.id.progressStatuss);
        matchWordLayout = rootView.findViewById(R.id.matchWordLayout);
        matchWordLayoutFailed = rootView.findViewById(R.id.matchWordLayoutFailed);
        matchWordTextFailed = rootView.findViewById(R.id.matchWordTextFailed);


//        wordMatchRecycleView=rootView.findViewById(R.id.wordMatchRecycleView);
//        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
//        linearLayoutManager.setReverseLayout(false);
//        wordMatchRecycleView.setLayoutManager(linearLayoutManager);
//
        marchResponseModelArrayList = new ArrayList<>();
//        wordMatchAdapter = new WordMatchAdapter(getActivity(), marchResponseModelArrayList);
//        wordMatchRecycleView.setAdapter(wordMatchAdapter);

        return rootView;
    }

    private void getWordMatchingQuestion() {
        //Retrieve the value


        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getUserId()));
        body.put("topic_id", RequestBody.create(MediaType.parse("multipart/form-data"), topic_id));
        body.put("language", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getValue()));
        Log.e("Topic_value", PreferencesHelper.getValue());
        new RetrofitService(getContext(), ServiceUrls.WORD_MATCHING, 2, 1, body, this).callService(true);
    }

    private void clickedListner() {
        wordMatchCheck.setOnClickListener(this);
        choice.setOnClickListener(this);
        //set touch listeners
        option1.setOnTouchListener(new ChoiceTouchListener());
        option2.setOnTouchListener(new ChoiceTouchListener());
        option3.setOnTouchListener(new ChoiceTouchListener());
        option4.setOnTouchListener(new ChoiceTouchListener());

        //set drag listeners
        choice.setOnDragListener(new ChoiceDragListener());
        custom_cancel_img.setOnClickListener(this);
    }

    @Override
    public void onServiceResponce(String result, int requestCode) {
        Log.e("an45", result + requestCode);
        if (requestCode == 2) {
            Log.e("answerword", result + requestCode);
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                    if (total_matching_answered.equalsIgnoreCase(total_matching_word)) {
                        navigateToFormingFragment();
                    } else {
                        getWordMatchingQuestion();
                    }
                } else if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase
                        ("warning")) {
                    String message = jsonObject.optString("message");
                    if (total_matching_answered.equalsIgnoreCase(total_matching_word)) {
                        navigateToFormingFragment();
                    } else {
                        getWordMatchingQuestion();
                    }
                } else if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("failed")) {
//                     {"status":"failed","message":"No word in this topic..!"}
                    matchCardLayout.setVisibility(View.GONE);
                    String message = jsonObject.optString("message");
                    errorData.setText(message);
//                    navigateToFormingFragment();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        if (requestCode == 1) {

            Log.e("Word_Matching_res", result + requestCode);

            try {
                JSONObject jsonObject = new JSONObject(result);
                Log.e("ok","okf");
                 if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                     matchWordLayout.setVisibility(View.VISIBLE);
                     matchWordLayoutFailed.setVisibility(View.GONE);
                     JSONArray dataArray=jsonObject.optJSONArray("data");
                     Log.e("ok1","okf1");
                     for (int i = 0; i <dataArray.length() ; i++) {
                         Log.e("ok55"+i,"okf");
                         JSONObject jsonObject1=dataArray.optJSONObject(i);
//                         "total_questions": 2,
//                                 "total_answer": 0,
                         Log.e("ok54"+i,"okf");
                         total_questions = jsonObject1.optString("total_questions");
                         total_answer = jsonObject1.optString("total_answer");
                         progressStatuss.setText(total_answer+"/"+total_questions);

                        /* int total_answerr=Integer.parseInt(total_answer);
                         int total_questionss=Integer.parseInt(total_questions);
                         wordMatchProgressBar.setProgress(total_answerr/total_questionss*100);*/
                         wordMatchProgressBar.setMax(Integer.valueOf(total_questions));
                         wordMatchProgressBar.setProgress(Integer.valueOf(total_answer));

                      //   wordMatchProgressBar.setProgress(total_answerr/total_questionss*100);

                         total_matching_word = jsonObject1.optString("total_matching_word");
                         total_matching_answered = jsonObject1.optString("total_matching_answered");
                         word = jsonObject1.optString("word");
                         word_id = jsonObject1.optString("word_id");
                         word_image = jsonObject1.optString("word_image");

                         Log.e("word123e", jsonObject.toString());
                         choice.setText(word);

                         Picasso.get()
                                 .load(word_image)
                                 .placeholder(R.drawable.lion)
                                 .error(R.drawable.bread).fit()
                                 .into(wordMatchImage);

                         AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                         alert.setTitle("Warning!!");
                         alert.setMessage(jsonObject.optString("message"));
                         alert.setPositiveButton("OK",null);
                         alert.show();

                         WordMatchingDataResponse wordMatchingDataResponse = new WordMatchingDataResponse();
                         wordMatchingDataResponse.setTotal_questions(total_questions);
                         wordMatchingDataResponse.setTotal_nbrof_ans(total_answer);
                         wordMatchingDataResponse.setTotal_matching_word_question(total_matching_word);
                         wordMatchingDataResponse.setTotal_matching_answered(total_matching_answered);
                     }

                     Log.e("w23", jsonObject.has("status") + "//" + jsonObject.optString("status").equalsIgnoreCase("success") + "//" + jsonObject.optJSONArray("data").toString());

                     JSONArray array = jsonObject.optJSONArray("option");
                    Log.e("010", array.toString());
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.optJSONObject(i);
                        Log.e("0101", object.toString());

                        WordMatchingOptionResponse wordMatchingOptionResponse = new WordMatchingOptionResponse();
                        wordMatchingOptionResponse.setOpt_id(object.optString("opt_id"));
                        wordMatchingOptionResponse.setOptions(object.optString("options"));
                        marchResponseModelArrayList.add(wordMatchingOptionResponse);

                        Log.e("dgsdg", i + "//" + object.optString("options"));

                        switch (i) {

                            case 0:
                                option1.setText(object.optString("options"));
                                option1.setTag(String.valueOf(object.optString("opt_id")));
//                              option2.setTag(new Integer(object.optInt("opt_id")));
                                Log.e("bgbg", i + "//" + object.optString("options"));
                                break;

                            case 1:
                                option2.setText(object.optString("options"));
                                option2.setTag(String.valueOf(object.optString("opt_id")));
                                Log.e("bgbg", i + "//" + object.optString("options"));
                                break;

                            case 2:
                                option3.setText(object.optString("options"));
                                option3.setTag(String.valueOf(object.optString("opt_id")));
                                Log.e("bgbg", i + "//" + object.optString("options"));
                                break;

                            case 3:
                                option4.setText(object.optString("options"));
                                option4.setTag(String.valueOf(object.optString("opt_id")));
                                Log.e("bgbg", i + "//" + object.optString("options"));
                                break;

                        }

                    }
                    //wordMatchAdapter.notifyDataSetChanged();

                }else if(jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("failed")){
                     matchWordLayout.setVisibility(View.GONE);
                     matchWordLayoutFailed.setVisibility(View.VISIBLE);
//                     {"status":"failed","message":"No word in this topic..!"}
                     String message = jsonObject.optString("message");
                     matchWordTextFailed.setText(message);
                     errorData.setText(message);
                     navigateToFormingFragment();
                 }
//                E/Flash_card_answer: {"status":"warning","message":"Wrong answer..!"}2
                 else if(jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("warning")){
                     String message = jsonObject.optString("message");
                     errorData.setText(message);
//                     Toast.makeText(getActivity(),""+message,Toast.LENGTH_SHORT).show();

                     AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                     alert.setTitle("Warning !!");
                     alert.setMessage(message);
                     alert.setPositiveButton("OK",null);
                     alert.show();
                 }
                 else {
                     navigateToFormingFragment();
                 }

            } catch (JSONException e) {

                e.printStackTrace();

            }

        }

    }

    private void navigateToFormingFragment() {
        FormingSentanceFragment formingSentanceFragment = new FormingSentanceFragment();
        Bundle args = new Bundle();
        args.putString("topic_id", topic_id);
        formingSentanceFragment.setArguments(args);
        Utility.changeFragment(getActivity(),formingSentanceFragment);
    }

    private final class ChoiceTouchListener implements View.OnTouchListener {
        @SuppressLint("NewApi")
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {

                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                view.startDrag(data, shadowBuilder, view, 0);
                return true;
            } else {
                return false;
            }
        }
    }

    @SuppressLint("NewApi")
    private class ChoiceDragListener implements View.OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {

            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    break;

                case DragEvent.ACTION_DROP:

                    //handle the dragged view being dropped over a drop view
                    View view = (View) event.getLocalState();
                    //view dragged item is being dropped on
                    TextView dropTarget = (TextView) v;
                    //view being dragged and dropped
                    TextView dropped = (TextView) view;
                    optionsId=dropped.getTag()+"";
                    Log.e("setr",dropped.getTag()+"//"+dropTarget.getTag());

                    //checking whether first character of dropTarget equals first character of dropped
//                    if (dropTarget.getText().toString().equalsIgnoreCase(dropped.getText().toString())) { // TODO: 5/21/2019  
                        //stop displaying the view where it was before it was dragged
                        view.setVisibility(View.INVISIBLE); // TODO: 5/23/2019
                        //update the text in the target view to reflect the data being dropped
//                        dropTarget.setText(dropTarget.getText().toString() + dropped.getText().toString());
                        dropTarget.setText(dropped.getText().toString());
                    if(dropped.getText().toString().equalsIgnoreCase(option1.toString()))
                        //make it bold to highlight the fact that an item has been dropped
                        dropTarget.setTypeface(Typeface.DEFAULT_BOLD);

                        dropTarget.setTextColor(ContextCompat.getColor(getActivity(), R.color.toolbarColor));
                        //if an item has already been dropped here, there will be a tag
                        Object tag = dropTarget.getTag();
                        //if there is already an item here, set it back visible in its original place
                        if (tag != null) {
                            //the tag is the view id already dropped here
                            int existingID = (Integer) tag;
                            //set the original view visible again
                            rootView.findViewById(existingID).setVisibility(View.VISIBLE);
                            Toast.makeText(getActivity(),"already selected",Toast.LENGTH_SHORT).show();
                        }
                        //set the tag in the target view being dropped on - to the ID of the view being dropped
                        dropTarget.setTag(dropped.getId());
                        //remove setOnDragListener by setting OnDragListener to null, so that no further drag & dropping on this TextView can be done
                        dropTarget.setOnDragListener(null);
//                    } else
//                        //displays message if first character of dropTarget is not equal to first character of dropped // TODO: 5/21/2019
//                        Toast.makeText(getActivity(), dropTarget.getText().toString() + "\t" + "is not " + dropped.getText().toString(), Toast.LENGTH_LONG).show();
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    //no action necessary
                    break;
                default:
                    break;
            }
            return true;

        }

    }

    private void initializeId() {
        wordMatchCheck = rootView.findViewById(R.id.wordMatchCheck);
        //views to drag
        option1 = rootView.findViewById(R.id.option_1);
        option2 = rootView.findViewById(R.id.option_2);
        option3 = rootView.findViewById(R.id.option_3);
        option4 = rootView.findViewById(R.id.option_4);

        //views to drop onto
        choice = rootView.findViewById(R.id.choice);
        wordMatchImage = rootView.findViewById(R.id.wordMatchImage);
        custom_cancel_img = rootView.findViewById(R.id.custom_cancel_img);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.wordMatchCheck:
                if(optionsId.equalsIgnoreCase("")){
                Toast.makeText(getActivity(),"Please select any option ",Toast.LENGTH_SHORT).show();
            }else{
                getMatchWordAnswer(word_id,optionsId);
            }
                break;

            case R.id.custom_cancel_img:
//                option1.setVisibility(TextView.VISIBLE);
//                option2.setVisibility(TextView.VISIBLE);
//                option3.setVisibility(TextView.VISIBLE);
//                option4.setVisibility(TextView.VISIBLE);
//                choice.setTag(null);
//                choice.setTypeface(Typeface.DEFAULT);
//                choice.setText(word);
//                choice.setTextColor(ContextCompat.getColor(getActivity(), R.color.textColor));
//                choice.setOnDragListener(new ChoiceDragListener());
                break;

            case R.id.choice:
                option1.setVisibility(TextView.VISIBLE);
                option2.setVisibility(TextView.VISIBLE);
                option3.setVisibility(TextView.VISIBLE);
                option4.setVisibility(TextView.VISIBLE);
                choice.setTag(null);
                choice.setTypeface(Typeface.DEFAULT);
                choice.setText(word);
                choice.setTextColor(ContextCompat.getColor(getActivity(), R.color.textColor));
                choice.setOnDragListener(new ChoiceDragListener());
                break;

        }

    }

    private void getMatchWordAnswer(String word_id, String options) {
        Log.e("WORD_REQUEST_ANS",word_id+ options);
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getUserId()));
        body.put("word_id", RequestBody.create(MediaType.parse("multipart/form-data"), word_id));
        body.put("opt_id", RequestBody.create(MediaType.parse("multipart/form-data"), options));
        new RetrofitService(getContext(), ServiceUrls.WORD_MATCHING_ANS, 2, 2, body, this).callService(true);
    }

}

