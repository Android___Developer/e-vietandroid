package eviet.vlogic.evietandroidapp.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.adapters.ChatUserListAdapter;
import eviet.vlogic.evietandroidapp.application.ChatApplication;
import eviet.vlogic.evietandroidapp.model.ChatUserListModel;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.retrofit.RetrofitService;
import eviet.vlogic.evietandroidapp.retrofit.ServiceResponce;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;
import eviet.vlogic.evietandroidapp.socketio.FragmentChatMain;
import eviet.vlogic.evietandroidapp.utility.Utility;

public class MessageFragment extends Fragment implements View.OnClickListener, ServiceResponce {
    private final String TAG = "MessageFragment1";
    private RecyclerView recyclerView;
    private Socket mSocket;
    private List<ChatUserListModel> chatUserList = new ArrayList<>();
    private boolean isVisible=false;
    private ProgressDialog dialog;
    private ImageView imgBack,setting;
    private TextView toolTitle;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.d(TAG,"visible to user");
        if (isVisibleToUser&&isVisible)
        {
            Log.d(TAG,"visible to user11");
            mSocket.on("onlineStack", onlineStack);
            mSocket.connect();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        ChatApplication app = (ChatApplication) getActivity().getApplication();
        mSocket = new ChatApplication().getmSocket();
        Log.e("soc", mSocket.toString());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_message, container, false);
        setHasOptionsMenu(true);
        initView(v);

        return v;
    }

    private void initView(View v) {
        imgBack = v.findViewById(R.id.imgBack);
        imgBack.setImageResource(R.drawable.ic_back);
        imgBack.setOnClickListener(this);
        toolTitle =  v.findViewById(R.id.toolTitle);
        toolTitle.setText("Questions");
        setting = v.findViewById(R.id.setting);
        setting.setImageResource(R.drawable.ic_add);
        recyclerView = v.findViewById(R.id.recyclerView);
        dialog=new ProgressDialog(getActivity());
        dialog.setIndeterminate(true);
        dialog.setMessage("Please wait...");
        dialog.setTitle("Loading users");

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        isVisible=true;
        getOrCreateUser();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG,"onResume");
        mSocket.on("onlineStack", onlineStack);
        mSocket.connect();
    }


    private void getOrCreateUser() {
        Log.e("usrnm",PreferencesHelper.getUserName()+"//"+PreferencesHelper.getUserId());
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userId", PreferencesHelper.getUserId());
        jsonObject.addProperty("username",PreferencesHelper.getUserName());
        jsonObject.addProperty("email", "abcd@gmail.com");
        new RetrofitService(getActivity(), ServiceUrls.URL_GET_OR_CREATE_USER, 5, 1, jsonObject, true, this).callService(true);
    }

    @Override
    public void onServiceResponce(String result, int requestCode) {
        Log.e("chtres",result.toString()+"//" );
        if (requestCode == 1) {
            Log.d(TAG, result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.optBoolean("isSuccess")) {
                    JSONObject obj = jsonObject.optJSONObject("message");
                    String username = obj.optString("username");

                    mSocket.emit("set-user-data", username);
                    if (chatUserList.isEmpty()&&!dialog.isShowing())
                    {
                        dialog.show();
                    }

                    Log.e("oko", "oko" + username);


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "OnDetach");
        mSocket.disconnect();
        mSocket.off("onlineStack", onlineStack);
    }

    Emitter.Listener onlineStack = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            if (getActivity()!=null)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("oko", "oko1");
                        chatUserList.clear();
                        JSONObject data = (JSONObject) args[0];
                        Log.e("onlineUser",data.toString());

                        String userData = data.toString();
                        String trimList = userData.substring(1, userData.length() - 1);
                        Log.e("data", trimList + "//");
                        String[] stepOne = trimList.split(",");
                        for (String s : stepOne) {
                            String[] stepTwo = s.split(":");
                            ChatUserListModel model = new ChatUserListModel();
                            model.setUserName(stepTwo[0]);
                            model.setStatus(stepTwo[1]);
                            chatUserList.add(model);
                        }
                        Log.e("list",chatUserList+"");

                        if (!chatUserList.isEmpty()) {
                            if (dialog.isShowing())
                            {
                                dialog.cancel();
                            }
                            ChatUserListAdapter adapter=new ChatUserListAdapter(chatUserList);
                            recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                            adapter.onUserClickListner(new ChatUserListAdapter.UserClickInterface() {
                                @Override
                                public void onUserClick(int position) {

                                    FragmentChatMain fragmentChatMain=new FragmentChatMain();
                                    String userName=chatUserList.get(position).getUserName();
                                    Bundle bundle=new Bundle();
                                    bundle.putString("username",userName);

                                    fragmentChatMain.setArguments(bundle);
                                   /* if (getActivity() instanceof MainActivity)
                                    {
                                        Utility.changeFragment(getActivity(),fragmentChatMain);
                                    }
                                    else {
                                        Utility.changeDashFragment(getActivity(),fragmentChatMain);
                                    }*/

                                    Utility.changeFragment(getActivity(),fragmentChatMain);
                                }
                            });

                        }
                    }
                });

        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                Utility.changeFragment(getActivity(),new DashboardFragment());
                break;
        }
    }
}
