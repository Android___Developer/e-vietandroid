package eviet.vlogic.evietandroidapp.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.BottomActivity;

public class GoogleMapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener, GoogleApiClient.ConnectionCallbacks {
    View rootView;
    private SupportMapFragment mMapFragment;
    private GoogleMap mGoogleMap;
    private FusedLocationProviderClient mFusedLocationClient;
    GoogleApiClient mGoogleApiClient;
    Location mLocation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView=inflater.inflate(R.layout.fragment_google_map, container, false);
        ((BottomActivity) getActivity()).navigation.setVisibility(View.GONE);
        mMapFragment= (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);
       // mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();
        }
        else {
            Log.e("okok1","oko");
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this).build();
            mGoogleApiClient.connect();
            mMapFragment.getMapAsync(this);
        }



        return rootView;
    }
    public void checkPermission() {

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ) {//Can add more as per requirement
         requestPermissions( new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                 123);

         /*   ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    123);*/
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode==123&&grantResults[0]==PackageManager.PERMISSION_GRANTED){

            Log.e("okok","okok");

            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this).build();
            mGoogleApiClient.connect();
            mMapFragment.getMapAsync(this);
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mGoogleMap = googleMap;
        Toast.makeText(getActivity(), "map ready", Toast.LENGTH_SHORT).show();
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

       // LatLng latLng = new LatLng(mLocation.getLatitude(),mLocation.getLongitude());
       // Add a marker in Sydney and move the camera

       LatLng sydney = new LatLng(-16.710858, -49.285672);
       // LatLng sydney = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
//        mGoogleMap.addMarker(new MarkerOptions().position(sydney).title("current location"));
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 10.0f));


        mGoogleMap.setOnMapClickListener(this);
        mGoogleMap.setOnMarkerClickListener(this);

        /*if (mLocation != null) {

            Log.e("locatn", mLocation.getLatitude() + "//" + mLocation.getLongitude());
            LatLng sydney1 = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
            mGoogleMap.addMarker(new MarkerOptions().position(sydney1).title("Sharan in map"));
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney1));
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney1, 17.0f));

        } else {

            Toast.makeText(this, "mlocation null", Toast.LENGTH_SHORT).show();

        }
*/
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        Toast.makeText(getActivity(), "OnConnected", Toast.LENGTH_SHORT).show();

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLocation != null) {
            Log.e("locatn", mLocation.getLatitude() + "//" + mLocation.getLongitude());
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(getActivity(), "OnConnectionSuspended", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMapClick(LatLng latLng) {


    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }
}

