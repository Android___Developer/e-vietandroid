package eviet.vlogic.evietandroidapp.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.BottomActivity;
import eviet.vlogic.evietandroidapp.adapters.FlashAdapter;
import eviet.vlogic.evietandroidapp.adapters.TopicAdapter;
import eviet.vlogic.evietandroidapp.model.FlashCardResponseModel;
import eviet.vlogic.evietandroidapp.model.FlashData;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.retrofit.RetrofitService;
import eviet.vlogic.evietandroidapp.retrofit.ServiceResponce;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;
import eviet.vlogic.evietandroidapp.utility.Utility;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class FlashCardApiFragment extends Fragment implements View.OnClickListener, ServiceResponce {
    View rootView;
    private Toolbar toolbar;
    private TextView toolTitle,progressStatus,flashcardTextFailed;
    private RecyclerView flashRecycleView;
    private FlashAdapter flashAdapter;
    FlashData flashData;
    private TextView questionFlashCard;
    private Button fleshCardCheck;
    private String total_questions,total_answer,total_flashcard_questions,total_flashcard_answered,topic_id;
    private ProgressBar flashcardProgressBar;
    private LinearLayout flashcardLayout,flashcardLayoutFailed;
    private ArrayList<FlashCardResponseModel> arrayList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_flash_card_api, container, false);
        flashRecycleView = rootView.findViewById(R.id.flashRecycleView);
        questionFlashCard = rootView.findViewById(R.id.questionFlashCard);
        fleshCardCheck = rootView.findViewById(R.id.fleshCardCheck);
        progressStatus = rootView.findViewById(R.id.progressStatus);
        flashcardProgressBar = rootView.findViewById(R.id.flashcardProgressBar);
        flashcardLayout = rootView.findViewById(R.id.flashcardLayout);
        flashcardLayoutFailed = rootView.findViewById(R.id.flashcardLayoutFailed);
        flashcardTextFailed = rootView.findViewById(R.id.flashcardTextFailed);

        toolbar = rootView.findViewById(R.id.toolbar);
        toolTitle = rootView.findViewById(R.id.toolTitle);

        if(PreferencesHelper.getValue()!=null){
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                toolTitle.setText("Flash Cards");
                fleshCardCheck.setText(R.string.continue_eng);

            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                toolTitle.setText("Thẻ Flash");
                fleshCardCheck.setText(R.string.continue_viet);
            }
        }

        fleshCardCheck.setOnClickListener(this);

        ((BottomActivity) getActivity()).navigation.setVisibility(View.GONE);
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        gridLayoutManager.setReverseLayout(false);
        flashRecycleView.setLayoutManager(gridLayoutManager);

        getFlashCard();
        return rootView;
    }

    private void getFlashCard() {
        //Retrieve the value
        topic_id = getArguments().getString("topic_id");
        Log.e("flash_topic_id",topic_id+"//"+PreferencesHelper.getValue());
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getUserId()));
        body.put("topic_id", RequestBody.create(MediaType.parse("multipart/form-data"), topic_id));
        body.put("language", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getValue()));
        Log.e("Topic_value", PreferencesHelper.getValue());
        new RetrofitService(getContext(), ServiceUrls.FLASH_CARD, 2, 1, body, this).callService(true);
    }

    @Override
    public void onServiceResponce(String result, int requestCode) {
        if (requestCode == 1) {
            Log.e("Flash_card_res", result + requestCode);

            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                    flashcardLayout.setVisibility(View.VISIBLE);
                    flashcardLayoutFailed.setVisibility(View.GONE);
                    JSONArray array = jsonObject.optJSONArray("data");
                    Log.e("json_array", "" + array);
                    arrayList.clear();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.optJSONObject(i);
                        total_questions = object.optString("total_questions");
                        total_answer = object.optString("total_answer");
                        progressStatus.setText(total_answer+"/"+total_questions);

                        flashcardProgressBar.setMax(Integer.valueOf(total_questions));
                        flashcardProgressBar.setProgress(Integer.valueOf(total_answer));

                        total_flashcard_questions = object.optString("total_flashcard_questions");
                        total_flashcard_answered = object.optString("total_flashcard_answered");

                        Log.e("total_questions", total_flashcard_questions);
                        Log.e("total_answered", total_flashcard_answered);

                        FlashCardResponseModel flashCardResponseModel = new FlashCardResponseModel();
                        flashCardResponseModel.setTotal_nbrof_questions(total_questions);
                        flashCardResponseModel.setTotal_nbrof_answered(total_answer);
                        flashCardResponseModel.setTotal_flashcard_questions(total_flashcard_questions);
                        flashCardResponseModel.setTotal_flashcard_answered(total_flashcard_answered);
                        flashCardResponseModel.setQ_id(object.optString("q_id"));
                        flashCardResponseModel.setQuestion(object.optString("question"));


                        questionFlashCard.setText(object.optString("question"));

                        JSONArray array2 = object.optJSONArray("flash_cards");
                        Log.e("json_array2", "" + array2);

                        ArrayList<FlashData> flashArrayList = new ArrayList<>();
                        flashArrayList.clear();

                        for (int j = 0; j < array2.length(); j++) {

                            JSONObject object2 = array2.optJSONObject(j);

                            String card_id = object2.optString("card_id");
                            String flash_name = object2.optString("name");
                            String flash_image = object2.optString("image");

                            flashData = new FlashData();
                            flashData.setCard_id(card_id);
                            flashData.setName(flash_name);
                            flashData.setImage(flash_image);
                            flashData.setStatus(false);

                            flashArrayList.add(flashData);
                        }

                        flashCardResponseModel.setFlashDatas(flashArrayList);

                        arrayList.add(flashCardResponseModel);

                    }

                    flashAdapter = new FlashAdapter(getActivity(), arrayList.get(arrayList.size() - 1).getFlashDatas());
                    flashRecycleView.setAdapter(flashAdapter);
                    flashAdapter.setOnFlashCardClickListener(new FlashAdapter.FlashCardClick() {
                        @Override
                        public void onFlashCardClick(int pos) {
                            for (int i = 0; i < arrayList.get(arrayList.size() - 1).getFlashDatas().size(); i++) {
                                if (i == pos) {
                                    arrayList.get(arrayList.size() - 1).getFlashDatas().get(i).setStatus(true);
                                } else {
                                    arrayList.get(arrayList.size() - 1).getFlashDatas().get(i).setStatus(false);
                                }
                            }
                            flashAdapter.notifyDataSetChanged();
                        }
                    });
                    //flashAdapter.notifyDataSetChanged();

                }
                else if(jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase
                        ("warning")){
                    String message = jsonObject.optString("message");
                    Toast.makeText(getActivity(),""+message,Toast.LENGTH_SHORT).show();
                }else {
//                    {"status":"failed","message":"Didn't find any Question..!"}
                    if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase
                            ("failed")) {
                        flashcardLayout.setVisibility(View.GONE);
                        flashcardLayoutFailed.setVisibility(View.VISIBLE);
                       String message = jsonObject.optString("message");
                        flashcardTextFailed.setText(message);
                        navigateToWordMatchFragment();
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if (requestCode == 2) {

            Log.e("Flash_card_answer", result + requestCode);

            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
//                    Toast.makeText(getActivity(), jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle("Success !!");
                    alert.setMessage(jsonObject.optString("message"));
                    alert.setPositiveButton("OK",null);
                    alert.show();
                    if (total_flashcard_answered.equalsIgnoreCase(total_flashcard_questions)) {
                        navigateToWordMatchFragment();
                    } else {
                        getFlashCard();
                    }
                }
                else if(jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("warning")){
                    String message = jsonObject.optString("message");
//                    Toast.makeText(getActivity(),""+message,Toast.LENGTH_SHORT).show();

                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle("Warning !!");
                    alert.setMessage(message);
                    alert.setPositiveButton("OK",null);
                    alert.show();
                    if (total_flashcard_answered.equalsIgnoreCase(total_flashcard_questions)) {
                        navigateToWordMatchFragment();
                    } else {
                        getFlashCard();
                    }
                }
                else {
                    if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("failed")) {
                        String message = jsonObject.optString("message");
//                        Toast.makeText(getActivity(),""+message,Toast.LENGTH_SHORT).show();
                        navigateToWordMatchFragment();

                    }

                }

            } catch (JSONException e) {

                e.printStackTrace();

            }

        }
    }

    private void navigateToWordMatchFragment() {
        //Put the value
        WordsMatchFragment wordsMatchFragment = new WordsMatchFragment();
        Bundle args = new Bundle();
        args.putString("topic_id", topic_id);
        wordsMatchFragment.setArguments(args);
        Utility.changeFragment(getActivity(),wordsMatchFragment);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fleshCardCheck:

                String questionId = "", cardId = "";

                for (int i = 0; i < arrayList.get(arrayList.size() - 1).getFlashDatas().size(); i++) {
                    questionId = arrayList.get(arrayList.size() - 1).getQ_id();
                    if (arrayList.get(arrayList.size() - 1).getFlashDatas().get(i).isStatus()) {
                        cardId = arrayList.get(arrayList.size() - 1).getFlashDatas().get(i).getCard_id();
                    }
                }

                if (cardId.equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), "Please select any options", Toast.LENGTH_SHORT).show();
                    return;
                }else{
                    getFlashCardAnswere(questionId, cardId);
                }
                break;
        }

    }

    private void getFlashCardAnswere(String questionId, String cardId) {
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getUserId()));
        body.put("q_id", RequestBody.create(MediaType.parse("multipart/form-data"), questionId));
        body.put("card_id", RequestBody.create(MediaType.parse("multipart/form-data"), cardId));
        new RetrofitService(getContext(), ServiceUrls.FLASH_CARD_ANS, 2, 2, body, this).callService(true);
    }

}
