package eviet.vlogic.evietandroidapp.fragment;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Random;
import java.util.prefs.Preferences;

import de.hdodenhof.circleimageview.CircleImageView;
import eviet.vlogic.evietandroidapp.R;
import eviet.vlogic.evietandroidapp.activities.ImagePickerActivity;
import eviet.vlogic.evietandroidapp.activities.LoginActivity;
import eviet.vlogic.evietandroidapp.prefrences.PreferencesHelper;
import eviet.vlogic.evietandroidapp.retrofit.RetrofitService;
import eviet.vlogic.evietandroidapp.retrofit.ServiceResponce;
import eviet.vlogic.evietandroidapp.retrofit.ServiceUrls;
import eviet.vlogic.evietandroidapp.utility.Utility;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;

public class ProfileFragment extends Fragment implements View.OnClickListener, ServiceResponce {
    View rootView;
    private Toolbar toolbar;
    private TextView toolTitle,loginUserText,profLevelText,profDescText,profLangText,totalFluentText,totallevelCompText,
            totalLessonText,totalAchievementsText,totalQuesSolvedText,Profileusername;
    private ImageView setting;
    CircleImageView Profileimg;
    private Button learnMoreProfileBtn;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getViewProfile();
        // Inflate the layout for this fragment
        rootView=inflater.inflate(R.layout.fragment_profile, container, false);
        toolbar = rootView.findViewById(R.id.toolbar);
        toolTitle = rootView.findViewById(R.id.toolTitle);
        Profileimg = rootView.findViewById(R.id.Profileimg);
        Profileusername = rootView.findViewById(R.id.Profileusername);
        loginUserText = rootView.findViewById(R.id.loginUserText);
        profLevelText = rootView.findViewById(R.id.profLevelText);
        profDescText = rootView.findViewById(R.id.profDescText);
        profLangText = rootView.findViewById(R.id.profLangText);
        totalFluentText = rootView.findViewById(R.id.totalFluentText);
        totallevelCompText = rootView.findViewById(R.id.totallevelCompText);
        totalLessonText = rootView.findViewById(R.id.totalLessonText);
        totalAchievementsText = rootView.findViewById(R.id.totalAchievementsText);
        totalQuesSolvedText = rootView.findViewById(R.id.totalQuesSolvedText);
        learnMoreProfileBtn = rootView.findViewById(R.id.learnMoreProfileBtn);

        if(PreferencesHelper.getUserNumber() == ("guest")){
            loginUserText.setText("Log in");
        }else {
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                loginUserText.setText("Logout");

            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                loginUserText.setText("đăng xuất");
            }
        }

        if(PreferencesHelper.getValue()!=null){
            if(PreferencesHelper.getValue().equalsIgnoreCase("Vietnamese")){
                toolTitle.setText("Profile");
                //loginUserText.setText("Login");
                profLevelText.setText(R.string.pro_level_eng);
                learnMoreProfileBtn.setText(R.string.learn_more_eng);
                profDescText.setText(R.string.pro_desg_eng);
                profLangText.setText(R.string.lang_eng);
                totalFluentText.setText(R.string.totalFluentText_eng);
                totallevelCompText.setText(R.string.totallevelCompText_eng);
                totalLessonText.setText(R.string.totalLessonText_eng);
                totalAchievementsText.setText(R.string.totalAchievementsText_eng);
                totalQuesSolvedText.setText(R.string.totalQuesSolvedText_eng);

            }else if(PreferencesHelper.getValue().equalsIgnoreCase("English")){
                toolTitle.setText("Hồ sơ");
                //loginUserText.setText("Đăng nhập");
                profLevelText.setText(R.string.pro_level_viet);
                learnMoreProfileBtn.setText(R.string.learn_more_viet);
                profDescText.setText(R.string.pro_desg_viet);
                profLangText.setText(R.string.lang_viet);
                totalFluentText.setText(R.string.totalFluentText_viet);
                totallevelCompText.setText(R.string.totallevelCompText_viet);
                totalLessonText.setText(R.string.totalLessonText_viet);
                totalAchievementsText.setText(R.string.totalAchievementsText_viet);
                totalQuesSolvedText.setText(R.string.totalQuesSolvedText_viet);
            }
        }
        setting = rootView.findViewById(R.id.setting);
        setting.setImageResource(R.drawable.ic_settings);
        learnMoreProfileBtn.setOnClickListener(this);
        setting.setOnClickListener(this);
        loginUserText.setOnClickListener(this);
        return rootView;
    }

    private void getViewProfile() {
        HashMap<String, RequestBody> body = new HashMap<>();
        body.put("user_id", RequestBody.create(MediaType.parse("multipart/form-data"), PreferencesHelper.getUserId()));
        new RetrofitService(getContext(), ServiceUrls.VIEW_PROFILE, 2, 1, body, this).callService(true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.setting:
                Utility.changeFragment(getActivity(),new SettingFragment());
                break;

            case R.id.learnMoreProfileBtn:
                Utility.changeFragment(getActivity(),new DashboardFragment());
                break;
            case R.id.loginUserText:
                if (loginUserText.getText().toString().equals("Log in")){
                    getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();
                }else if (loginUserText.getText().toString().equals("Logout")){
                    Toast.makeText(getActivity(), "Logout", Toast.LENGTH_SHORT).show();
                    PreferencesHelper.getInstance(getActivity()).clearPreferencess();
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();

                }
                break;
        }
    }

    @Override
    public void onServiceResponce(String result, int requestCode) {
        if (requestCode == 1) {
            Log.e("view_profile", result + requestCode);

            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("status") && jsonObject.optString("status").equalsIgnoreCase("success")) {
                    JSONArray array = jsonObject.optJSONArray("data");
                    Log.e("json_array", "" + array);

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.optJSONObject(i);
                        Log.e("json_obj", "" + object);
                        String profile_image = object.optString("profile_image");

                        Profileusername.setText(object.optString("username"));


                        Picasso.get()
                                .load(profile_image)
                                .placeholder(R.drawable.avatar)
                                .error(R.drawable.avatar)
                                .into(Profileimg);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
